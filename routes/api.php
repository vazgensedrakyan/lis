<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['prefix' => 'v1', 'namespace' => 'Api\v1'], function () {


    # UPLOAD IMAGE - REQUIRE TOKEN
    Route::group(['namespace' => 'Image', 'middleware' => 'CheckAWTToken'], function () {
        Route::post('/upload-image', "APIUploadImageController@uploadimg");
    });

    # OPEN API
    Route::group(['prefix' => 'client', 'namespace' => 'Client'], function () {
        Route::post('/code', "ClientController@code");
        Route::post('/login', "ClientController@login");
    });

    # PROTECTED API
    Route::group(['prefix' => 'client', 'namespace' => 'Client', 'middleware' => 'CheckAWTToken'], function () {
        Route::post('/get-profile', "ClientController@getProfile");
        Route::post('/set-profile', "ClientController@setProfile");
        Route::post('/logout', "ClientController@logout");
    });







//    Route::group(['prefix' => 'order', 'namespace' => 'Order', 'middleware' => ['auth:api']], function () {
//        Route::post('/make', "OrderController@make")->name('order_make')->middleware('client.only');
//    });

    Route::group(['prefix' => 'client', 'namespace' => 'Client'], function () {
        Route::get('/shops', 'ShopsController@getShops');
        Route::get('/categories', 'CategoriesController@getCategories');
        Route::get('/subcategories', 'CategoriesController@getSubcategories');
        Route::get('/products', 'ProductsController@getProducts');
    });
});

