<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#Auth::routes();
#Route::get('/home', 'HomeController@index')->name('home');

Route::get('/test', 'Admin\LoginController@test')->name('test');
Route::get('/test1', function (){
    echo "test1";
});


# ----------------------------------------------------------------------------------------------------------------------
# ADMIN
# ----------------------------------------------------------------------------------------------------------------------

# SIGNIN ADMIN
Route::group(['prefix' => 'admin'], function() {
    Route::get('/', 'Admin\LoginController@index')->name('login');
    Route::post('/signin', 'Admin\LoginController@signin')->name('signin');
    Route::get('/signout', 'Admin\LoginController@signout')->name('signout');

    Route::get('/password/reset', 'Admin\LoginController@resetpassword')->name('resetpassword');
    Route::post('/password/reset_form', 'Admin\LoginController@resetpassword_form')->name('resetpassword_form');
    Route::get('/password/new', 'Admin\LoginController@setpassword')->name('setpassword');
    Route::post('/password/setpassword', 'Admin\LoginController@setpassword_form')->name('setpassword_form');
});

# CRUDS FOR ADMIN
Route::group(['prefix' => 'admin', 'middleware'=>['auth','role:admin']], function() {

    Route::get('/orders', 'Admin\OrdersController@index')->name('orders');
    Route::get('/order/{id}', 'Admin\OrdersController@order')->name('order.show');


    Route::get('/clients', 'Admin\ClientsController@index')->name('clients');
    Route::get('/client/{id}', 'Admin\ClientsController@client')->name('client');
    Route::put('/client/{id}/toggle_banned', 'Admin\ClientsController@toggle_banned')->name('client.toggle_banned');
    Route::delete('/client/{id}', 'Admin\ClientsController@delete')->name('client.delete');


    Route::get('/managers', 'Admin\ManagersController@index')->name('managers');
    Route::get('/manager/add', 'Admin\ManagersController@add')->name('manager.add');
    Route::get('/manager/{id}', 'Admin\ManagersController@show')->name('manager.show');
    Route::post('/manager', 'Admin\ManagersController@create')->name('manager.create');
    Route::get('/manager/edit/{id}', 'Admin\ManagersController@edit')->name('manager.edit');
    Route::put('/manager/{id}', 'Admin\ManagersController@update')->name('manager.update');
    Route::delete('/manager/{id}', 'Admin\ManagersController@delete')->name('manager.delete');
//    Route::get('/manager', 'Admin\ManagersController@manager')->name('manager');
//    Route::get('/manager/form/{id?}', 'Admin\ManagersController@manager_form')->name('manager_form');

    Route::get('/pickers', 'Admin\PickerController@index')->name('pickers');
//    Route::get('/picker', 'Admin\PickerController@picker')->name('picker');
    Route::get('/picker/add', 'Admin\PickerController@add')->name('picker.add');
    Route::get('/picker/{id}', 'Admin\PickerController@show')->name('picker.show');
    Route::post('/picker', 'Admin\PickerController@create')->name('picker.create');
    Route::get('/picker/edit/{id}', 'Admin\PickerController@edit')->name('picker.edit');
    Route::put('/picker/{id}', 'Admin\PickerController@update')->name('picker.update');
    Route::delete('/picker/{id}', 'Admin\PickerController@delete')->name('picker.delete');
//    Route::get('/picker/form/{id?}', 'Admin\PickerController@picker_form')->name('picker_form');


    Route::get('/couriers', 'Admin\CouriersController@index')->name('couriers');
    Route::get('/courier', 'Admin\CouriersController@courier')->name('courier');
    Route::get('/courier/add', 'Admin\CouriersController@add')->name('courier.add');
    Route::get('/courier/{id}', 'Admin\CouriersController@show')->name('courier.show');
    Route::post('/courier', 'Admin\CouriersController@create')->name('courier.create');
    Route::get('/courier/edit/{id}', 'Admin\CouriersController@edit')->name('courier.edit');
    Route::put('/courier/{id}', 'Admin\CouriersController@update')->name('courier.update');
    Route::delete('/courier/{id}', 'Admin\CouriersController@delete')->name('courier.delete');
    Route::put('/courier/{id}/toggle_banned', 'Admin\CouriersController@toggle_banned')->name('courier.toggle_banned');
    Route::get('/courier/form/{id?}', 'Admin\CouriersController@courier_form')->name('courier_form');


    Route::get('/stores', 'Admin\StoresController@index')->name('stores');

    Route::get('/stores/categories', 'Admin\StoresController@categories')->name('categories');

    Route::get('/stores/products', 'Admin\StoresController@products')->name('products');
    Route::get('/stores/product/{id?}', 'Admin\StoresController@product')->name('product');

    Route::get('/stores/stats', 'Admin\StoresController@stats')->name('stats');

    Route::get('/messages', 'Admin\MessagesController@index')->name('messages');

    Route::get('/feedback', 'Admin\FeedbackController@index')->name('feedback');

    Route::get('/support', 'Admin\SupportController@index')->name('supports');
    Route::get('/support/{id}', 'Admin\SupportController@support')->name('support');


    Route::get('/loyalty', 'Admin\LoyaltyController@index')->name('loyalty');
    Route::get('/loyalty/stock', 'Admin\LoyaltyController@stock')->name('stock');
    Route::get('/loyalty/bonuses', 'Admin\LoyaltyController@bonuses')->name('bonuses');

});







# ----------------------------------------------------------------------------------------------------------------------
# ASSEMBLY
# ----------------------------------------------------------------------------------------------------------------------
# SIGNIN ASSEMBLY

    Route::get('/', 'Assembly\LoginController@index')->name('assemblySignin');
    Route::post('/signin', 'Assembly\LoginController@signin')->name('assemblySigninPost');
    Route::get('/signout', 'Assembly\LoginController@signout')->name('assemblySignout');


    Route::get('/password/reset', 'Admin\LoginController@resetpassword')->name('resetpassword');
    Route::post('/password/reset_form', 'Admin\LoginController@resetpassword_form')->name('resetpassword_form');
    Route::get('/password/new', 'Admin\LoginController@setpassword')->name('setpassword');
    Route::post('/password/setpassword', 'Admin\LoginController@setpassword_form')->name('setpassword_form');

    Route::group(['prefix' => 'assembly', 'middleware'=>['auth','role:manager']], function() {

        Route::get('/orders', 'Assembly\OrdersController@index')->name('assemblyOrders');
        Route::get('/order/{id}', 'Assembly\OrdersController@order')->name('assemblyOrder');

        Route::get('/profile', 'Assembly\ProfileController@index')->name('assemblyProfiles');
        Route::get('/profile/edit/{id}', 'Assembly\ProfileController@profile')->name('assemblyProfile');

        Route::get('/support', 'Assembly\SupportController@index')->name('assemblySupports');
        Route::get('/support/{id}', 'Assembly\SupportController@support')->name('assemblySupport');

    });
