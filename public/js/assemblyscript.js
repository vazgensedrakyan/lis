$(function () {
    $(".main-nav__burger").on("click", function () {
       $(this).toggleClass("main-nav__burger--active");
    });



    if ($(".main-content__dropdown-container")) {
        $(".main-content__dropdown-inner-container").on("click", function () {
            $(this).addClass("main-content__dropdown-inner-container--focus");
            $(".main-content__dropdown-inner-container").not($(this)).removeClass("main-content__dropdown-inner-container--focus");

            $(this).children(".main-content__dropdown-list").toggleClass("main-content__dropdown-list--opened");
            $(".main-content__dropdown-inner-container").not($(this)).children(".main-content__dropdown-list").removeClass("main-content__dropdown-list--opened");
        });

        /*==================================================================*/
        $(".main-content__dropdown-item").on("click", function (e) {
            var $choosen_item = $(this).parent().siblings(".main-content__dropdown-current");
            $choosen_item.text($(this).text());
        });

        $(document).on("click", function (e) {

            if (!$(e.target).is($(".main-content__dropdown-inner-container")) &&
                !$(e.target).is(".main-content__dropdown-list")) {
                $(".main-content__dropdown-inner-container").removeClass("main-content__dropdown-inner-container--focus");
                $(".main-content__dropdown-list").removeClass("main-content__dropdown-list--opened");
            }
        });
    }

    /*==================================================================*/
    if ($(".js--add-field")) {
        $(".js--add-field").on("click", function () {

            if ($(this).siblings($(".main-content__hidden-input-container")).length < 5) {
                $(this).siblings(".main-content__hidden-input-container").not(".main-content__cloned").clone().addClass("main-content__cloned").insertBefore($(this));
            }

            if ($(this).siblings($(".main-content__hidden-input-container")).length === 5) {
                $(this).css("display", "none");
            }
        });

        $(".main-content__clones-container").on("click", ".js--hidden-save", function () {
            $(this).parent().css("display", "none");
            $(this).parent().parent().addClass("main-content__accepted");
        });

        $(".main-content__clones-container").on("click", ".js--hidden-delete", function () {
            $(this).parent().parent().siblings(".main-content__add-field.js--add-field").css("display", "block");
            $(this).parent().parent().remove();
        });
    }

    /*==================================================================*/

    if ($(".pseudo-table__table-row")) {
        $(".pseudo-table__table-dropdown-btn.pseudo-table__category-dropdown-btn").on("click", function () {
            $(this).parent().parent().toggleClass("pseudo-table__table-row--border");
            $(this).toggleClass("pseudo-table__table-dropdown-btn--opened");

            if ($(this).hasClass("pseudo-table__table-dropdown-btn--opened")) {
                        // $(".pseudo-table__table-row--subcategory").css("display", "flex");
                        // $(".pseudo-table__table-row--subcategory").last().addClass("pseudo-table__table-row--subcategory-bottom");
                    } else {
                        // $(".pseudo-table__table-row--subcategory").css("display", "none");
                $(".pseudo-table__table-dropdown-btn.pseudo-table__subcategory-dropdown-btn").trigger("click");
                    }
        });

        $(".pseudo-table__table-dropdown-btn.pseudo-table__subcategory-dropdown-btn").on("click", function () {
            $(this).toggleClass("pseudo-table__table-dropdown-btn--opened");

            if ($(this).hasClass("pseudo-table__table-dropdown-btn--opened")) {
                // $(".pseudo-table__table-row--subcategory-2").css("display", "flex");
                // $(".pseudo-table__table-row--subcategory-2").last().addClass("pseudo-table__table-row--subcategory-2-bottom");
            } else {
                // $(".pseudo-table__table-row--subcategory-2").css("display", "none");
            }
        });
    }

    /*==================================================================*/
    if ($(".main-content__table-eye-btn")) {
        $(".main-content__table-eye-btn").on("click", function () {
            $(this).parent().parent().toggleClass("main-content__table-row--invisible");

            if ($(this).parent().parent().hasClass("main-content__table-row--invisible")) {
                $(this).parent().siblings(".js--product-status").text("Скрыт");
            }
            if (!$(this).parent().parent().hasClass("main-content__table-row--invisible")) {
                $(this).parent().siblings(".js--product-status").text("Опубликован");
            }
        });

    }


    if ($(".main-content__table-eye-btn.main-content__category-eye-btn")) {
        $(".main-content__table-eye-btn.main-content__category-eye-btn").on("click", function () {
            if ($(this).parent().parent().hasClass("main-content__table-row--invisible")) {
                $(this).parent().parent().nextUntil(".main-content__table-row--category").addClass("main-content__table-row--invisible").addClass("main-content__table-row--invisible-subcategory");
            } else {
                $(this).parent().parent().nextUntil(".main-content__table-row--category").removeClass("main-content__table-row--invisible-subcategory");
            }
        });
    }

    // if ($(".pseudo-table__table-row")) {
    //     $(".main-content__table-eye-btn.main-content__category-eye-btn").on("click", function () {
    //         if ($(this).parent().parent().hasClass("pseudo-table__table-row--invisible")) {
    //             $(this).parent().parent().nextUntil(".pseudo-table__table-row--category").addClass("pseudo-table__table-row--invisible").addClass("pseudo-table__table-row--invisible-subcategory");
    //         } else {
    //             $(this).parent().parent().nextUntil(".pseudo-table__table-row--category").removeClass("pseudo-table__table-row--invisible-subcategory");
    //         }
    //     });
    // }

    /*==================================================================*/
    if ($(".js--create-category-popup")) {
        $(".js--create-category-popup").on("click", function (e) {
            e.preventDefault();
            $(".popup").addClass("popup--showed");
            $("body").addClass("unscrolled");
            $(".popup__create-category").addClass("popup--showed");
        });
    }

    if ($(".js--edit-category-popup")) {
        $(".js--edit-category-popup").on("click", function (e) {
            e.preventDefault();
            $(".popup").addClass("popup--showed");
            $("body").addClass("unscrolled");
            $(".popup__edit-category").addClass("popup--showed");
        });
    }

    if ($(".js--product-edit-popup")) {
        $(".js--product-edit-popup").on("click", function (e) {
            e.preventDefault();
            $(".popup").addClass("popup--showed");
            $("body").addClass("unscrolled");
            $(".popup__product-edit").addClass("popup--showed");
        });
    }

    if($(".js--action-popup")) {
        $(".js--action-popup").on("click", function (e) {
            e.preventDefault();
            $(".popup").addClass("popup--showed");
            $("body").addClass("unscrolled");
            $(".popup__action").addClass("popup--showed");
        });
    }

    if($(".js--action-edit-popup")) {
        $(".js--action-edit-popup").on("click", function (e) {
            e.preventDefault();
            $(".popup").addClass("popup--showed");
            $("body").addClass("unscrolled");
            $(".popup__action-edit").addClass("popup--showed");
        });
    }

    if($(".js--promocode-popup")) {
        $(".js--promocode-popup").on("click", function (e) {
            e.preventDefault();
            $(".popup").addClass("popup--showed");
            $("body").addClass("unscrolled");
            $(".popup__promocode").addClass("popup--showed");
        });
    }

    if($(".js--support-popup")) {
        $(".js--support-popup").on("click", function (e) {
            e.preventDefault();
            $(".popup").addClass("popup--showed");
            $("body").addClass("unscrolled");
            $(".popup__support").addClass("popup--showed");
        });
    }

    if($(".js--delete-row")) {
        $(".js--delete-row").on("click", function (e) {
            e.preventDefault();
            $(".popup").addClass("popup--showed");
            $("body").addClass("unscrolled");
            $(".popup__delete").addClass("popup--showed");
        });
    }

    if($(".js--add-users-loyalty")) {
        $(".js--add-users-loyalty").on("click", function (e) {
            e.preventDefault();
            $(".popup").addClass("popup--showed");
            $("body").addClass("unscrolled");
            $(".popup__add-users-loyalty").addClass("popup--showed");
        });
    }

    if($(".js--add-loyalty-category")) {
        $(".js--add-loyalty-category").on("click", function (e) {
            e.preventDefault();
            $(".popup").addClass("popup--showed");
            $("body").addClass("unscrolled");
            $(".popup__add-loyalty-category").addClass("popup--showed");
        });
    }

    if($(".js--edit-loyalty-category")) {
        $(".js--edit-loyalty-category").on("click", function (e) {
            e.preventDefault();
            $(".popup").addClass("popup--showed");
            $("body").addClass("unscrolled");
            $(".popup__edit-loyalty-category").addClass("popup--showed");
        });
    }

    if ($(".popup")) {
        $(".popup__close").on("click", function () {
            $(".popup").removeClass("popup--showed");
            $("body").removeClass("unscrolled");

            $(".popup__create-category").removeClass("popup--showed");
            $(".popup__edit-category").removeClass("popup--showed");
            $(".popup__product-edit").removeClass("popup--showed");
            $(".popup__action").removeClass("popup--showed");
            $(".popup__action-edit").removeClass("popup--showed");
            $(".popup__promocode").removeClass("popup--showed");
            $(".popup__support").removeClass("popup--showed");
            $(".popup__delete").removeClass("popup--showed");
            $(".popup__add-users-loyalty").removeClass("popup--showed");
            $(".popup__add-loyalty-category").removeClass("popup--showed");
            $(".popup__edit-loyalty-category").removeClass("popup--showed");
        });

        $(".popup").on("click", function (e) {
            if ($(".popup").is(e.target) && $(".popup").has(e.target).length === 0) {
                $(".popup").removeClass("popup--showed");
                $("body").removeClass("unscrolled");

                $(".popup__create-category").removeClass("popup--showed");
                $(".popup__edit-category").removeClass("popup--showed");
                $(".popup__product-edit").removeClass("popup--showed");
                $(".popup__action").removeClass("popup--showed");
                $(".popup__action-edit").removeClass("popup--showed");
                $(".popup__promocode").removeClass("popup--showed");
                $(".popup__support").removeClass("popup--showed");
                $(".popup__delete").removeClass("popup--showed");
                $(".popup__add-users-loyalty").removeClass("popup--showed");
                $(".popup__add-loyalty-category").removeClass("popup--showed");
                $(".popup__edit-loyalty-category").removeClass("popup--showed");
            }
        });

        $(".popup-submit").on("click", function () {
            $(".popup").removeClass("popup--showed");
            $("body").removeClass("unscrolled");

            $(".popup__create-category").removeClass("popup--showed");
            $(".popup__edit-category").removeClass("popup--showed");
            $(".popup__product-edit").removeClass("popup--showed");
            $(".popup__action").removeClass("popup--showed");
            $(".popup__action-edit").removeClass("popup--showed");
            $(".popup__promocode").removeClass("popup--showed");
            $(".popup__support").removeClass("popup--showed");
            $(".popup__delete").removeClass("popup--showed");
            $(".popup__add-users-loyalty").removeClass("popup--showed");
            $(".popup__add-loyalty-category").removeClass("popup--showed");
            $(".popup__edit-loyalty-category").removeClass("popup--showed");
        });
    }

    if($(".main-content__table-btn--switch")) {
        $(".main-content__table-btn--switch").on("click", function () {
            if($(this).hasClass("main-content__table-btn--gray")) {
                $(this).removeClass("main-content__table-btn--gray").addClass("main-content__table-btn--yellow").text("Отключить");
            }
            else if($(this).hasClass("main-content__table-btn--yellow")) {
                $(this).removeClass("main-content__table-btn--yellow").addClass("main-content__table-btn--gray").text("Включить");
            }
        });
    }

    /*==================================================================*/
    if($(".js--block-btn")) {
        $(".js--block-btn").on("click", function () {
            $(this).toggleClass("js--block-btn--allowed");

            if($(this).hasClass("js--block-btn--allowed")) {
                $(this).text("Восстановить");
            }
            if(!$(this).hasClass("js--block-btn--allowed")) {
                $(this).text("Забанить");
            }

            $(this).parent().parent().toggleClass("main-content__table-row--invisible");

            if ($(this).parent().parent().hasClass("main-content__table-row--invisible")) {
                $(this).parent().siblings(".js--courier-status").text("Не активен");
            }
            if (!$(this).parent().parent().hasClass("main-content__table-row--invisible")) {
                $(this).parent().siblings(".js--courier-status").text("Активен");
            }
        });
    }


    /*==============================================================================================*/
    if ($(".main-content__profile-document-container")) {
        $(".main-content__profile-document-container img").on("click",function () {
            var img = $(this);
            var src = img.attr('src');
            $("body").append("<div class='popup-documents'>" +
                "<img src='" + src + "' class='popup-documents__img' />" + "</div>");
            $("body").addClass("unscrolled");
            $(".popup-documents").fadeIn(300);

            $(".popup-documents").click(function () {
                $("body").removeClass("unscrolled");
                $(".popup-documents").fadeOut(300);
                setTimeout(function () {
                    $(".popup-documents").remove();
                }, 300);
            });
        });
    }

    if ($(".main-content__profile-big-document-container")) {
        $(".main-content__profile-big-document-container img").on("click",function () {
            var img = $(this);
            var src = img.attr('src');
            $("body").append("<div class='popup-documents'>" +
                "<img src='" + src + "' class='popup-documents__img' />" + "</div>");
            $("body").addClass("unscrolled");
            $(".popup-documents").fadeIn(300);

            $(".popup-documents").click(function () {
                $("body").removeClass("unscrolled");
                $(".popup-documents").fadeOut(300);
                setTimeout(function () {
                    $(".popup-documents").remove();
                }, 300);
            });
        });
    }

    $(".switcher").on("click", function () {
        $(this).toggleClass("switcher--active")
    });

    $(".main-nav__inactive-link-container").on("click", function () {
        $(this).children(".main-nav__sublist").slideToggle(200);
        $(this).find(".main-nav__sublist-btn").toggleClass("main-nav__sublist-btn--opened");
        $(".main-nav__inactive-link-container").not($(this)).children(".main-nav__sublist").slideUp(200);
        $(".main-nav__inactive-link-container").not($(this)).find(".main-nav__sublist-btn").removeClass("main-nav__sublist-btn--opened");
    });

    $(".checkbox").on("click", function () {
       $(this).toggleClass("checkbox--checked");
    });
});