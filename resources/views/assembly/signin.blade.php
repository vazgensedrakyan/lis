@extends('admin.layouts.authLayout')

@section('content')

    <section class="white-square">
        <div class="white-square__wrapper">

            <h1 class="white-square__title">Авторизация сборщика</h1>

            {{ Form::open(array('id' => 'formx', 'class'=> 'white-square__form', 'route' => 'assemblySigninPost', 'method' => 'POST')) }}

                <div class="white-square__input-container">
                    <label class="white-square__input-label">E-mail</label>
                    <input name="email" type="text" class="white-square__input">

                    @if($errors->any())
                        <span class="help-block">
                            <strong> {{ $errors->get("email")?$errors->get("email")[0] : "" }}</strong>
                        </span>
                    @endif


                </div>

                <div class="white-square__input-container">
                    <label class="white-square__input-label">Пароль</label>
                    <input name="password" type="password" class="white-square__input">

                    @if($errors->any())
                        <span class="help-block">
                            <strong> {{ $errors->get("password")?$errors->get("password")[0] : "" }}</strong>
                        </span>
                    @endif
                </div>

                <div class="white-square__links-container">
                    <button type="submit" class="green-btn">Войти</button>
                    <a class="white-square__gray-link" href="{{ route("resetpassword") }}">Забыли пароль?</a>
                </div>
            {{ Form::close() }}

        </div>

        <p class="paragraph  metaproject__container">
            Разработано в дизайн студии
            <a class="metaproject__link" href="#">MetaProject</a>
        </p>
    </section>


@endsection
