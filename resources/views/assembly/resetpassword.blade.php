@extends('admin.layouts.authLayout')

@section('content')


    <section class="white-square">
        <div class="white-square__wrapper">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <h1 class="white-square__title">Забыли пароль?</h1>

            <p class="paragraph  paragraph--margin-bottom">На указанную почту будет отправлена ссылка для сброса
                пароля для входа в учетную запись</p>


            {{ Form::open(array('id' => 'formx', 'class'=> 'white-square__form', 'route' => 'resetpassword_form', 'method' => 'POST')) }}

            <div class="white-square__input-container{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="white-square__input-label">E-mail</label>
                <input type="text" class="white-square__input" name="email" value="{{ old('email') }}" required>
                @if($errors->any())
                    <span class="help-block">
                            <strong> {{ $errors->get("email")?$errors->get("email")[0] : "" }}</strong>
                        </span>
                @endif
            </div>

            <div class="white-square__links-container">
                <button type="submit" class="green-btn">Сброс пароля</button>
            </div>
            {{ Form::close() }}

        </div>
    </section>


@endsection
