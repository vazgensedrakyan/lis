@extends('assembly.layouts.assemblyLayout')

@section('content')


            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <span class="breadcrumbs__item  breadcrumbs__item--current">Профиль</span>
                </div>
            </section>


            <div class="main-content__profile-info">
                <div class="main-content__profile-img-container">
                    <img src="/img/user-anchor.jpg" alt="photo">
                </div>

                <div class="main-content__profile-top">

                    <h2 class="main-content__profile-name">Геогрий Георгиевич</h2>

                    <a class="btn btn__green" href="{{ route("assemblyProfile", ["id" => 1]) }}">Редактировать</a>

                </div>

                <div class="main-content__profile-data">

                    <ul class="main-content__profile-data-list  main-content__profile-data-list--col-3">
                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">ID:</li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">250</li>

                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                            Зарегистрирован:
                        </li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">01 / 03 /
                            2019
                        </li>

                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">Моб.
                            телефон:
                        </li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                            +79630007007
                        </li>

                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">Почта:</li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                            <a class="main-content__profile-data-link" href="#">geo@mail.ru</a>
                        </li>
                    </ul>

                </div>


                <input type="radio" class="hidden-input" id="client-stats" name="client-inner-tabs" checked>
                <input type="radio" class="hidden-input" id="client-orders" name="client-inner-tabs">

                <div class="tabs-container">
                    <label class="tabs-container__btn" for="client-stats">Статистика</label>
                    <label class="tabs-container__btn" for="client-orders">Мои заказы</label>
                </div>


                <div class="main-content__store-info  main-content__order-split  main-content__order-split--flex-start"
                     id="for-client-stats">
                    <div class="split-container split-container--wrap">
                        <h2 class="main-content__profile-stats-title">Статистика</h2>

                        <div class="main-content__profile-stats-item">
                            <p class="main-content__profile-stats-paragraph">Время сбора заказа</p>

                            <h3 class="main-content__profile-stats-bold">35 мин</h3>
                        </div>


                        <div class="main-content__profile-stats-item">
                            <p class="main-content__profile-stats-paragraph">Рекорд собранных заказов за день</p>

                            <h3 class="main-content__profile-stats-bold">98 шт</h3>
                        </div>
                    </div>
                </div>

                <div id="for-client-orders">


                    <div class="split-container  paragraph--margin-bottom">
                        <div class="main-content__dropdown-container  main-content__dropdown-container--margin">
                <span class="main-content__dropdown-label">Выберите тип заказа
                </span>

                            <div class="main-content__dropdown-inner-container">
                                <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                                <ul class="main-content__dropdown-list">
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                                </ul>
                            </div>
                        </div>

                        <div class="white-square__input-container  main-content__data-range-container  ">

                            <label class="white-square__input-label">
                                Выберите период
                            </label>

                            <input type="text" class="white-square__input  popup__input  datepicker-here"
                                   data-position="bottom right" data-timepicker="true" data-range="true"
                                   data-multiple-dates-separator=" - ">
                        </div>
                    </div>

                    <table class="main-content__table">
                        <tr class="main-content__header-row">
                            <th class="main-content__header-cell  main-content__header-cell--nowrap">
                                <button class="main-content__table-sort-btn" type="button">Дата</button>
                            </th>

                            <th class="main-content__header-cell  main-content__header-cell--nowrap">
                                <button class="main-content__table-sort-btn" type="button">№ Заказа</button>
                            </th>

                            <th class="main-content__header-cell  main-content__header-cell--nowrap">
                                <button class="main-content__table-sort-btn" type="button">№ Пакета</button>
                            </th>

                            <th class="main-content__header-cell">
                                <button class="main-content__table-sort-btn" type="button">Доставка</button>
                            </th>

                            <th class="main-content__header-cell">
                                <button class="main-content__table-sort-btn" type="button">Товары</button>
                            </th>

                            <th class="main-content__header-cell">
                                <button class="main-content__table-sort-btn" type="button">Вес</button>
                            </th>

                            <th class="main-content__header-cell">Статус</th>
                        </tr>

                        <tr class="main-content__table-row">
                            <td class="main-content__table-cell  main-content__table-cell--padding">22/10/19</td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">
                                <a class="main-content__id  main-content__id--link" href="order-inner.html">225584</a>
                            </td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">154887</td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">Курьером сегодня 15:00</td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">13 шт</td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">156 гр</td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">
                                <a class="btn  btn__green  btn--fixed-width-table" href="order-inner.html">Собрать</a>
                            </td>
                        </tr>

                        <tr class="main-content__table-row">
                            <td class="main-content__table-cell  main-content__table-cell--padding">22/10/19</td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">
                                <a class="main-content__id  main-content__id--link" href="order-inner.html">225584</a>
                            </td>

                            <td class="main-content__table-cell  main-content__table-cell--padding  main-content__table-cell--red">
                                не назначен
                            </td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">Курьером сегодня 15:00</td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">13 шт</td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">156 гр</td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">
                                <span class="btn  btn__half-blue  btn--no-events  btn--fixed-width-table">Идет доставка</span>
                            </td>
                        </tr>


                    </table>
                </div>


            </div>


@endsection
