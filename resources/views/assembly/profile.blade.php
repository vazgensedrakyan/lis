@extends('assembly.layouts.assemblyLayout')

@section('content')

            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <a class="breadcrumbs__item" href="client-inner.html">Профиль</a>

                    <span class="breadcrumbs__item  breadcrumbs__item--current">Редактирование профиля</span>
                </div>
            </section>

            <div class="content-center">
                <div class="main-content__top-container">
                    <h1 class="main-content__title">Редактирование профиля</h1>
                </div>

                <div class="main-content__store-photo  paragraph--margin-bottom">
                    <div class="main-content__store-photo-container">
                        <h3 class="main-content__store-photo-title">Фото</h3>
                        <p class="main-content__store-photo-description">(фото должно быть до 3Mb)</p>
                    </div>

                    <button class="main-content__store-photo-delete" type="button"></button>

                    <label class="main-content__upload-container">
                        <input class="hidden-input" type="file">

                        <span class="main-content__upload-btn">Обзор...</span>

                        <span class="main-content__upload-message">Файл не выбран</span>
                    </label>
                </div>


                <div class="half">
                    <div class="white-square__input-container">
                        <label class="white-square__input-label">
                            Имя менеджера
                        </label>

                        <input type="text" class="white-square__input">
                    </div>

                    <div class="split-container  split-container--space-between">
                        <div class="white-square__input-container">
                            <label class="white-square__input-label">
                                Контактный телефон
                            </label>

                            <input type="text" class="white-square__input">
                        </div>
                    </div>
                </div>


                <div class="main-content__store-btns-container">
                    <a class="btn btn__red" href="client-inner.html">Отмена</a>
                    <a class="btn btn__green" href="client-inner.html">Сохранить</a>
                </div>
            </div>


@endsection
