@extends('assembly.layouts.assemblyLayout')

@section('content')

            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <span class="breadcrumbs__item  breadcrumbs__item--current">Заказы</span>
                </div>
            </section>

            <div class="main-content__top-container">
                <h1 class="main-content__title">Заказы</h1>
            </div>


            <div class="split-container  paragraph--margin-bottom">
                <div class="main-content__dropdown-container  main-content__dropdown-container--margin">
                <span class="main-content__dropdown-label">Выберите тип заказа
                </span>

                    <div class="main-content__dropdown-inner-container">
                        <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                        <ul class="main-content__dropdown-list">
                            <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                            <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                        </ul>
                    </div>
                </div>

                <div class="white-square__input-container  main-content__data-range-container  ">

                    <label class="white-square__input-label">
                        Выберите период
                    </label>

                    <input type="text" class="white-square__input  popup__input  datepicker-here"
                           data-position="bottom right" data-timepicker="true" data-range="true"
                           data-multiple-dates-separator=" - ">
                </div>
            </div>

            <table class="main-content__table">
                <tr class="main-content__header-row">
                    <th class="main-content__header-cell  main-content__header-cell--nowrap">
                        <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">№ Заказа</button>
                    </th>

                    <th class="main-content__header-cell  main-content__header-cell--nowrap">
                        <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">№ Пакета</button>
                    </th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">Доставка</button>
                    </th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">Товары</button>
                    </th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">Вес</button>
                    </th>

                    <th class="main-content__header-cell">Статус</th>
                </tr>

                <tr class="main-content__table-row">
                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <a class="main-content__id  main-content__id--link" href="{{ route("assemblyOrder", ["id" => 1]) }}">225584</a>
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">154887</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">Курьером сегодня 15:00</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">13 шт</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">156 гр</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <a class="btn  btn__green  btn--fixed-width-table" href="order-inner.html">Собрать</a>
                    </td>
                </tr>

                <tr class="main-content__table-row">
                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <a class="main-content__id  main-content__id--link" href="{{ route("assemblyOrder", ["id" => 2]) }}">225584</a>
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding  main-content__table-cell--red">
                        не назначен
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">Курьером сегодня 15:00</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">13 шт</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">156 гр</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <span class="btn  btn__half-blue  btn--no-events  btn--fixed-width-table">Идет доставка</span>
                    </td>
                </tr>


            </table>



@endsection
