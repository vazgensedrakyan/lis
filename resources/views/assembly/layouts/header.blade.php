@section('header')
    <header class="page-header">
        <div class="page-header__wrapper">

            <img class="page-header__logo" src="/img/logo.png" alt="logo">
            <a class="page-header__user-name" href="#">Георгий Георгиевич</a>
            <a class="logout-btn" href="{{ route("assemblySignout") }}">Выйти</a>
        </div>
    </header>
@stop
