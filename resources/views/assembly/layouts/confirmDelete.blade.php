@section('confirmDelete')
    <section class="popup__delete">
        <h4 class="popup__title">Вы уверены, что хотите удалить?</h4>

        <button class="green-btn  green-btn--profile  popup-submit" type="button">Удалить</button>
        <button class="red-btn  red-btn--profile  popup-submit" type="button">Отменить</button>

        <button class="popup__close" type="button">Закрыть</button>
    </section>
@stop
