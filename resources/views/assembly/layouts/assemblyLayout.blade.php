<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
    <link href="/css/assembly.style.min.css" rel="stylesheet">
</head>
<body>

<div class="page-grid">

    @include('assembly.layouts.header')
    @yield('header')

    @include('assembly.layouts.navbar')
    @yield('navbar')

    <section class="main-content">
        <div class="main-content__wrapper">
            @yield('content')
        </div>
    </section>

</div>




@stack('popup')


<script src="/js/jquery-3.3.1.min.js"></script>
<script src="/libs/bootstrap-4.3.1/js/bootstrap.js"></script>
<script src="/js/datepicker.min.js"></script>
<script src="/js/assemblyscript.js"></script>
@stack('scripts')

</body>
</html>
