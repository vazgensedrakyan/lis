@section('navbar')

    <button class="main-nav__burger" type="button">main nav</button>

    <aside class="main-nav">

        <div class="main-nav__wrapper">

            <div class="main-nav__links-list">
                <a class="main-nav__link  main-nav__link--orders  @if(@$menu == "orders") main-nav__link--current @endif " href="{{ route("assemblyOrders") }}">Заказы
                    <span class="main-nav__sublink-count">5</span>
                </a>

                <a class="main-nav__link  main-nav__link--couriers @if(@$menu == "profile") main-nav__link--current @endif " href="{{ route("assemblyProfiles") }}">Профиль</a>

                <a class="main-nav__link  main-nav__link--support @if(@$menu == "support") main-nav__link--current @endif " href="{{ route("assemblySupports") }}">Поддержка
                    <span class="main-nav__sublink-count">23435456262457742</span>
                </a>

                <a class="main-nav__link  main-nav__link--logout" href="{{ route("assemblySignout") }}">Выйти</a>
            </div>

            <p class="paragraph  metaproject__container  metaproject__aside-container">
                Разработано в дизайн студии <br>
                <a class="metaproject__link  metaproject__aside-link" href="#">MetaProject</a>
            </p>

        </div>
    </aside>
@stop
