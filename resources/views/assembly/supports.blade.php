@extends('assembly.layouts.assemblyLayout')

@section('content')

    <section class="breadcrumbs">
        <div class="breadcrumbs__list">
            <span class="breadcrumbs__item  breadcrumbs__item--current">Поддержка</span>
        </div>
    </section>

    <div class="main-content__top-container">
        <h1 class="main-content__title">Поддержка</h1>

        <button type="button" class="btn btn__green  js--support-popup">Создать тему</button>
    </div>

    <table class="main-content__table  main-content__shops-table">
        <tr class="main-content__header-row">
            <th class="main-content__header-cell">ID</th>

            <th class="main-content__header-cell">Тема</th>

            <th class="main-content__header-cell">Создана</th>

            <th class="main-content__header-cell">Статус</th>
        </tr>

        <tr class="main-content__table-row">
            <td class="main-content__table-cell  main-content__table-cell--padding">
                <a href="{{ route("assemblySupport", ["id" => 1]) }}"
                   class="main-content__table-link">225584</a>
            </td>

            <td class="main-content__table-cell  main-content__table-cell--padding  half">Поправьте фичу которую вы
                сделали случайно в моей учетной записи
            </td>

            <td class="main-content__table-cell  main-content__table-cell--padding">01 / 01 / 2019</td>

            <td class="main-content__table-cell  main-content__table-cell--padding  main-content__table-cell--relative">
                Решен
                <span class="main-nav__sublink-count">23435456262457742</span>
            </td>
        </tr>
    </table>


    @push('popup')
        <section class="popup">

            <section class="popup__support">
                <h4 class="popup__title">Создать тему</h4>

                <div class="white-square__input-container">
                    <label class="white-square__input-label">Тема</label>

                    <input type="text" class="white-square__input">
                </div>

                <div class="white-square__input-container">
                    <label class="white-square__input-label">
                        Ваш вопрос
                    </label>

                    <textarea class="white-square__input"
                              rows="6">Зашел в админку и вышел. ничего не понятно. жаль.</textarea>
                </div>

                <div class="white-square__input-container  main-content__chat-file-container">
                    <span class="white-square__input-label">Добавить файл (макс. размер 10 MЬ)</span>

                    <label class="main-content__upload-container">
                        <input class="hidden-input" type="file">

                        <span class="main-content__upload-btn">Обзор...</span>

                        <span class="main-content__upload-message">Файл не выбран</span>
                    </label>
                </div>

                <button class="btn  btn__green  popup-submit" type="button">Создать</button>

                <button class="popup__close" type="button">Закрыть</button>
            </section>

        </section>
    @endpush

@endsection
