@extends('assembly.layouts.assemblyLayout')

@section('content')


            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <a class="breadcrumbs__item" href="{{ route("assemblySupports") }}">Поддержка</a>

                    <span class="breadcrumbs__item  breadcrumbs__item--current">Поправьте фичу которую вы сделали случайно в моей учетной записи</span>
                </div>
            </section>

            <div class="main-content__top-container">
                <h1 class="main-content__title  main-content__title--max-width">Поправьте фичу которую вы сделали
                    случайно в моей учетной записи</h1>

                <p class="main-content__chat-paragraph">
                    <strong>Активность</strong>
                    25 / 04 / 2019
                </p>
            </div>

            <div class="main-content__chat-message  main-content__chat-message--user">
                <div class="main-content__chat-message-status  main-content__chat-message-status--double-check">
                    Прочитано
                </div>


                <div class="main-content__chat-photo-container">
                    <img src="/img/user-anchor.jpg" alt="photo">
                </div>

                <div class="main-content__chat-text-container">
                    <h4 class="main-content__chat-name">Александр Пушкин</h4>

                    <span class="main-content__chat-time">01 / 01 / 2019 18:55</span>

                    <div class="main-content__chat-link">
                        <div class="main-content__chat-link-img-container">
                            <img src="/img/product-anchor.jpg" alt="photo">
                        </div>

                        <div class="main-content__chat-link-name-container">
                            <a class="main-content__table-link" href="goods-inner.html">Шоколадное яйцо Макси Kinder
                                FERRORO, 100г</a>
                            <br>
                            <span class="main-content__table-count">Арт. 40505848</span>
                        </div>
                    </div>

                    <p class="main-content__chat-paragraph">Равным образом постоянный количественный рост и сфера нашей
                        активности позволяет оценить
                        значение пози ций, занимаемых участниками в отношении поставленных задач. Таким образом
                        начало повседневной работы по формированию позиции обеспечивает широкому кругу
                        (специалистов) участие в формировании новых пред ложений. С другой стороны новая модель
                        организационной деятельности позволяет оценить значение</p>
                </div>
            </div>

            <div class="main-content__chat-message  main-content__chat-message--new  main-content__chat-message--service">
                <div class="main-content__chat-photo-container">
                    <img src="/img/user-anchor.jpg" alt="photo">
                </div>

                <div class="main-content__chat-text-container">
                    <h4 class="main-content__chat-name">Admin</h4>

                    <span class="main-content__chat-time">01 / 01 / 2019 19:00</span>

                    <p class="main-content__chat-paragraph">Равным образом постоянный количественный рост и сфера нашей
                        активности позволяет оценить значение позиций, занимаемых участниками в отношении поставленных
                        задач.</p>
                </div>
            </div>

            <div class="main-content__chat-write-container">

                <label class="white-square__input-label  white-square__input-label--bold">Написать сообщение:</label>

                <textarea class="white-square__input" rows="5"></textarea>

                <div class="white-square__input-container  main-content__chat-file-container">
                    <span class="white-square__input-label">Добавить файл (макс. размер 10 MЬ)</span>

                    <label class="main-content__upload-container">
                        <input class="hidden-input" type="file">

                        <span class="main-content__upload-btn">Обзор...</span>

                        <span class="main-content__upload-message">Файл не выбран</span>
                    </label>
                </div>

                <button class="btn  btn__green" type="button">Отправить</button>
            </div>

           
@endsection
