@extends('assembly.layouts.assemblyLayout')

@section('content')


    <section class="breadcrumbs">
        <div class="breadcrumbs__list">
            <a class="breadcrumbs__item" href="orders.html">Заказы</a>

            <span class="breadcrumbs__item  breadcrumbs__item--current">Заказ №225584</span>
        </div>
    </section>

    <div class="main-content__top-container">
        <h1 class="main-content__title">Заказ №225584</h1>

        <span class="btn btn--status btn__half-blue">Идет доставка</span>
    </div>

    <input type="radio" class="hidden-input" id="order-inner-details" name="order-inner-tabs" checked>
    <input type="radio" class="hidden-input" id="order-inner-composition" name="order-inner-tabs">

    <div class="tabs-container">
        <label class="tabs-container__btn" for="order-inner-details">Подробности о заказе</label>
        <label class="tabs-container__btn" for="order-inner-composition">Состав заказа</label>
    </div>


    <div class="main-content__store-info  main-content__order-split  main-content__order-split--flex-start"
         id="for-order-inner-details">
        <div class="main-content__gray-section">

            <div class="main-content__gray-top  split-container  split-container--space-between">
                <h2 class="main-content__gray-title">Состав заказа № 35631</h2>
                <h2 class="main-content__gray-title">Номер пакета № 0001</h2>
            </div>


            <div class="main-content__gray-bottom">
                <ul class="main-content__profile-data-list  main-content__profile-data-list--col-1">
                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">Время
                        доставки:
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">Сегодня в
                        15:00
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">Способ оплаты:
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">Наличными
                        курьеру
                    </li>


                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">Клиент:</li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">Юлия
                        Колот
                        <a class="main-content__table-link" href="#">(+79630007007)</a>
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title"> Тип
                        устройства:
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">IOS</li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                        Доставка:
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">Курьером</li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">Город:</li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">Киров</li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">Улица:
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                        Московская ул .123
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">Квартира:
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                        23
                    </li>
                </ul>
            </div>

        </div>

        <div class="main-content__gray-section">

            <div class="main-content__gray-top">
                <h2 class="main-content__gray-title">Итого</h2>
            </div>

            <div class="main-content__gray-bottom">
                <ul class="main-content__profile-data-list">
                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                        Наименований:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">3</li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                        Вес:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">1234
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">Сумма по
                        чеку:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">650.78
                        руб
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                        Доставка:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">0 руб
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                        Промокод:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value"><a
                            class="main-content__table-link"
                            href="#">Отсутствует</a>
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">Итого:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">650.78
                        руб
                    </li>
                </ul>
            </div>


        </div>

        <div class="main-content__gray-section">

            <div class="main-content__gray-top">
                <h2 class="main-content__gray-title">Информация о персонале:</h2>
            </div>

            <div class="main-content__gray-bottom">
                <ul class="main-content__profile-data-list">
                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                        Сборщик:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                        <a href="" class="main-content__table-link">Белокаменский Алексей
                            Игоревич</a>
                        <br>
                        +79630007007
                        <br>
                        <a href="#" class="main-content__table-link">hoty@gmail.com</a>
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                        Курьер:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                        <a href="" class="main-content__table-cell--red">Не назначен</a>
                    </li>

                </ul>
            </div>


        </div>

        <div class="main-content__dropdown-container--wide">
            <a class="btn btn__green" href="#">Клиент забрал заказ</a>
        </div>
    </div>

    <div id="for-order-inner-composition">

        <table class="main-content__table  paragraph--margin-bottom">
            <tr class="main-content__header-row">
                <th class="main-content__header-cell">
                    <button class="main-content__table-sort-btn" type="button">Товар</button>
                </th>

                <th class="main-content__header-cell">
                    <button class="main-content__table-sort-btn" type="button">Наименование</button>
                </th>

                <th class="main-content__header-cell">
                    <button class="main-content__table-sort-btn" type="button">Количество</button>
                </th>

                <th class="main-content__header-cell">Действия</th>
            </tr>

            <tr class="main-content__table-row">
                <td class="main-content__table-cell">
                    <img class="main-content__table-product-img"
                         src="/img/product-anchor.jpg" alt="photo">
                </td>

                <td class="main-content__table-cell  main-content__table-cell--padding">
                    <a class="main-content__table-link" href="goods-inner.html">Шоколадное яйцо Макси Kinder
                        FERRORO, 100г</a>
                    <br>
                    <span class="main-content__table-count">Арт. 40505848</span>
                </td>

                <td class="main-content__table-cell  main-content__table-cell--padding">254 шт</td>

                <td class="main-content__table-cell  main-content__table-cell--padding  main-content__table-cell--product">
                    <button class="checkbox" type="button"></button>
                </td>
            </tr>
        </table>

        <a class="btn btn__green" href="#">Заказ собран отправить в доставку</a>
    </div>


@endsection
