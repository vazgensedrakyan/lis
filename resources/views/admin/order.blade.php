@extends('admin.layouts.adminLayout')

@section('content')

    <section class="breadcrumbs">
        <div class="breadcrumbs__list">
            <a class="breadcrumbs__item" href="orders.html">Заказы</a>

            <span class="breadcrumbs__item  breadcrumbs__item--current">Заказ №225584</span>
        </div>
    </section>

    <div class="main-content__top-container">
        <h1 class="main-content__title">Заказ №225584</h1>

        <span class="main-content__order-status  main-content__order-status--green">В процессе</span>
        <!--<span class="main-content__order-status  main-content__order-status&#45;&#45;red">Отменен</span>-->
        <!--<span class="main-content__order-status  main-content__order-status&#45;&#45;white">Ожидание</span>-->
    </div>

    <div class="main-content__store-info  main-content__order-split  main-content__order-split--flex-start">
        <div class="main-content__gray-section">

            <div class="main-content__gray-top">
                <h2 class="main-content__gray-title">Состав заказа № 35631 Номер пакета № 0001</h2>
            </div>

            <div class="main-content__gray-bottom">
                <ul class="main-content__profile-data-list  main-content__profile-data-list--col-1">
                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">Способ
                        оплаты:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">Оплата
                        картой
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                        Клиент:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">Юлия
                        Колот
                        <a class="main-content__table-link" href="#">(+79630007007)</a>
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">Тип
                        устройства:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">Web</li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                        Способ доставки:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">Курьером
                        / Самовывоз
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">Адрес/
                        магазин:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">Киров /
                        Магазин и его адрес
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">Улица:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                        Московская ул .123
                    </li>
                </ul>
            </div>

        </div>

        <div class="main-content__gray-section">

            <div class="main-content__gray-top">
                <h2 class="main-content__gray-title">Итого</h2>
            </div>

            <div class="main-content__gray-bottom">
                <ul class="main-content__profile-data-list">
                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                        Наименований:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">3</li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                        Вес:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">1234
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">Сумма по
                        чеку:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">650.78
                        руб
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                        Доставка:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">0 руб
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                        Бонусы:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value"><span
                            class="main-content__table-product-name">+123</span>
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">Итого:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">650.78
                        руб
                    </li>
                </ul>
            </div>


        </div>

        <div class="main-content__gray-section">

            <div class="main-content__gray-top">
                <h2 class="main-content__gray-title">Информация о персонале:</h2>
            </div>

            <div class="main-content__gray-bottom">
                <ul class="main-content__profile-data-list">
                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                        Сборщик:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                        <a href="" class="main-content__table-link">Белокаменский Алексей
                            Игоревич</a>
                        <br>
                        +79630007007
                        <br>
                        <a href="#" class="main-content__table-link">hoty@gmail.com</a>
                    </li>

                    <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                        Курьер:
                    </li>
                    <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                        <a href="" class="main-content__table-link">Белокаменский Алексей
                            Игоревич</a>
                        <br>
                        +79630007007
                        <br>
                        <a href="#" class="main-content__table-link">hoty@gmail.com</a>
                    </li>

                </ul>
            </div>


        </div>
    </div>

    <div class="main-content__gray-section">

        <div class="main-content__gray-top">
            <h2 class="main-content__gray-title">Состав заказа № 35631 в FreshMarket</h2>
        </div>

        <div class="main-content__gray-bottom  main-content__gray-bottom--start">


            <table class="main-content__table  main-content__shops-table">
                <tr class="main-content__header-row">
                    <th class="main-content__header-cell">№</th>

                    <th class="main-content__header-cell">Фото</th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn" type="button">Артикул</button>
                    </th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn" type="button">Наименование</button>
                    </th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn" type="button">Цена</button>
                    </th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn" type="button">Кол-во</button>
                    </th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn" type="button">Сумма</button>
                    </th>
                </tr>

                <tr class="main-content__table-row">
                    <td class="main-content__table-cell  main-content__table-cell--padding">1</td>

                    <td class="main-content__table-cell  main-content__table-cell--preview">
                        <img class="main-content__table-product-img" src="/img/product-anchor.jpg" alt="photo">

                        <div class="main-content__product-preview-container">
                            <img src="/img/product-anchor.jpg" alt="photo">
                        </div>
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <span class="main-content__table-count">405058480</span>
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding"><p class="main-content__table-product-name">Шоколадное яйцо Макси Kinder FERRORO, 100г</p></td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">73,99 руб.</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">1</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">73,99 руб.</td>
                </tr>
            </table>

        </div>
    </div>



    <section class="popup">

        <section class="popup__promocode">
            <h4 class="popup__title">Создать промокод</h4>

            <div class="popup__promo-dropdowns-container">
                <div class="white-square__input-container  popup__input-container">
                    <label class="white-square__input-label">Создать код</label>

                    <input type="text" class="white-square__input  popup__input">
                </div>

                <div class="main-content__dropdown-container  popup__dropdown-container">
                    <span class="main-content__dropdown-label">Скидка</span>

                    <div class="main-content__dropdown-inner-container">
                        <span class="main-content__dropdown-current  popup__dropdown-current">20%</span>

                        <ul class="main-content__dropdown-list">
                            <li class="main-content__dropdown-item  popup__dropdown-item">1%</li>
                            <li class="main-content__dropdown-item  popup__dropdown-item">5%</li>
                            <li class="main-content__dropdown-item  popup__dropdown-item">10%</li>
                        </ul>
                    </div>
                </div>

                <div class="main-content__dropdown-container  popup__dropdown-container">
                <span class="main-content__dropdown-label">Активность
                    <span class="main-content__dropdown-label-gray">(да/нет)</span>
                </span>

                    <div class="main-content__dropdown-inner-container">
                        <span class="main-content__dropdown-current  popup__dropdown-current">Да</span>

                        <ul class="main-content__dropdown-list">
                            <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                            <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                        </ul>
                    </div>
                </div>

                <div class="main-content__dropdown-container  popup__dropdown-container">
                    <span class="main-content__dropdown-label">Бесплатная доставка</span>

                    <div class="main-content__dropdown-inner-container">
                        <span class="main-content__dropdown-current  popup__dropdown-current">Да</span>

                        <ul class="main-content__dropdown-list">
                            <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                            <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="popup__datetime-container">
                <div class="white-square__input-container  popup__data-range-container">
                    <label class="white-square__input-label">Активен с <span class="popup__required">*</span></label>

                    <input type="text" class="white-square__input  popup__input  popup__input-from  datepicker-here"
                           data-position="right bottom" data-timepicker="true">
                </div>

                <div class="white-square__input-container  popup__data-range-container">
                    <label class="white-square__input-label">Активен по <span class="popup__required">*</span></label>

                    <input type="text" class="white-square__input  popup__input  datepicker-here"
                           data-position="right bottom" data-timepicker="true">
                </div>
            </div>

            <button class="green-btn  popup-submit" type="button">Создать</button>

            <button class="popup__close" type="button">Закрыть</button>
        </section>

    </section>

@endsection
