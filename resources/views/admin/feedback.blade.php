@extends('admin.layouts.adminLayout')

@section('content')




            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <span class="breadcrumbs__item  breadcrumbs__item--current">Отзывы</span>
                </div>
            </section>

            <div class="main-content__top-container">
                <h1 class="main-content__title">Отзывы</h1>
            </div>

            <input type="radio" class="hidden-input" id="reviews-for-goods" name="reviews-for" checked>
            <input type="radio" class="hidden-input" id="reviews-for-couriers" name="reviews-for">

            <div class="tabs-container">
                <label class="tabs-container__btn" for="reviews-for-goods">Товары</label>
                <label class="tabs-container__btn" for="reviews-for-couriers">Курьеры</label>
            </div>

            <div id="for-reviews-for-goods">

                <div class="split-container  split-container--align-center split-container--space-between">
                    <div class="split-container  half">
                        <div class="main-content__dropdown-container  main-content__dropdown-container--margin">
                <span class="main-content__dropdown-label">Выберите товар
                </span>

                            <div class="main-content__dropdown-inner-container">
                                <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                                <ul class="main-content__dropdown-list">
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                                </ul>
                            </div>
                        </div>

                        <div class="white-square__input-container  split-container__inner--fixed-short">
                            <label class="white-square__input-label">Дата отправки</label>

                            <input type="text" class="white-square__input  popup__input  datepicker-here"
                                   data-position="right bottom" data-timepicker="true">
                        </div>
                    </div>

                    <div class="main-content__search-container">
                        <input class="main-content__search-input" type="text" placeholder="Искать...">
                        <button class="main-content__search-btn" type="button"></button>
                    </div>
                </div>

                <table class="main-content__table">
                    <tr class="main-content__header-row">
                        <th class="main-content__header-cell  main-content__header-cell--nowrap">
                            <button class="main-content__table-sort-btn" type="button">Дата</button>
                        </th>
                        <th class="main-content__header-cell  main-content__header-cell--nowrap">
                            <button class="main-content__table-sort-btn" type="button">№ Заказа</button>
                        </th>

                        <th class="main-content__header-cell">
                            Клиент
                        </th>

                        <th class="main-content__header-cell">Категория / подкатегория товара</th>

                        <th class="main-content__header-cell">
                            <button class="main-content__table-sort-btn" type="button">
                                Товар
                            </button>
                        </th>

                        <th class="main-content__header-cell">Комментарий клиента</th>

                        <th class="main-content__header-cell">Действия</th>
                    </tr>

                    <tr class="main-content__table-row">
                        <td class="main-content__table-cell  main-content__table-cell--padding">07.02.2019 16:54</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <a class="main-content__id  main-content__id--link" href="order-inner.html">225584</a>
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <a href="" class="main-content__table-link">Белокаменский Алексей
                                Игоревич</a>
                            <br>
                            +79630007007
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">Название категории Подкатегория товара</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <div class="main-content__chat-link">
                                <div class="main-content__chat-link-img-container">
                                    <img src="/img/product-anchor.jpg" alt="photo">
                                </div>

                                <div class="main-content__chat-link-name-container">
                                    <a class="main-content__table-link" href="goods-inner.html">Шоколадное яйцо Макси Kinder
                                        FERRORO, 100г</a>
                                    <br>
                                    <span class="main-content__table-count">Арт. 40505848</span>
                                </div>
                            </div>
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">Задача организации, в особенности же постоянное информационно-п</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <button class="main-content__table-btn  main-content__table-delete-btn  js--delete-row" type="button"></button>
                            <button class="main-content__table-btn  main-content__table-msg-btn  js--add-msg" type="button"></button>
                        </td>
                    </tr>
                </table>

            </div>

            <div id="for-reviews-for-couriers">

                <div class="split-container  split-container--align-center split-container--space-between">
                    <div class="split-container  half">
                        <div class="main-content__dropdown-container  main-content__dropdown-container--margin">
                <span class="main-content__dropdown-label">Выберите курьера
                </span>

                            <div class="main-content__dropdown-inner-container">
                                <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                                <ul class="main-content__dropdown-list">
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                                </ul>
                            </div>
                        </div>


                        <div class="main-content__dropdown-container  main-content__dropdown-container--margin">
                <span class="main-content__dropdown-label">Сортировка
                </span>

                            <div class="main-content__dropdown-inner-container">
                                <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                                <ul class="main-content__dropdown-list">
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="main-content__search-container">
                        <input class="main-content__search-input" type="text" placeholder="Искать...">
                        <button class="main-content__search-btn" type="button"></button>
                    </div>
                </div>

                <table class="main-content__table">
                    <tr class="main-content__header-row">
                        <th class="main-content__header-cell  main-content__header-cell--nowrap">
                            <button class="main-content__table-sort-btn" type="button">№ Заказа</button>
                        </th>

                        <th class="main-content__header-cell  main-content__header-cell--nowrap">
                            <button class="main-content__table-sort-btn" type="button">№ Пакета</button>
                        </th>

                        <th class="main-content__header-cell">
                            Курьер
                        </th>

                        <th class="main-content__header-cell">
                            <button class="main-content__table-sort-btn" type="button">
                                Оценка
                            </button>
                        </th>

                        <th class="main-content__header-cell">Комментарий</th>

                        <th class="main-content__header-cell">
                            <button class="main-content__table-sort-btn" type="button">
                                Кто поставил
                            </button>
                        </th>

                        <th class="main-content__header-cell">
                            <button class="main-content__table-sort-btn" type="button">
                                Дата
                            </button>
                        </th>

                        <th class="main-content__header-cell">Действия</th>
                    </tr>

                    <tr class="main-content__table-row">
                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <a class="main-content__id  main-content__id--link" href="order-inner.html">225584</a>
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">225584</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <a href="courier-inner.html" class="main-content__table-link">Белокаменский Алексей
                                Игоревич</a>
                            <br>
                            +79630007007
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding  main-content__table-cell--mark">
                            <span class="main-content__profile-star"></span>
                            <span class="main-content__profile-star"></span>
                            <span class="main-content__profile-star"></span>
                            <span class="main-content__profile-star"></span>
                            <span class="main-content__profile-star  main-content__profile-star--empty"></span>
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">Задача организации, в особенности же постоянное информационно-п</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <a href="client-inner.html" class="main-content__table-link">Белокаменский Алексей
                                Игоревич</a>
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">07.02.2019 16:54</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <button class="main-content__table-btn  main-content__table-delete-btn  js--delete-row" type="button"></button>
                            <button class="main-content__table-btn  main-content__table-msg-btn  js--add-msg" type="button"></button>
                        </td>
                    </tr>
                </table>

            </div>




    @push('popup')
        <section class="popup">

            <section class="popup__add-msg">
                <h4 class="popup__title">Создать уведомление</h4>


                <div class="main-content__store-photo--margin-bottom">
                    <div class="split-container  split-container--align-center  split-container--space-between">

                        <div class="white-square__input-container  split-container__inner--fixed-short">
                            <label class="white-square__input-label">Дата отправки</label>

                            <input type="text" class="white-square__input  popup__input  datepicker-here"
                                   data-position="right bottom">
                        </div>

                        <div class="paragraph--margin-bottom  ">
                            <span class="main-content__dropdown-label">Время</span>

                            <div class="main-content__dropdown-inner-container">
                                <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                                <ul class="main-content__dropdown-list">
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                                </ul>
                            </div>
                        </div>

                        <div class="paragraph--margin-bottom  ">
                <span class="main-content__dropdown-label">Тип сообщения
                </span>

                            <div class="main-content__dropdown-inner-container">
                                <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                                <ul class="main-content__dropdown-list">
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <input type="radio" class="hidden-input" id="push-add-for-user" name="push-add-type" checked>
                    <input type="radio" class="hidden-input" id="push-add-for-manager" name="push-add-type">


                    <div class="paragraph--margin-bottom">
                        <span class="main-content__dropdown-label">Тип пользователя</span>

                        <div class="main-content__dropdown-inner-container">
                            <span class="main-content__dropdown-current  popup__dropdown-current">Клиенты</span>

                            <div class="main-content__dropdown-list  main-content__dropdown-list--flex">
                                <label class="main-content__dropdown-item  popup__dropdown-item"
                                       for="push-add-for-user">Клиенты</label>
                                <label class="main-content__dropdown-item  popup__dropdown-item" for="push-add-for-manager">Менеджеры</label>
                            </div>
                        </div>

                    </div>

                    <div id="for-push-add-for-user">
                        <div class="split-container  split-container--align-center  split-container--space-between">
                            <div class="paragraph--margin-bottom  half">
                                <span class="main-content__dropdown-label">Группа</span>

                                <div class="main-content__dropdown-inner-container">
                                    <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                                    <ul class="main-content__dropdown-list">
                                        <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                        <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="paragraph--margin-bottom  half">
                                <span class="main-content__dropdown-label">Статус</span>

                                <div class="main-content__dropdown-inner-container">
                                    <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                                    <ul class="main-content__dropdown-list">
                                        <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                        <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="white-square__input-container">
                        <label class="white-square__input-label">
                            Текст сообщения
                        </label>

                        <textarea class="white-square__input" rows="4"></textarea>
                    </div>


                    <button class="checkbox" type="button">Сохранить в шаблоны</button>
                </div>


                <button class="green-btn  green-btn--profile  popup-submit" type="button">Создать</button>

                <button class="popup__close" type="button">Закрыть</button>
            </section>

            <section class="popup__delete">
                <h4 class="popup__title">Вы уверены, что хотите удалить?</h4>

                <button class="green-btn  green-btn--profile  popup-submit" type="button">Удалить</button>
                <button class="red-btn  red-btn--profile  popup-submit" type="button">Отменить</button>

                <button class="popup__close" type="button">Закрыть</button>
            </section>
        </section>
    @endpush

@endsection
