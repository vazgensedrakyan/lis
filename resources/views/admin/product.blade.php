@extends('admin.layouts.adminLayout')

@section('content')


    <section class="breadcrumbs">
        <div class="breadcrumbs__list">
            <a class="breadcrumbs__item" href="goods.html">Товары</a>

            <span class="breadcrumbs__item  breadcrumbs__item--current">Товар № 0001</span>
        </div>
    </section>

    <div class="main-content__top-container">
        <h1 class="main-content__title">Товар № 0001</h1>
        {{--<button class="yellow-btn  yellow-btn--profile" type="button">Редактировать</button>--}}
    </div>

    <div class="main-content__store-info">
        <div class="main-content__store-photo">
            <div class="main-content__store-photo-container">
                <h3 class="main-content__store-photo-title">Обложка магазина</h3>
                <p class="main-content__store-photo-description">(фото должно быть до 3Mb)</p>
            </div>

            <button class="main-content__store-photo-delete" type="button"></button>

            <label class="main-content__upload-container">
                <input class="hidden-input" type="file">

                <span class="main-content__upload-btn">Обзор...</span>

                <span class="main-content__upload-message">Файл не выбран</span>
            </label>
        </div>

        <div class="main-content__store-data split-container  split-container--wrap">
            <h2 class="main-content__profile-name  main-content__dropdown-container--wide  main-content__dropdown-container--margin">
                Название товара</h2>

            <ul class="main-content__profile-data-list  split-container__inner--fixed-half  paragraph--margin-bottom">
                <li class="main-content__profile-data-item  main-content__profile-data-item--title">№:</li>
                <li class="main-content__profile-data-item  main-content__profile-data-item--value">00001</li>

                <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                    Цена:
                </li>
                <li class="main-content__profile-data-item  main-content__profile-data-item--value">Москва</li>

                <li class="main-content__profile-data-item  main-content__profile-data-item--title">Кол-во:</li>
                <li class="main-content__profile-data-item  main-content__profile-data-item--value">1254 шт.
                </li>

                <li class="main-content__profile-data-item  main-content__profile-data-item--title">Сумма:</li>
                <li class="main-content__profile-data-item  main-content__profile-data-item--value">147,8 руб.
                </li>

                <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                    Активность:
                </li>
                <li class="main-content__profile-data-item  main-content__profile-data-item--value">01 / 03 /
                    2019
                </li>
            </ul>

            <ul class="main-content__profile-data-list  main-content__profile-data-list--col-3  paragraph--margin-bottom">
                <li class="main-content__profile-data-item  main-content__profile-data-item--title"> Статус:
                </li>

                <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                    <button class="switcher  switcher--active" type="button"></button>
                    <span>Опубликован</span>
                </li>

                <li class="main-content__profile-data-item  main-content__profile-data-item--title">В
                    категории:
                </li>

                <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                    <ul class="main-content__profile-data-sublist  main-content__profile-data-sublist--column">
                        <li class="main-content__profile-data-subitem">
                            <a class="main-content__table-link" href="goods-inner.html">Сладости</a>
                        </li>
                        <li class="main-content__profile-data-subitem">
                            <a class="main-content__table-link" href="goods-inner.html">Детское питание</a>
                        </li>
                        <li class="main-content__profile-data-subitem">
                            <a class="main-content__table-link" href="goods-inner.html"> Шоколадные конфеты</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <ul class="main-content__profile-data-list  main-content__profile-data-list--wide">
                <li class="main-content__profile-data-item  main-content__profile-data-item--title">Описание Товара</li>
                <li class="main-content__profile-data-item  main-content__profile-data-item--value">Сухой корм
                    предназначен для того, чтобы сбалансировать рацион питания котов. Хрустящие подушечки с тунцом
                    обязательно придутся по вкусу домашнему любимцу. Кроме того, корм содержит все необходимое, чтобы
                    еда животного была не только вкусной, но и полезной. Оптимальное сочетание питательных веществ и
                    нутриентов поддерживает обмен веществ, укрепляет иммунитет и способствует здоровой и счастливой
                    жизни животного. В упаковке 300 г корма.
                </li>
            </ul>
        </div>
    </div>


    <div class="main-content__profile-reviews  main-content__profile-reviews--margin-top">
        <div class="main-content__top-container  main-content__top-container--table">
            <h4 class="subtitle">Отзывы о
                товаре</h4>

            <div class="main-content__search-container">
                <input class="main-content__search-input" type="text" placeholder="Искать...">
                <button class="main-content__search-btn" type="button"></button>
            </div>
        </div>

        <table class="main-content__table">
            <tr class="main-content__header-row">
                <th class="main-content__header-cell">
                    <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">
                        Дата
                    </button>
                </th>

                <th class="main-content__header-cell">
                    <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">№
                        Заказа
                    </button>
                </th>

                <th class="main-content__header-cell">
                    <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">
                        Клиент
                    </button>
                </th>

                <th class="main-content__header-cell">Комментарий клиента</th>

                <th class="main-content__header-cell">
                    <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">
                        Действия
                    </button>
                </th>
            </tr>

            <tr class="main-content__table-row">
                <td class="main-content__table-cell  main-content__table-cell--padding">07.02.2019 16:54</td>

                <td class="main-content__table-cell  main-content__table-cell--padding">296</td>

                <td class="main-content__table-cell  main-content__table-cell--padding">
                    Славик
                    <br>
                    <b class="main-content__table-product-name">+ 37 098 456 56 78</b>
                </td>

                <td class="main-content__table-cell  main-content__table-cell--padding  main-content__table-cell--category-wide">
                    Задача организации, в особенности же постоянное информационно-пропагандистское обеспечение нашей.
                    обеспечивает широкому кругу (специалистов) участие в формировании существенных финансовых и
                    административных условий.
                    Повседневная практика показывает, что сложившаяся
                </td>

                <td class="main-content__table-cell  main-content__table-cell--padding">
                    <button class="main-content__table-btn  main-content__table-delete-btn  js--delete-row"
                            type="button"></button>
                    <button class="main-content__table-btn  main-content__table-msg-btn  js--add-msg"
                            type="button"></button>
                </td>
            </tr>
        </table>

    </div>


    <section class="popup">
        <section class="popup__add-msg">
            <h4 class="popup__title">Создать уведомление</h4>


            <div class="main-content__store-photo--margin-bottom">
                <div class="split-container  split-container--align-center  split-container--space-between">

                    <div class="white-square__input-container  split-container__inner--fixed-short">
                        <label class="white-square__input-label">Дата отправки</label>

                        <input type="text" class="white-square__input  popup__input  datepicker-here"
                               data-position="right bottom">
                    </div>

                    <div class="paragraph--margin-bottom  ">
                        <span class="main-content__dropdown-label">Время</span>

                        <div class="main-content__dropdown-inner-container">
                            <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                            <ul class="main-content__dropdown-list">
                                <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                            </ul>
                        </div>
                    </div>

                    <div class="paragraph--margin-bottom  ">
                <span class="main-content__dropdown-label">Тип сообщения
                </span>

                        <div class="main-content__dropdown-inner-container">
                            <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                            <ul class="main-content__dropdown-list">
                                <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <input type="radio" class="hidden-input" id="push-add-for-user" name="push-add-type" checked>
                <input type="radio" class="hidden-input" id="push-add-for-manager" name="push-add-type">


                <div class="paragraph--margin-bottom">
                    <span class="main-content__dropdown-label">Тип пользователя</span>

                    <div class="main-content__dropdown-inner-container">
                        <span class="main-content__dropdown-current  popup__dropdown-current">Клиенты</span>

                        <div class="main-content__dropdown-list  main-content__dropdown-list--flex">
                            <label class="main-content__dropdown-item  popup__dropdown-item"
                                   for="push-add-for-user">Клиенты</label>
                            <label class="main-content__dropdown-item  popup__dropdown-item" for="push-add-for-manager">Менеджеры</label>
                        </div>
                    </div>

                </div>

                <div id="for-push-add-for-user">
                    <div class="split-container  split-container--align-center  split-container--space-between">
                        <div class="paragraph--margin-bottom  half">
                            <span class="main-content__dropdown-label">Группа</span>

                            <div class="main-content__dropdown-inner-container">
                                <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                                <ul class="main-content__dropdown-list">
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                                </ul>
                            </div>
                        </div>

                        <div class="paragraph--margin-bottom  half">
                            <span class="main-content__dropdown-label">Статус</span>

                            <div class="main-content__dropdown-inner-container">
                                <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                                <ul class="main-content__dropdown-list">
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="white-square__input-container">
                    <label class="white-square__input-label">
                        Текст сообщения
                    </label>

                    <textarea class="white-square__input" rows="4"></textarea>
                </div>


                <button class="checkbox" type="button">Сохранить в шаблоны</button>
            </div>


            <button class="green-btn  green-btn--profile  popup-submit" type="button">Создать</button>

            <button class="popup__close" type="button">Закрыть</button>
        </section>

    </section>




@endsection
