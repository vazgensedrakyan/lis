@extends('admin.layouts.adminLayout')

@section('content')

    <section class="breadcrumbs">
        <div class="breadcrumbs__list">
            <span class="breadcrumbs__item  breadcrumbs__item--current">Заказы</span>
        </div>
    </section>

    <div class="main-content__top-container">
        <h1 class="main-content__title">Заказы</h1>

        <div class="main-content__search-container">
            <input class="main-content__search-input" type="text" placeholder="Искать...">
            <button class="main-content__search-btn" type="button"></button>
        </div>
    </div>

    <div class="popup__split-container">
        <div class="white-square__input-container  main-content__data-range-container">

            <label class="white-square__input-label">
                Выберите период
            </label>

            <input type="text" class="white-square__input  popup__input  datepicker-here"
                   data-position="right bottom" data-timepicker="true" data-range="true"
                   data-multiple-dates-separator=" - ">
        </div>

        <div class="main-content__dropdown-container  popup__dropdown-container  main-content__dropdown-container--margin">
                <span class="main-content__dropdown-label">Статус заказа
                </span>

            <div class="main-content__dropdown-inner-container">
                <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                <ul class="main-content__dropdown-list">
                    <li class="main-content__dropdown-item  popup__dropdown-item">Все</li>
                    <li class="main-content__dropdown-item  popup__dropdown-item">В ожидании</li>
                    <li class="main-content__dropdown-item  popup__dropdown-item">Идет сборка</li>
                    <li class="main-content__dropdown-item  popup__dropdown-item">Собран</li>
                    <li class="main-content__dropdown-item  popup__dropdown-item">Идет доставка</li>
                    <li class="main-content__dropdown-item  popup__dropdown-item">Доставлен</li>
                    <li class="main-content__dropdown-item  popup__dropdown-item">Доставлен (оставлен консъержу)</li>
                    <li class="main-content__dropdown-item  popup__dropdown-item">Завершен</li>
                    <li class="main-content__dropdown-item  popup__dropdown-item">На хранении (самовывоз)</li>
                </ul>
            </div>
        </div>
    </div>

    <table class="main-content__table">
        <tr class="main-content__header-row">
            <th class="main-content__header-cell  main-content__header-cell--nowrap">
                <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">№ Заказа</button>
            </th>

            <th class="main-content__header-cell  main-content__header-cell--nowrap">№ Пакета</th>

            <th class="main-content__header-cell">
                Вес
            </th>

            <th class="main-content__header-cell">Заказчик</th>

            <th class="main-content__header-cell">Адрес доставки</th>

            <th class="main-content__header-cell">
                <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">
                    Статус
                </button>
            </th>

            <th class="main-content__header-cell">
                <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort-up" type="button">Доставка</button>
            </th>

            <th class="main-content__header-cell">
                <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort-down" type="button">Чек</button>
            </th>

            <th class="main-content__header-cell">Курьер</th>

            <th class="main-content__header-cell">Сборщик</th>
        </tr>

        @foreach($orders as $order)
        <tr class="main-content__table-row">
            <td class="main-content__table-cell  main-content__table-cell--padding">
                <a class="main-content__id  main-content__id--link" href="{{ route("order.show", ["id" => $order->id]) }}">{{ $order->id }}</a>
            </td>

            <td class="main-content__table-cell  main-content__table-cell--padding">154887</td>

            <td class="main-content__table-cell  main-content__table-cell--padding">{{ $order->weight }} {{$order->unit}}</td>

            <td class="main-content__table-cell  main-content__table-cell--padding">
                <a href="{{ route('client', ['id' => $order->client->id]) }}" class="main-content__table-link">
                    {{ $order->client->name }}
                </a>
                <br>
                {{ $order->client->phone }}
                <br>
                <a href="#" class="main-content__table-link">{{ $order->client->email }}</a>
            </td>

            <td class="main-content__table-cell  main-content__table-cell--padding">
                {{ $order->address }}
            </td>

            <td class="main-content__table-cell  main-content__table-cell--padding">{{ $order->status }}</td>

            <td class="main-content__table-cell  main-content__table-cell--padding">
                <span class="main-content__activated-from"> {{ $order->delivery_from->format('d.m.Y H:s') }}</span>
                <span>{{ $order->delivery_to->format('d.m.Y H:s') }}</span>
            </td>

            <td class="main-content__table-cell  main-content__table-cell--padding">
                {{ $order->price }} {{ $order->currency }} {{ $order->payment_method }}
            </td>

            <td class="main-content__table-cell  main-content__table-cell--padding">
                <a href="{{ route('courier.show', ['id' => $order->courier->id]) }}" class="main-content__table-link">{{ $order->courier->full_name }}</a>
            </td>

            <td class="main-content__table-cell  main-content__table-cell--padding">
                <a href="{{ route('picker.show', ['id' => $order->picker->id]) }}" class="main-content__table-link">{{ $order->picker->full_name }}</a>
            </td>
        </tr>
            @endforeach
    </table>

@endsection
