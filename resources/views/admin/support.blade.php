@extends('admin.layouts.adminLayout')

@section('content')


            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <a class="breadcrumbs__item" href="support.html">Поддержка</a>

                    <span class="breadcrumbs__item  breadcrumbs__item--current">Поправьте фичу которую вы сделали случайно в моей учетной записи</span>
                </div>
            </section>

            <div class="main-content__top-container">
                <h1 class="main-content__title">Поправьте фичу которую вы сделали случайно в моей учетной записи</h1>
            </div>

            <div class="main-content__chat-container">
                <div class="main-content__chat-left-col">

                    <div class="main-content__chat-message  main-content__chat-message--user">
                        <div class="main-content__chat-photo-container">
                            <img src="/img/user-anchor.jpg" alt="photo">
                        </div>

                        <div class="main-content__chat-text-container">
                            <h4 class="main-content__chat-name">Александр Пушкин</h4>

                            <span class="main-content__chat-time">01 / 01 / 2019 18:55</span>

                            <p class="main-content__chat-paragraph">Равным образом постоянный количественный рост и сфера нашей активности позволяет оценить
                                значение пози ций, занимаемых участниками в отношении поставленных задач. Таким образом
                                начало повседневной работы по формированию позиции обеспечивает широкому кругу
                                (специалистов) участие в формировании новых пред ложений. С другой стороны новая модель
                                организационной деятельности позволяет оценить значение</p>
                        </div>
                    </div>

                    <div class="main-content__chat-message  main-content__chat-message--service">
                        <div class="main-content__chat-photo-container">
                            <img src="/img/user-anchor.jpg" alt="photo">
                        </div>

                        <div class="main-content__chat-text-container">
                            <h4 class="main-content__chat-name">Admin</h4>

                            <span class="main-content__chat-time">01 / 01 / 2019 19:00</span>

                            <p class="main-content__chat-paragraph">Равным образом постоянный количественный рост и сфера нашей активности позволяет оценить значение позиций, занимаемых участниками в отношении поставленных задач.</p>
                        </div>
                    </div>

                    <div class="main-content__chat-write-container">

                        <label class="white-square__input-label  white-square__input-label--bold">Написать сообщение:</label>

                        <textarea class="white-square__input" rows="5"></textarea>

                        <div class="white-square__input-container  main-content__chat-file-container">
                            <span class="white-square__input-label">Добавить файл (макс. размер 10 MЬ)</span>

                            <label class="main-content__upload-container">
                                <input class="hidden-input" type="file">

                                <span class="main-content__upload-btn">Обзор...</span>

                                <span class="main-content__upload-message">Файл не выбран</span>
                            </label>
                        </div>

                        <button class="green-btn  green-btn--small" type="button">Отправить</button>
                    </div>

                </div>

                <div class="main-content__chat-right-col">
                    <button class="green-btn  main-content__dropdown-container--margin" type="button">Отметить решенным</button>

                    <ul class="main-content__chat-info-list">
                        <li class="main-content__chat-info-title">Статус</li>
                        <li class="main-content__chat-info-value">Решен</li>

                        <li class="main-content__chat-info-title">Автор запроса</li>
                        <li class="main-content__chat-info-value">Александр Пушкин</li>

                        <li class="main-content__chat-info-title">Магазин</li>
                        <li class="main-content__chat-info-value">Магазин 1</li>

                        <li class="main-content__chat-info-title">Создана</li>
                        <li class="main-content__chat-info-value">01 / 01 / 2019</li>

                        <li class="main-content__chat-info-title">Активность</li>
                        <li class="main-content__chat-info-value">25 / 04 / 2019</li>

                        <li class="main-content__chat-info-title">ID</li>
                        <li class="main-content__chat-info-value">154887</li>
                    </ul>
                </div>
            </div>


@endsection
