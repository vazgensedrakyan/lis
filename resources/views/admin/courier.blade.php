@extends('admin.layouts.adminLayout')



@section('content')


            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <a class="breadcrumbs__item" href="couriers.html">Курьеры</a>

                    <span class="breadcrumbs__item  breadcrumbs__item--current">Юзер-1</span>
                </div>
            </section>


            <div class="main-content__profile-info">
                <div class="main-content__profile-img-container">
                    <img src="/img/user-anchor.jpg" alt="photo">
                </div>

                <div class="main-content__profile-top">

                    <h2 class="main-content__profile-name">Геогрий Георгиевич</h2>

                    <span class="main-content__order-status  main-content__order-status--green">Онлайн</span>
                    <!--<span class="main-content__order-status  main-content__order-status&#45;&#45;red">Онлайн</span>-->

                    <div>
                        <a class="green-btn  green-btn--profile" href="{{ route("courier_form", ["id" => 1]) }}">Редактировать</a>
                        <button class="yellow-btn  yellow-btn--profile" type="button">Забанить</button>
                        <button class="red-btn  red-btn--profile  js--delete-row" type="button">Удалить</button>
                    </div>

                </div>

                <div class="main-content__profile-data">

                    <ul class="main-content__profile-data-list  main-content__profile-data-list--col-3">
                        <li class="main-content__profile-data-item  main-content__profile-data-item--title"> №:</li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">250</li>

                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                            Зарегистрирован:
                        </li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">01 / 03 /
                            2019
                        </li>

                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">Выполненных заказов:</li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">50</li>

                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">Склад:</li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">Магазин1</li>
                    </ul>

                </div>

                <h2 class="main-content__profile-stats-title">Все заказы</h2>

                <div class="main-content__profile-stats  main-content__order-split--flex-start">
                    <div class="white-square__input-container  main-content__data-range-container">

                        <label class="white-square__input-label">
                            Выберите период
                        </label>

                        <input type="text" class="white-square__input  popup__input  datepicker-here"
                               data-position="right bottom" data-timepicker="true" data-range="true"
                               data-multiple-dates-separator=" - ">
                    </div>
                </div>

                <table class="main-content__table  main-content__store-photo--margin-bottom">
                    <tr class="main-content__header-row">
                        <th class="main-content__header-cell  main-content__header-cell--nowrap">№ Заказа</th>

                        <th class="main-content__header-cell  main-content__header-cell--nowrap">№ Пакета</th>

                        <th class="main-content__header-cell">
                            <button class="main-content__table-sort-btn" type="button">
                                Вес
                            </button>
                        </th>

                        <th class="main-content__header-cell  main-content__header-cell--nowrap">Адрес доставки</th>

                        <th class="main-content__header-cell">
                            <button class="main-content__table-sort-btn" type="button">Стаус</button>
                        </th>

                        <th class="main-content__header-cell">
                            <button class="main-content__table-sort-btn" type="button">Доставка</button>
                        </th>

                        <th class="main-content__header-cell">
                            <button class="main-content__table-sort-btn" type="button">Чек</button>
                        </th>


                        <th class="main-content__header-cell">
                            <button class="main-content__table-sort-btn" type="button">Заказчик</button>
                        </th>
                    </tr>

                    <tr class="main-content__table-row">
                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <a class="main-content__id  main-content__id--link" href="order-inner.html">225584</a>
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">225584</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">123 гр</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">610048, Киров, Московская улица,  дом 153 кв 98</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">Выполняется</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <span class="main-content__activated-from"> 07.02.2019 18:00</span>
                            <span>07.02.2019 18:00</span>
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            1 084 руб наличными курьеру
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <a href="client-inner.html" class="main-content__table-link">Белокаменский Алексей
                                Игоревич</a>
                            <br>
                            +79630007007
                            <br>
                            <a href="#" class="main-content__table-link">hoty@gmail.com</a>
                        </td>
                    </tr>
                </table>


                <div class="main-content__profile-reviews">

                    <div class="main-content__profile-reviews-btns">
                        <h2 class="main-content__profile-stats-title">Отзывы от клиентов</h2>
                    </div>

                    <table class="main-content__table">
                        <tr class="main-content__header-row">
                            <th class="main-content__header-cell">
                                <button class="main-content__table-sort-btn" type="button">№ Заказа</button>
                            </th>

                            <th class="main-content__header-cell">
                                <button class="main-content__table-sort-btn" type="button">№ Заказа</button>
                            </th>

                            <th class="main-content__header-cell">
                                <button class="main-content__table-sort-btn" type="button">Оценка</button>
                            </th>

                            <th class="main-content__header-cell">Комментарий</th>

                            <th class="main-content__header-cell">
                                <button class="main-content__table-sort-btn" type="button">Кто поставил</button>
                            </th>

                            <th class="main-content__header-cell">
                                <button class="main-content__table-sort-btn" type="button">Дата</button>
                            </th>
                        </tr>

                        <tr class="main-content__table-row">
                            <td class="main-content__table-cell  main-content__table-cell--padding">296</td>
                            <td class="main-content__table-cell  main-content__table-cell--padding">296</td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">
                                <span class="main-content__profile-star"></span>
                                <span class="main-content__profile-star"></span>
                                <span class="main-content__profile-star"></span>
                                <span class="main-content__profile-star"></span>
                                <span class="main-content__profile-star  main-content__profile-star--empty"></span>
                            </td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">Нет</td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">
                                <a class="main-content__profile-review-author  main-content__table-link" href="client-inner.html">Артем
                                    Высокий</a>
                            </td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">12 / 03 / 2019</td>
                        </tr>
                    </table>

                </div>
            </div>


@endsection
