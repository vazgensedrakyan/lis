@extends('admin.layouts.adminLayout')

@section('content')

    <section class="breadcrumbs">
        <div class="breadcrumbs__list">
            <a class="breadcrumbs__item" href="{{route('managers')}}">Менеджеры</a>

            <span class="breadcrumbs__item  breadcrumbs__item--current">
                    Новый менеджер
            </span>
        </div>
    </section>

    <div class="main-content__top-container">
        <h1 class="main-content__title">
            Новый менеджер
        </h1>
    </div>
    <form action="{{route('manager.create')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="main-content__store-info">

            <div class="main-content__store-photo">
                <div class="main-content__store-photo-container">
                    <h3 class="main-content__store-photo-title">Фото</h3>
                    <p class="main-content__store-photo-description">(фото должно быть до 3Mb)</p>
                    <img src="" alt="" id="preview-photo">
                </div>

                <button class="main-content__store-photo-delete" type="button"></button>

                <label class="main-content__upload-container">
                    <input class="hidden-input" type="file" name="photo" id="photo">

                    <span class="main-content__upload-btn">Обзор...</span>

                    <span class="main-content__upload-message">Файл не выбран</span>
                </label>
            </div>

            <div class="main-content__store-data  split-container--max-width">
                <div class="white-square__input-container">
                    <label class="white-square__input-label">
                        Имя менеджера
                    </label>

                    <input type="text" class="white-square__input" name="full_name">
                </div>

                <div class="split-container  split-container--space-between">
                    <div class="white-square__input-container  half  white-square__input-container--margin-right">
                        <label class="white-square__input-label">
                            Контактный телефон
                        </label>

                        <input type="text" class="white-square__input" name="phone">
                    </div>

                    <div class="white-square__input-container  half">
                        <label class="white-square__input-label">
                            Почта
                        </label>

                        <input type="email" class="white-square__input" name="email">
                    </div>
                </div>

                <div class="split-container  split-container--space-between">
                    <div class="white-square__input-container  half  white-square__input-container--margin-right">
                        <label class="white-square__input-label">
                            User name доступа к приложению
                        </label>

                        <input type="text" class="white-square__input" name="username">
                    </div>

                    <div class="white-square__input-container  half">
                        <label class="white-square__input-label">
                            Пароль доступа к приложению
                        </label>

                        <input type="password" class="white-square__input" name="password">
                    </div>
                </div>

                <div class="main-content__store-btns-container">
                    <a class="red-btn red-btn--profile" href="{{ route("managers") }}">Отмена</a>
                    <button class="green-btn green-btn--profile">Сохранить</button>
                </div>
            </div>

        </div>

    </form>
@endsection
@push('scripts')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview-photo').attr('src', e.target.result).css('display', 'block');
                    $('.main-content__store-photo-container p, .main-content__store-photo-container h3').attr('src', e.target.result).css('display', 'none');
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).ready(function () {
            $("#photo").change(function () {
                readURL(this);
            });

            $(".main-content__store-photo-delete").click(function (e) {
                e.preventDefault();
                $("#photo").val('');
                $('#preview-photo').css('display', 'none');
                $('.main-content__store-photo-container p, .main-content__store-photo-container h3').attr('src', e.target.result).css('display', 'block');
            });
        })
    </script>
@endpush
@push('styles')
    <style>
        #preview-photo {
            display: none;
            width: 100%;
            height: 100%;
        }
    </style>
@endpush
