@extends('admin.layouts.adminLayout')

@section('content')


            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <a class="breadcrumbs__item" href="{{ route("pickers") }}">Сборщики</a>

                    <span class="breadcrumbs__item  breadcrumbs__item--current">
                        @if($id == 0)
                            Новый сборщик
                        @else
                            Редактирование профиля сборщика {{ $id }}
                        @endif


                    </span>
                </div>
            </section>

            <div class="main-content__top-container">
                <h1 class="main-content__title">
                    @if($id == 0)
                        Новый сборщик
                    @else
                        Редактирование профиля сборщика {{ $id }}
                    @endif
                </h1>
            </div>

            <div class="main-content__store-info">

                <div class="main-content__store-photo">
                    <div class="main-content__store-photo-container">
                        <h3 class="main-content__store-photo-title">Фото</h3>
                        <p class="main-content__store-photo-description">(фото должно быть до 3Mb)</p>
                    </div>

                    <button class="main-content__store-photo-delete" type="button"></button>

                    <label class="main-content__upload-container">
                        <input class="hidden-input" type="file">

                        <span class="main-content__upload-btn">Обзор...</span>

                        <span class="main-content__upload-message">Файл не выбран</span>
                    </label>
                </div>


                <div class="main-content__store-data  split-container--max-width">
                    <div class="white-square__input-container">
                        <label class="white-square__input-label">
                            Имя сборщика
                        </label>

                        <input type="text" class="white-square__input">
                    </div>

                    <div class="split-container  split-container--space-between">
                        <div class="white-square__input-container  half  white-square__input-container--margin-right">
                            <label class="white-square__input-label">
                                Контактный телефон
                            </label>

                            <input type="text" class="white-square__input">
                        </div>

                        <div class="white-square__input-container  half">
                            <label class="white-square__input-label">
                                Почта
                            </label>

                            <input type="text" class="white-square__input">
                        </div>
                    </div>

                    <div class="split-container  split-container--space-between">
                        <div class="white-square__input-container  half  white-square__input-container--margin-right">
                            <label class="white-square__input-label">
                                User name доступа к приложению
                            </label>

                            <input type="text" class="white-square__input">
                        </div>

                        <div class="white-square__input-container  half">
                            <label class="white-square__input-label">
                                Пароль доступа к приложению
                            </label>

                            <input type="text" class="white-square__input">
                        </div>
                    </div>


                    <div class="main-content__store-btns-container">
                        <a class="red-btn red-btn--profile" href="{{ route("pickers") }}">Отмена</a>
                        <button class="green-btn green-btn--profile">Сохранить</button>
                    </div>
                </div>
            </div>


@endsection
