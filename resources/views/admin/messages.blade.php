@extends('admin.layouts.adminLayout')

@section('content')


            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <span class="breadcrumbs__item  breadcrumbs__item--current">Уведомления</span>
                </div>
            </section>

            <div class="main-content__top-container">
                <h1 class="main-content__title">Уведомления</h1>

                <div class="main-content__search-container  main-content__search-container--margin">
                    <input class="main-content__search-input" type="text" placeholder="Искать...">
                    <button class="main-content__search-btn" type="button"></button>
                </div>

                <button class="green-btn  green-btn--small  js--add-msg" type="button">Создать уведомление</button>
            </div>

            <div
                class="split-container  split-container--align-center  split-container--space-between  split-container--max-width">

                <div class="paragraph--margin-bottom  fourth">
                <span class="main-content__dropdown-label">Тип сообщения
                </span>

                    <div class="main-content__dropdown-inner-container">
                        <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                        <ul class="main-content__dropdown-list">
                            <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                            <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                        </ul>
                    </div>
                </div>

                <div class="white-square__input-container  fourth">
                    <label class="white-square__input-label">Дата отправки</label>

                    <input type="text" class="white-square__input  popup__input  datepicker-here"
                           data-position="right bottom" data-timepicker="true">
                </div>

                <div class="paragraph--margin-bottom  fourth">
                <span class="main-content__dropdown-label">Группа клиентов
                </span>

                    <div class="main-content__dropdown-inner-container">
                        <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                        <ul class="main-content__dropdown-list">
                            <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                            <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                        </ul>
                    </div>
                </div>

                <button class="checkbox" type="button">Отображать только сохраненные в шаблоны</button>
            </div>

            <table class="main-content__table  main-content__shops-table">
                <tr class="main-content__header-row">
                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn" type="button">ID</button>
                    </th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn" type="button">
                            Дата и время
                        </button>
                    </th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn" type="button">
                            Тип сообщения
                        </button>
                    </th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn" type="button">
                            Тип пользователя
                        </button>
                    </th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn" type="button">
                            Группа клиентов
                        </button>
                    </th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn" type="button">
                            Cтатус клиента
                        </button>
                    </th>

                    <th class="main-content__header-cell">
                        Сообщение
                    </th>

                    <th class="main-content__header-cell">Действия</th>
                </tr>

                <tr class="main-content__table-row">
                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <a class="main-content__table-link"
                           href="support-inner.html">225584</a>
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">12 / 03 / 2019 12:57:20</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">СМС</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">Клиенты</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">VIP</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">Реанимированный</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">Текст пуш сообщения.</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <button class="main-content__table-btn  main-content__table-schedule-btn"
                                type="button"></button>
                        <!--<button class="main-content__table-btn  main-content__table-schedule-btn  main-content__table-schedule-btn&#45;&#45;added" type="button"></button>-->
                        <button class="main-content__table-btn  main-content__table-delete-btn  js--delete-row"
                                type="button"></button>
                    </td>
                </tr>
            </table>




    @push('popup')
        <section class="popup">

            <section class="popup__add-msg">
                <h4 class="popup__title">Создать уведомление</h4>


                <div class="main-content__store-photo--margin-bottom">
                    <div class="split-container  split-container--align-center  split-container--space-between">

                        <div class="white-square__input-container  split-container__inner--fixed-short">
                            <label class="white-square__input-label">Дата отправки</label>

                            <input type="text" class="white-square__input  popup__input  datepicker-here"
                                   data-position="right bottom">
                        </div>

                        <div class="paragraph--margin-bottom  ">
                            <span class="main-content__dropdown-label">Время</span>

                            <div class="main-content__dropdown-inner-container">
                                <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                                <ul class="main-content__dropdown-list">
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                                </ul>
                            </div>
                        </div>

                        <div class="paragraph--margin-bottom  ">
                <span class="main-content__dropdown-label">Тип сообщения
                </span>

                            <div class="main-content__dropdown-inner-container">
                                <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                                <ul class="main-content__dropdown-list">
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                    <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <input type="radio" class="hidden-input" id="push-add-for-user" name="push-add-type" checked>
                    <input type="radio" class="hidden-input" id="push-add-for-manager" name="push-add-type">


                    <div class="paragraph--margin-bottom">
                        <span class="main-content__dropdown-label">Тип пользователя</span>

                        <div class="main-content__dropdown-inner-container">
                            <span class="main-content__dropdown-current  popup__dropdown-current">Клиенты</span>

                            <div class="main-content__dropdown-list  main-content__dropdown-list--flex">
                                <label class="main-content__dropdown-item  popup__dropdown-item"
                                       for="push-add-for-user">Клиенты</label>
                                <label class="main-content__dropdown-item  popup__dropdown-item"
                                       for="push-add-for-manager">Менеджеры</label>
                            </div>
                        </div>

                    </div>

                    <div id="for-push-add-for-user">
                        <div class="split-container  split-container--align-center  split-container--space-between">
                            <div class="paragraph--margin-bottom  half">
                                <span class="main-content__dropdown-label">Группа</span>

                                <div class="main-content__dropdown-inner-container">
                                    <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                                    <ul class="main-content__dropdown-list">
                                        <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                        <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="paragraph--margin-bottom  half">
                                <span class="main-content__dropdown-label">Статус</span>

                                <div class="main-content__dropdown-inner-container">
                                    <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                                    <ul class="main-content__dropdown-list">
                                        <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                        <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="white-square__input-container">
                        <label class="white-square__input-label">
                            Текст сообщения
                        </label>

                        <textarea class="white-square__input" rows="4"></textarea>
                    </div>


                    <button class="checkbox" type="button">Сохранить в шаблоны</button>
                </div>


                <button class="green-btn  green-btn--profile  popup-submit" type="button">Создать</button>

                <button class="popup__close" type="button">Закрыть</button>
            </section>

            <section class="popup__delete">
                <h4 class="popup__title">Вы уверены, что хотите удалить?</h4>

                <button class="green-btn  green-btn--profile  popup-submit" type="button">Удалить</button>
                <button class="red-btn  red-btn--profile  popup-submit" type="button">Отменить</button>

                <button class="popup__close" type="button">Закрыть</button>
            </section>

        </section>
    @endpush

@endsection
