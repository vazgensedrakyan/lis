@extends('admin.layouts.adminLayout')

@section('content')

            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <span class="breadcrumbs__item  breadcrumbs__item--current">Менеджеры (сборщики)</span>
                </div>
            </section>

            <div class="main-content__top-container">
                <h1 class="main-content__title">Менеджеры</h1>

                <div class="main-content__search-container  main-content__search-container--margin">
                    <input class="main-content__search-input" type="text" placeholder="Искать...">
                    <button class="main-content__search-btn" type="button"></button>
                </div>

                <a class="green-btn  green-btn--small" href="{{ route("manager_form") }}">Добавить менеджера</a>
            </div>

            <table class="main-content__table">
                <tr class="main-content__header-row">
                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn" type="button">ID</button>
                    </th>

                    <th class="main-content__header-cell">Фото</th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn" type="button">ФИО</button>
                    </th>

                    <th class="main-content__header-cell  main-content__header-cell--wide">
                        <button class="main-content__table-sort-btn" type="button">Дата регистрации</button>
                    </th>

                    <th class="main-content__header-cell  main-content__header-cell--wide">Почта</th>

                    <th class="main-content__header-cell">Склад</th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn" type="button">Статус</button>
                    </th>

                    <th class="main-content__header-cell  main-content__header-cell--clients-actions">Действия</th>
                </tr>

                @foreach($managers as $manager)
                    <tr class="main-content__table-row">
                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <p class="main-content__id">{{$manager->id}}</p>
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <img class="main-content__table-img" src="{{ $manager->photo ? asset(''): '/img/user-anchor.jpg' }}" alt="photo">
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <a href="{{ route("manager", ["id" => $manager->id]) }}" class="main-content__table-link">{{ $manager->full_name }}</a>
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">{{ $manager->created_at->format('d / m / Y') }}</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">{{ $manager->email }}</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <span class="main-content__table-count">Магазин1</span>
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">Онлайн</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <a class="main-content__table-btn  main-content__table-btn--inline-block  main-content__table-list-btn" href="{{ route("manager_form", ["id" => 1]) }}"></a>
                            <button class="main-content__table-btn  main-content__table-delete-btn  js--delete-row" type="button"></button>
                        </td>
                    </tr>
                @endforeach
            </table>



@endsection
