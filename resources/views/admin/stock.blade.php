@extends('admin.layouts.adminLayout')

@section('content')


            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <a class="breadcrumbs__item" href="loyalty.html">Система лояльности</a>

                    <span class="breadcrumbs__item  breadcrumbs__item--current">Акции</span>
                </div>
            </section>

            <div class="main-content__top-container">
                <h1 class="main-content__title">Акции</h1>

                <button class="green-btn  green-btn--small  js--add-stock" type="button">Создать акцию</button>
            </div>

            <div class="popup__split-container">
                <div class="white-square__input-container">
                    <label class="white-square__input-label">
                        Дата начала
                    </label>

                    <input type="text" class="white-square__input  datepicker-here"
                           data-position="right bottom">
                </div>

                <div class="main-content__dropdown-container  popup__dropdown-container">
                    <span class="main-content__dropdown-label">Тип клиентов</span>

                    <div class="main-content__dropdown-inner-container">
                        <span class="main-content__dropdown-current  popup__dropdown-current">2+1</span>

                        <ul class="main-content__dropdown-list">
                            <li class="main-content__dropdown-item  popup__dropdown-item">1%</li>
                            <li class="main-content__dropdown-item  popup__dropdown-item">5%</li>
                            <li class="main-content__dropdown-item  popup__dropdown-item">10%</li>
                        </ul>
                    </div>
                </div>

            </div>

            <table class="main-content__table">
                <tr class="main-content__header-row">
                    <th class="main-content__header-cell">Товары или группа</th>

                    <th class="main-content__header-cell">Скидка</th>

                    <th class="main-content__header-cell">Бонусы</th>

                    <th class="main-content__header-cell">Тип клиентов</th>

                    <th class="main-content__header-cell">Начало и конец акции</th>

                    <th class="main-content__header-cell">Баннер</th>

                    <th class="main-content__header-cell  main-content__header-cell--promocode">Действия</th>
                </tr>

                <tr class="main-content__table-row">

                    <!--<td class="main-content__table-cell  main-content__table-cell&#45;&#45;padding">-->
                    <!--<p class="main-content__table-product-name">Овощи ( 128437 товаров )</p>-->
                    <!--</td>-->

                    <td class="main-content__table-cell  main-content__table-cell--padding  main-content__table-cell--preview">
                        <p class="main-content__table-product-name">Овощи ( 128437 товаров )</p>

                        <div class="main-content__product-list-container">
                            <h4 class="main-content__gray-title  main-content__dropdown-container--margin">Список
                                товаров для акции</h4>

                            <a class="main-content__table-link" href="goods-inner.html">№ 2345465</a>
                            <a class="main-content__table-link" href="goods-inner.html">№ 2345465</a>
                            <a class="main-content__table-link" href="goods-inner.html">№ 2345465</a>
                            <a class="main-content__table-link" href="goods-inner.html">№ 2345465</a>
                        </div>
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">20%</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">23%</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">VIP</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <span class="main-content__activated-from">02 / 03 / 2019</span>
                        <span>02 / 03 / 2019</span>
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">выаопыдвао.jpg</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <button class="main-content__table-btn  main-content__table-btn--switch  main-content__table-btn--gray"
                                type="button">Включить
                        </button>

                        <button class="main-content__table-btn  main-content__table-delete-btn  js--delete-row"
                                type="button"></button>
                    </td>
                </tr>
            </table>

    @push('popup')
        <section class="popup">

            <section class="popup__add-stock">
                <h4 class="popup__title">Создать акцию</h4>

                <div class="split-container  split-container--space-between">
                    <div class="white-square__input-container  half">
                        <label class="white-square__input-label">Начало акции с
                            <span class="popup__required">*</span>
                        </label>

                        <input type="text" class="white-square__input  popup__input  popup__input-from  datepicker-here"
                               data-position="right bottom" data-timepicker="true">
                    </div>

                    <div class="white-square__input-container  half">
                        <label class="white-square__input-label">Окончание акции
                            <span class="popup__required">*</span>
                        </label>

                        <input type="text" class="white-square__input  popup__input  datepicker-here"
                               data-position="right bottom" data-timepicker="true">
                    </div>
                </div>


                <input type="radio" class="hidden-input" id="add-stock-for-good" name="add-stock-for" checked>
                <input type="radio" class="hidden-input" id="add-stock-for-category" name="add-stock-for">

                <div class="split-container  split-container--space-between main-content__dropdown-container--margin">
                    <label class="radio__btn  half" for="add-stock-for-good">Акция для товара</label>
                    <label class="radio__btn  half" for="add-stock-for-category">Акция для категории</label>
                </div>

                <div id="for-add-stock-for-good">
                    <div class="white-square__input-container  white-square__input-container--no-margin">
                        <label class="white-square__input-label">
                            Артикул товара
                        </label>

                        <input type="text" class="white-square__input">
                    </div>

                    <div class="white-square__input-container  main-content__chat-file-container">
                        <span class="white-square__input-label">Файл с акционными товарами для загрузки</span>

                        <label class="main-content__upload-container">
                            <input class="hidden-input" type="file">

                            <span class="main-content__upload-btn">Обзор...</span>

                            <span class="main-content__upload-message">Файл не выбран</span>
                        </label>
                    </div>
                </div>

                <div id="for-add-stock-for-category">
                    <div class="white-square__input-container">
                        <label class="white-square__input-label">
                            Название категории
                        </label>

                        <input type="text" class="white-square__input">
                    </div>
                </div>


                <div class="popup__split-container  split-container--space-between  main-content__dropdown-container--margin">
                    <div class="white-square__input-container  white-square__input-container--no-margin  half">
                        <label class="white-square__input-label">
                            Скидка
                        </label>

                        <input type="text" class="white-square__input">
                    </div>

                    <div class="white-square__input-container  white-square__input-container--no-margin  half">
                        <label class="white-square__input-label">
                            Бонусы
                        </label>

                        <input type="text" class="white-square__input">
                    </div>
                </div>

                <div class="main-content__dropdown-container  main-content__dropdown-container--wide  main-content__dropdown-container--margin">
                    <span class="main-content__dropdown-label">Тип клиентов</span>

                    <div class="main-content__dropdown-inner-container">
                        <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                        <ul class="main-content__dropdown-list">
                            <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                            <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                        </ul>
                    </div>
                </div>

                <div class="white-square__input-container  main-content__chat-file-container">
                    <span class="white-square__input-label">Загрузить рекламный баннер</span>

                    <label class="main-content__upload-container">
                        <input class="hidden-input" type="file">

                        <span class="main-content__upload-btn">Обзор...</span>

                        <span class="main-content__upload-message">Файл не выбран</span>
                    </label>
                </div>
                <button class="green-btn  popup-submit" type="button">Создать</button>

                <button class="popup__close" type="button">Закрыть</button>
            </section>

            <section class="popup__delete">
                <h4 class="popup__title">Вы уверены, что хотите удалить?</h4>

                <button class="green-btn  green-btn--profile  popup-submit" type="button">Удалить</button>
                <button class="red-btn  red-btn--profile  popup-submit" type="button">Отменить</button>

                <button class="popup__close" type="button">Закрыть</button>
            </section>

        </section>
    @endpush


@endsection
