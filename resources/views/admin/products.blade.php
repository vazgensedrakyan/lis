@extends('admin.layouts.adminLayout')

@section('content')


            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <span class="breadcrumbs__item  breadcrumbs__item--current">Товары</span>
                </div>
            </section>

            <div class="main-content__top-container">
                <h1 class="main-content__title">Товары</h1>

                <div class="main-content__search-container">
                    <input class="main-content__search-input" type="text" placeholder="Искать...">
                    <button class="main-content__search-btn" type="button"></button>
                </div>
            </div>


            <div class="main-content__dropdowns-block">
                <div class="main-content__dropdown-container">
                    <span class="main-content__dropdown-label">Категория</span>

                    <div class="main-content__dropdown-inner-container">
                        <span class="main-content__dropdown-current">Выберите раздел</span>

                        <ul class="main-content__dropdown-list">
                            <li class="main-content__dropdown-item">Раздел 1</li>
                            <li class="main-content__dropdown-item">Раздел 2</li>
                            <li class="main-content__dropdown-item">Раздел 3</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="main-content__dropdowns-block">
                <div class="main-content__dropdown-container">
                    <span class="main-content__dropdown-label">Категория</span>

                    <div class="main-content__dropdown-inner-container">
                        <span class="main-content__dropdown-current">Выберите раздел</span>

                        <ul class="main-content__dropdown-list">
                            <li class="main-content__dropdown-item">Раздел 1</li>
                            <li class="main-content__dropdown-item">Раздел 2</li>
                            <li class="main-content__dropdown-item">Раздел 3</li>
                        </ul>
                    </div>
                </div>

                <div class="main-content__dropdown-container">
                    <span class="main-content__dropdown-label">Подкатегория</span>

                    <div class="main-content__dropdown-inner-container">
                        <span class="main-content__dropdown-current">Все актуальные разделы и подразделы</span>

                        <ul class="main-content__dropdown-list">
                            <li class="main-content__dropdown-item">Раздел 1</li>
                            <li class="main-content__dropdown-item">Раздел 2</li>
                            <li class="main-content__dropdown-item">Раздел 3</li>
                        </ul>
                    </div>
                </div>
            </div>

            <table class="main-content__table">
                <tr class="main-content__header-row">

                    <th class="main-content__header-cell  main-content__header-cell--product-padding">Фото</th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">Артикул</button>
                    </th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">Наименования</button>
                    </th>

                    <th class="main-content__header-cell">
                        <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">Наличие</button>
                    </th>

                    <th class="main-content__header-cell  main-content__header-cell--width">
                        <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort  main-content__table-sort-btn--width" type="button">
                            Статус
                        </button>
                    </th>

                    <th class="main-content__header-cell">Изменения</th>

                    <th class="main-content__header-cell">Действия</th>
                </tr>

                <tr class="main-content__table-row">
                    <td class="main-content__table-cell">
                        <img class="main-content__table-product-img  main-content__table-product-img--big"
                             src="/img/product-anchor.jpg" alt="photo">
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <span class="main-content__table-count">405058480</span>
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <a class="main-content__table-link" href="{{ route("product", ["id" => 1]) }}">Шоколадное яйцо Макси Kinder
                            FERRORO, 100г</a>
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">254 шт</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding  js--product-status">
                        Опубликован
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        07.02.2019 16:48 <br>
                        <span class="main-content__table-change-name">Admin</span>
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding  main-content__table-cell--product">
                        {{--<button class="main-content__table-btn  main-content__table-list-btn  js--product-edit-popup" type="button"></button>--}}
                        <button class="main-content__table-btn  main-content__table-eye-btn" type="button"></button>
                    </td>
                </tr>
            </table>


@endsection
