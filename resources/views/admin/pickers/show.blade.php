@extends('admin.layouts.adminLayout')

@section('content')


            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <a class="breadcrumbs__item" href="{{ route('pickers') }}">Сборщики</a>

                    <span class="breadcrumbs__item  breadcrumbs__item--current">Юзер-{{ $picker->id }}</span>
                </div>
            </section>


            <div class="main-content__profile-info">
                <div class="main-content__profile-img-container">
                    <img src="{{ $picker->photo ? asset('storage/'.$picker->photo): '/img/user-anchor.jpg' }}" alt="photo">
                </div>

                <div class="main-content__profile-top">

                    <h2 class="main-content__profile-name">{{ $picker->full_name }}</h2>

                    <span class="main-content__order-status  main-content__order-status--green">Онлайн</span>
                    <!--<span class="main-content__order-status  main-content__order-status&#45;&#45;red">Онлайн</span>-->

                    <div>
                        <a href="{{ route("picker.edit", ["id" => $picker->id]) }}" class="green-btn  green-btn--profile" type="button">Редактировать</a>
                        <form action="{{route('picker.delete', ['id' => $picker->id])}}" class="d-inline-block"
                              method="post">
                            @csrf
                            @method('delete')
                            <button class="red-btn  red-btn--profile" type="submit">Удалить</button>
                        </form>
                    </div>

                </div>

                <div class="main-content__profile-data">

                    <ul class="main-content__profile-data-list  main-content__profile-data-list--col-3">
                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">ID:</li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">{{ $picker->id }}</li>

                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                            Дата регистрации:
                        </li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                            {{ $picker->created_at->format('d / m / Y') }}
                        </li>

                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">Склад:</li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">Магазин1</li>
                    </ul>

                    <ul class="main-content__profile-data-list  main-content__profile-data-list--col-3">
                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">Моб.
                            телефон:
                        </li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                            {{ $picker->phone }}
                        </li>

                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">Почта:</li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                            <a class="main-content__profile-data-link" href="#">{{ $picker->email }}</a>
                        </li>
                    </ul>

                </div>

                <div class="main-content__profile-stats  main-content__order-split--flex-start">
                    <h2 class="main-content__profile-stats-title">Активность</h2>
                </div>

                <div class="main-content__profile-stats  main-content__order-split--flex-start">
                    <div class="white-square__input-container  main-content__data-range-container">

                        <label class="white-square__input-label">
                            Выберите период
                        </label>

                        <input type="text" class="white-square__input  popup__input  datepicker-here"
                               data-position="right bottom" data-timepicker="true" data-range="true"
                               data-multiple-dates-separator=" - ">
                    </div>
                </div>

                <table class="main-content__table  main-content__store-photo--margin-bottom">
                    <tr class="main-content__header-row">
                        <th class="main-content__header-cell  main-content__header-cell--nowrap">
                            <button class="main-content__table-sort-btn" type="button">Время</button>
                        </th>

                        <th class="main-content__header-cell  main-content__header-cell--nowrap">
                            <button class="main-content__table-sort-btn" type="button">№ Заказа</button>
                        </th>

                        <th class="main-content__header-cell  main-content__header-cell--nowrap">№ Пакета</th>

                        <th class="main-content__header-cell">
                            <button class="main-content__table-sort-btn" type="button">
                                Вес
                            </button>
                        </th>

                        <th class="main-content__header-cell">
                            <button class="main-content__table-sort-btn" type="button">Заказчик</button>
                        </th>

                        <th class="main-content__header-cell">
                            <button class="main-content__table-sort-btn" type="button">Стаус заказа</button>
                        </th>

                        <th class="main-content__header-cell">
                            <button class="main-content__table-sort-btn" type="button">Курьер</button>
                        </th>
                    </tr>

                    <tr class="main-content__table-row">
                        <td class="main-content__table-cell  main-content__table-cell--padding">12 / 03 / 2019  16:05</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <a class="main-content__id  main-content__id--link" href="order-inner.html">225584</a>
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">225584</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">123 гр</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <a href="" class="main-content__table-link">Белокаменский Алексей
                                Игоревич</a>
                            <br>
                            +79630007007
                            <br>
                            <a href="#" class="main-content__table-link">hoty@gmail.com</a>
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            Идет сборка
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <p class="main-content__table-cell--red"> Не назначен</p>
                        </td>
                    </tr>
                </table>
            </div>


@endsection
