@extends('admin.layouts.adminLayout')

@section('content')




            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <a class="breadcrumbs__item" href="couriers.html">Курьеры</a>

                    <span class="breadcrumbs__item  breadcrumbs__item--current">
                         @if($id == 0)
                            Новый курьер
                        @else
                            Редактировать профиль {{ $id }}
                        @endif
                    </span>
                </div>
            </section>

            <div class="main-content__top-container">
                <h1 class="main-content__title">
                    @if($id == 0)
                        Новый курьер
                    @else
                        Редактировать профиль {{ $id }}
                    @endif
                </h1>
            </div>

            <div class="main-content__store-info">

                <div class="main-content__store-photo">
                    <div class="main-content__store-photo-container">
                        <h3 class="main-content__store-photo-title">Фото</h3>
                        <p class="main-content__store-photo-description">(фото должно быть до 3Mb)</p>
                    </div>

                    <button class="main-content__store-photo-delete" type="button"></button>

                    <label class="main-content__upload-container">
                        <input class="hidden-input" type="file">

                        <span class="main-content__upload-btn">Обзор...</span>

                        <span class="main-content__upload-message">Файл не выбран</span>
                    </label>
                </div>


                <div class="main-content__store-data  split-container--max-width">
                    <div class="white-square__input-container">
                        <label class="white-square__input-label">
                            Имя курьера
                        </label>

                        <input type="text" class="white-square__input">
                    </div>

                    <div class="split-container  split-container--space-between">
                        <div class="white-square__input-container  half  white-square__input-container--margin-right">
                            <label class="white-square__input-label">
                                Контактный телефон
                            </label>

                            <input type="text" class="white-square__input">
                        </div>

                        <div class="white-square__input-container  half">
                            <label class="white-square__input-label">
                                Почта
                            </label>

                            <input type="text" class="white-square__input">
                        </div>
                    </div>

                    <div class="main-content__dropdown-container  main-content__dropdown-container--margin">
                <span class="main-content__dropdown-label">Склад
                </span>

                        <div class="main-content__dropdown-inner-container">
                            <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

                            <ul class="main-content__dropdown-list">
                                <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                            </ul>
                        </div>
                    </div>


                    <div class="main-content__store-btns-container">
                        <a class="red-btn red-btn--profile" href="{{ route("couriers") }}">Отмена</a>
                        <a class="green-btn green-btn--profile" href="managers.html">Сохранить</a>
                    </div>
                </div>
            </div>


@endsection
