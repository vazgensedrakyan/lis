@extends('admin.layouts.adminLayout')

@section('content')

    <section class="breadcrumbs">
        <div class="breadcrumbs__list">
            <span class="breadcrumbs__item  breadcrumbs__item--current">Клиенты</span>
        </div>
    </section>

    <div class="main-content__top-container">
        <h1 class="main-content__title">Клиенты</h1>

        <div class="main-content__search-container">
            <input class="main-content__search-input" type="text" placeholder="Искать...">
            <button class="main-content__search-btn" type="button"></button>
        </div>
    </div>

    <div class="popup__split-container  main-content__dropdown-container--margin">
        <div class="main-content__dropdown-container">
                <span class="main-content__dropdown-label">Фильтровать по группе
                </span>

            <div class="main-content__dropdown-inner-container">
                <span class="main-content__dropdown-current  popup__dropdown-current">VIP</span>

                <ul class="main-content__dropdown-list">
                    <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                    <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                </ul>
            </div>
        </div>

        <div class="main-content__dropdown-container">
            <span class="main-content__dropdown-label">Фильтровать по статусу</span>

            <div class="main-content__dropdown-inner-container">
                <span class="main-content__dropdown-current  popup__dropdown-current">Реанимированный</span>

                <ul class="main-content__dropdown-list">
                    <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                    <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                </ul>
            </div>
        </div>
    </div>

    <table class="main-content__table">
        <tr class="main-content__header-row">
            <th class="main-content__header-cell">
                <button class="main-content__table-sort-btn" type="button">ID</button>
            </th>

            <th class="main-content__header-cell">Фото</th>

            <th class="main-content__header-cell">
                <button class="main-content__table-sort-btn" type="button">ФИО</button>
            </th>

            <th class="main-content__header-cell  main-content__header-cell--wide">Телефон</th>

            <th class="main-content__header-cell">
                <button class="main-content__table-sort-btn" type="button">Кол-во заказов</button>
            </th>

            <th class="main-content__header-cell  main-content__header-cell--wide  main-content__header-cell--clients">
                <button class="main-content__table-sort-btn" type="button">Статус</button>
            </th>

            <th class="main-content__header-cell  main-content__header-cell--width">
                <button class="main-content__table-sort-btn  main-content__table-sort-btn--width" type="button">Группа
                </button>
            </th>

            <th class="main-content__header-cell  main-content__header-cell--clients-actions">Действия</th>
        </tr>

        @if(empty($usersList))
            <tr>
                <td colspan="8">
                    Клиентов нет
                </td>
            </tr>
        @else
            @foreach($usersList as $item)
                <tr class="{{$item['banned'] ? 'main-content__table-row main-content__table-row--invisible' : 'main-content__table-row'}}">
                <td class="main-content__table-cell  main-content__table-cell--padding">
                    <p class="main-content__id">{{ $item["id"] }}</p>
                </td>

                <td class="main-content__table-cell  main-content__table-cell--padding">
                    <img class="main-content__table-img" src="{{ $item["photo"] }}" alt="photo">
                </td>

                <td class="main-content__table-cell  main-content__table-cell--padding">
                    <a href="{{ route("client", ["id" => $item["id"]]) }}" class="main-content__table-link">{{ $item["name"] }}</a>
                </td>

                <td class="main-content__table-cell  main-content__table-cell--padding">+7{{ $item["phone"] }}</td>

                <td class="main-content__table-cell  main-content__table-cell--padding">{{ $item["orders"] }}</td>

                <td class="main-content__table-cell  main-content__table-cell--padding">
                    <span class="main-content__table-count">Разовый</span>
                </td>

                <td class="main-content__table-cell  main-content__table-cell--padding  main-content__table-cell--wide">
                    VIP
                </td>

                <td class="main-content__table-cell  main-content__table-cell--padding">
                    <form id="banClient{{ $item["id"] }}"
                          action="{{route('client.toggle_banned', ['id' => $item["id"]])}}"
                          method="post">
                        @csrf
                        @method('put')
                        <button class="{{$item['banned'] ? 'main-content__table-btn main-content__table-btn--yellow' : 'main-content__table-btn  main-content__table-btn--yellow'}}"
                                type="submit">{{$item['banned'] ? 'Восстановить' : 'Забанить'}}
                        </button>
                    </form>
                    <form id="deleteClient{{ $item["id"] }}"
                          action="{{route('client.delete', ['id' => $item["id"]])}}"
                          method="post">
                        @csrf
                        @method('delete')
                        <button class="main-content__table-btn  main-content__table-btn--red" type="submit" >
                            Удалить
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
        @endif
    </table>

@endsection
