@extends('admin.layouts.adminLayout')

@section('content')


    <section class="breadcrumbs">
        <div class="breadcrumbs__list">
            <span class="breadcrumbs__item  breadcrumbs__item--current">Поддержка</span>
        </div>
    </section>

    <div class="main-content__top-container">
        <h1 class="main-content__title">Поддержка</h1>

        <div class="main-content__search-container">
            <input class="main-content__search-input" type="text" placeholder="Искать...">
            <button class="main-content__search-btn" type="button"></button>
        </div>
    </div>

    <div class="main-content__dropdowns-block">
        <div class="main-content__dropdown-container">
            <span class="main-content__dropdown-label">Статус</span>

            <div class="main-content__dropdown-inner-container">
                <span class="main-content__dropdown-current">Решенные</span>

                <ul class="main-content__dropdown-list">
                    <li class="main-content__dropdown-item">Раздел 1</li>
                    <li class="main-content__dropdown-item">Раздел 2</li>
                    <li class="main-content__dropdown-item">Раздел 3</li>
                </ul>
            </div>
        </div>
    </div>

    <table class="main-content__table  main-content__shops-table">
        <tr class="main-content__header-row">
            <th class="main-content__header-cell">
                <button class="main-content__table-sort-btn" type="button">ID</button>
            </th>

            <th class="main-content__header-cell">
                Тема
            </th>

            <th class="main-content__header-cell">
                <button class="main-content__table-sort-btn" type="button">
                    Автор
                </button>
            </th>

            <th class="main-content__header-cell">
                <button class="main-content__table-sort-btn" type="button">
                    Создана
                </button>
            </th>

            <th class="main-content__header-cell">
                <button class="main-content__table-sort-btn" type="button">
                    Изменена
                </button>
            </th>

            <th class="main-content__header-cell">
                <button class="main-content__table-sort-btn" type="button">
                    Статус
                </button>
            </th>
        </tr>

        <tr class="main-content__table-row">
            <td class="main-content__table-cell  main-content__table-cell--padding">
                <a class="main-content__table-link" href="{{ route("support", ["id" => 1]) }}">225584</a>
            </td>

            <td class="main-content__table-cell  main-content__table-cell--padding">Поправьте фичу которую вы
                сделали случайно в моей учетной записи
            </td>

            <td class="main-content__table-cell  main-content__table-cell--padding">
                Алексей Сергеевич
                <span class="main-nav__sublink-count">267263478628734678</span>
            </td>

            <td class="main-content__table-cell  main-content__table-cell--padding">01 / 01 / 2019</td>

            <td class="main-content__table-cell  main-content__table-cell--padding">
                25 / 04 / 2019
            </td>

            <td class="main-content__table-cell  main-content__table-cell--padding">
                Решен
            </td>
        </tr>
    </table>


@endsection
