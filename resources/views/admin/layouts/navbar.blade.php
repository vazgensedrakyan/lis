@section('navbar')

    <button class="main-nav__burger" type="button">main nav</button>
    <aside class="main-nav">

        <div class="main-nav__wrapper">

            <div class="main-nav__links-list">
                <a class="main-nav__link  main-nav__link--clients  @if($menu =="clients")  main-nav__link--current @endif " href="{{ route("clients") }}">Клиенты</a>
                <a class="main-nav__link  main-nav__link--couriers @if($menu =="managers") main-nav__link--current @endif " href="{{ route("managers") }}">Менеджеры</a>
                <a class="main-nav__link  main-nav__link--couriers @if($menu =="pickers") main-nav__link--current @endif " href="{{ route("pickers") }}">Сборщики</a>
                <a class="main-nav__link  main-nav__link--requests @if($menu =="couriers") main-nav__link--current @endif " href="{{ route("couriers") }}">Курьеры</a>
                <a class="main-nav__link  main-nav__link--orders   @if($menu =="orders")   main-nav__link--current @endif " href="{{ route("orders") }}">Заказы</a>


                <div class="main-nav__inactive-link-container">
                    <span class="main-nav__link  main-nav__link--stores  main-nav__link--inactive">Магазин
                    <span class="main-nav__sublist-btn"></span>
                    </span>

                    <div class="main-nav__sublist" @if($menu =="stores") style="display: block" @endif>
                        <a class="main-nav__sublink @if(@$submenu == "stores") main-nav__link--current @endif " href="{{ route("stores") }}">Склады</a>
                        <a class="main-nav__sublink @if(@$submenu == "categories") main-nav__link--current @endif " href="{{ route("categories") }}">Категории</a>
                        <a class="main-nav__sublink @if(@$submenu == "products") main-nav__link--current @endif " href="{{ route("products") }}">Товары</a>
                        <a class="main-nav__sublink @if(@$submenu == "stats") main-nav__link--current @endif " href="{{ route("stats") }}">Статистика</a>
                    </div>
                </div>

                <a class="main-nav__link  main-nav__link--push @if(@$menu == "messages") main-nav__link--current @endif " href="{{ route("messages") }}">Push уведомления</a>
                <a class="main-nav__link  main-nav__link--reviews @if(@$menu == "feedback") main-nav__link--current @endif " href="{{ route("feedback") }}">Отзывы</a>
                <a class="main-nav__link  main-nav__link--support @if(@$menu == "support") main-nav__link--current @endif " href="{{ route("supports") }}">Поддержка</a>

                <div class="main-nav__inactive-link-container">
                    <div class="main-nav__links-container">
                        <a class="main-nav__link  main-nav__link--loyalty @if(@$menu == "loyalty" && @$submenu == "") main-nav__link--current @endif " href="{{ route("loyalty") }}">Система лояльности</a>
                        <span class="main-nav__sublist-btn"></span>
                    </div>

                    <div class="main-nav__sublist"  @if(@$menu == "loyalty") style="display: block" @endif>
                        <a class="main-nav__sublink @if(@$submenu == "stock") main-nav__link--current @endif " href="{{ route("stock") }}">Акции</a>
                        <a class="main-nav__sublink @if(@$submenu == "bonuses") main-nav__link--current @endif " href="{{ route("bonuses") }}">Бонусы</a>
                    </div>
                </div>

                <!--<a class="main-nav__link  main-nav__link&#45;&#45;loyalty" href="loyalty.html">Система лояльности</a>-->
                <!--<a class="main-nav__sublink" href="stocks.html">Акции</a>-->
                <!--<a class="main-nav__sublink" href="#">Бонусы</a>-->

            </div>

            <p class="paragraph  metaproject__container  metaproject__aside-container">
                Разработано в дизайн студии <br>
                <a class="metaproject__link  metaproject__aside-link" href="#">MetaProject</a>
            </p>

        </div>
    </aside>
@stop
