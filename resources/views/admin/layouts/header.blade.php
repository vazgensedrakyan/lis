@section('header')
    <header class="page-header">
        <div class="page-header__wrapper">
            <img class="page-header__logo" src="/img/logo.png" alt="logo">
            <a class="logout-btn" href="/admin/signout">
                Выйти
            </a>
        </div>
    </header>
@stop
