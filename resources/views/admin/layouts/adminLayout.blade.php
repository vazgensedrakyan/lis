<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="/css/style.css">

    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="/libs/bootstrap-4.3.1/css/bootstrap.css">
    <link rel="stylesheet" href="/libs/bootstrap-4.3.1/css/bootstrap.css">
    @stack('styles')

</head>
<body>

<div class="page-grid">

    @include('admin.layouts.header')
    @yield('header')

    @include('admin.layouts.navbar')
    @yield('navbar')

    <section class="main-content">
        <div class="main-content__wrapper">
            @yield('content')
        </div>
    </section>

</div>




@stack('popup')


<script src="/js/jquery-3.3.1.min.js"></script>
<script src="/libs/bootstrap-4.3.1/js/bootstrap.js"></script>
<script src="/js/datepicker.min.js"></script>
<script src="/js/script.js"></script>
<script src="/js/admin_js_controller.js"></script>
@stack('scripts')

</body>
</html>
