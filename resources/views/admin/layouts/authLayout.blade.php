<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <link rel="stylesheet" href="/css/style.css">

    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="/libs/bootstrap-4.3.1/css/bootstrap.css">
    <link rel="stylesheet" href="/libs/bootstrap-4.3.1/css/bootstrap.css">



</head>
<body>


    @yield('content')


    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/libs/bootstrap-4.3.1/js/bootstrap.js"></script>

</body>
</html>
