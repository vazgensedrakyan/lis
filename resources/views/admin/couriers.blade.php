@extends('admin.layouts.adminLayout')

@section('content')


    <section class="breadcrumbs">
        <div class="breadcrumbs__list">
            <span class="breadcrumbs__item  breadcrumbs__item--current">Курьеры</span>
        </div>
    </section>

    <div class="main-content__top-container">
        <h1 class="main-content__title">Курьеры</h1>

        <div class="main-content__search-container  main-content__search-container--margin">
            <input class="main-content__search-input" type="text" placeholder="Искать...">
            <button class="main-content__search-btn" type="button"></button>
        </div>

        <a class="green-btn  green-btn--small" href="{{ route("courier_form") }}">Добавить курьера</a>
    </div>

    <table class="main-content__table">
        <tr class="main-content__header-row">
            <th class="main-content__header-cell">
                <button class="main-content__table-sort-btn" type="button">ID</button>
            </th>

            <th class="main-content__header-cell">Фото</th>

            <th class="main-content__header-cell">
                <button class="main-content__table-sort-btn" type="button">ФИО</button>
            </th>

            <th class="main-content__header-cell">
                <button class="main-content__table-sort-btn" type="button">Дата регистрации</button>
            </th>

            <th class="main-content__header-cell  main-content__header-cell--wide">Телефон</th>

            <th class="main-content__header-cell  main-content__header-cell--wide">Количество заказов</th>

            <th class="main-content__header-cell">
                <button class="main-content__table-sort-btn" type="button">Статус</button>
            </th>

            <th class="main-content__header-cell">Склад</th>

            <th class="main-content__header-cell  main-content__header-cell--clients-actions">Действия</th>
        </tr>

        <tr class="main-content__table-row">
            <td class="main-content__table-cell  main-content__table-cell--padding">
                <p class="main-content__id">225584</p>
            </td>

            <td class="main-content__table-cell  main-content__table-cell--padding">
                <img class="main-content__table-img" src="/img/user-anchor.jpg" alt="photo">
            </td>

            <td class="main-content__table-cell  main-content__table-cell--padding">
                <a href="{{ route("courier") }}" class="main-content__table-link">Белокаменский Алексей Игоревич</a>
            </td>

            <td class="main-content__table-cell  main-content__table-cell--padding">22.10 2019</td>

            <td class="main-content__table-cell  main-content__table-cell--padding">+79458445321</td>

            <td class="main-content__table-cell  main-content__table-cell--padding">
                <span class="main-content__table-count">0</span>
            </td>

            <td class="main-content__table-cell  main-content__table-cell--padding">Онлайн</td>

            <td class="main-content__table-cell  main-content__table-cell--padding">Магазин 1</td>

            <td class="main-content__table-cell  main-content__table-cell--padding">
                <button class="main-content__table-btn  main-content__table-btn--yellow  js--block-btn" type="button">
                    Забанить
                </button>
                <button class="main-content__table-btn  main-content__table-btn--red  js--delete-row" type="button">
                    Удалить
                </button>
            </td>
        </tr>
    </table>


    @push('popup')
        <section class="popup">

            <section class="popup__delete">
                <h4 class="popup__title">Вы уверены, что хотите удалить?</h4>

                <button class="green-btn  green-btn--profile  popup-submit" type="button">Удалить</button>
                <button class="red-btn  red-btn--profile  popup-submit" type="button">Отменить</button>

                <button class="popup__close" type="button">Закрыть</button>
            </section>

        </section>
    @endpush

    @push('script')
        CourierController.init();
    @endpush


@endsection
