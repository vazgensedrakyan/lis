@extends('admin.layouts.adminLayout')

@section('content')

            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <span class="breadcrumbs__item  breadcrumbs__item--current">Система лояльности</span>
                </div>
            </section>

            <div class="main-content__top-container">
                <h1 class="main-content__title">Система лояльности</h1>

                <button class="green-btn  green-btn--small  js--add-loyalty-category" type="button">Добавить категорию</button>
            </div>

            <table class="main-content__table">
                <tr class="main-content__header-row">
                    <th class="main-content__header-cell">Группы</th>

                    <th class="main-content__header-cell">Условие</th>

                    <th class="main-content__header-cell">Активность</th>

                    <th class="main-content__header-cell">Скидка</th>

                    <th class="main-content__header-cell">Бонусы</th>

                    <th class="main-content__header-cell">Доставка</th>

                    <th class="main-content__header-cell">Кол- во клиентов</th>

                    <th class="main-content__header-cell">Действия</th>
                </tr>

                <tr class="main-content__table-row">
                    <td class="main-content__table-cell  main-content__table-cell--padding">Случайный</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">от 20 тыс руб.</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">от 20 заказов</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">10%</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">5</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">10</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">73649 чел.</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <button class="main-content__table-btn  main-content__table-user-btn  js--add-users-loyalty"
                                type="button"></button>
                        <button class="main-content__table-btn  main-content__table-list-btn  js--edit-loyalty-category" type="button"></button>
                        <button class="main-content__table-btn  main-content__table-delete-btn  js--delete-row" type="button"></button>
                    </td>
                </tr>
            </table>


            @push('popup')
                <section class="popup">

                    <section class="popup__add-loyalty-category">
                        <h4 class="popup__title">Создать категорию</h4>

                        <div class="white-square__input-container  white-square__input-container--list">
                            <label class="white-square__input-label">Название</label>

                            <input type="text" class="white-square__input">
                        </div>

                        <div class="white-square__input-container  white-square__input-container--list">
                            <label class="white-square__input-label">Потраченная сумма</label>

                            <input type="text" class="white-square__input">
                        </div>

                        <div class="white-square__input-container  white-square__input-container--list">
                            <label class="white-square__input-label">Кол-во заказов</label>

                            <input type="text" class="white-square__input">
                        </div>

                        <div class="popup__split-container  split-container--space-between  main-content__dropdown-container--margin">
                            <div class="white-square__input-container  white-square__input-container--no-margin  half">
                                <label class="white-square__input-label">
                                    Скидка
                                </label>

                                <input type="text" class="white-square__input">
                            </div>

                            <div class="white-square__input-container  white-square__input-container--no-margin  half">
                                <label class="white-square__input-label">
                                    Бонусы
                                </label>

                                <input type="text" class="white-square__input">
                            </div>
                        </div>

                        <div class="white-square__input-container  white-square__input-container--list">
                            <label class="white-square__input-label">Цена за доставку</label>

                            <input type="text" class="white-square__input">
                        </div>


                        <button class="green-btn  popup-submit" type="button">Создать</button>

                        <button class="popup__close" type="button">Закрыть</button>
                    </section>

                    <section class="popup__edit-loyalty-category">
                        <h4 class="popup__title">Редактировать категорию</h4>

                        <div class="white-square__input-container  white-square__input-container--list">
                            <label class="white-square__input-label">Название</label>

                            <input type="text" class="white-square__input">
                        </div>

                        <div class="white-square__input-container  white-square__input-container--list">
                            <label class="white-square__input-label">Потраченная сумма</label>

                            <input type="text" class="white-square__input">
                        </div>

                        <div class="white-square__input-container  white-square__input-container--list">
                            <label class="white-square__input-label">Кол-во заказов</label>

                            <input type="text" class="white-square__input">
                        </div>

                        <div class="popup__split-container  split-container--space-between  main-content__dropdown-container--margin">
                            <div class="white-square__input-container  white-square__input-container--no-margin  half">
                                <label class="white-square__input-label">
                                    Скидка
                                </label>

                                <input type="text" class="white-square__input">
                            </div>

                            <div class="white-square__input-container  white-square__input-container--no-margin  half">
                                <label class="white-square__input-label">
                                    Бонусы
                                </label>

                                <input type="text" class="white-square__input">
                            </div>
                        </div>

                        <div class="white-square__input-container  white-square__input-container--list">
                            <label class="white-square__input-label">Цена за доставку</label>

                            <input type="text" class="white-square__input">
                        </div>


                        <button class="green-btn  popup-submit" type="button">Сохранить</button>

                        <button class="popup__close" type="button">Закрыть</button>
                    </section>

                    <section class="popup__add-users-loyalty">
                        <h4 class="popup__title">Добавление пользователя</h4>


                        <div class="white-square__input-container  white-square__input-container--list">
                            <label class="white-square__input-label">Имя клиента</label>

                            <input type="text" class="white-square__input">

                            <ul class="main-content__dropdown-list">
                                <li class="main-content__dropdown-item">

                                    <ul class="split-container  split-container--space-between">
                                        <li class="split-container__inner--fixed-wide">
                                            <a class="main-content__table-link" href="client-inner.html">Белокаменский Алексей</a>
                                        </li>
                                        <li class="paragraph  split-container__inner--fixed-short">+79630007007</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>


                        <button class="green-btn  popup-submit" type="button">Сохранить</button>

                        <button class="popup__close" type="button">Закрыть</button>
                    </section>

                    <section class="popup__delete">
                        <h4 class="popup__title">Вы уверены, что хотите удалить?</h4>

                        <button class="green-btn  green-btn--profile  popup-submit" type="button">Удалить</button>
                        <button class="red-btn  red-btn--profile  popup-submit" type="button">Отменить</button>

                        <button class="popup__close" type="button">Закрыть</button>
                    </section>

                </section>
            @endpush


@endsection
