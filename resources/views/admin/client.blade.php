@extends('admin.layouts.adminLayout')

@section('content')

            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <a class="breadcrumbs__item" href="{{route('clients')}}">Клиенты</a>

                    <span class="breadcrumbs__item  breadcrumbs__item--current">Юзер-{{$client->id}}</span>
                </div>
            </section>


            <div class="main-content__profile-info">
                <div class="main-content__profile-img-container">
                    <img src="{{ $client->photo ? asset('storage/'.$client->photo) : '/img/user-anchor.jpg' }}" alt="photo">
                </div>

                <div class="main-content__profile-top">

                    <h2 class="main-content__profile-name">{{$client->name}}</h2>

                    <div>
                        <form id="banClient" class="d-none"
                              action="{{route('client.toggle_banned', ['id' => $client->id])}}"
                              method="post">
                            @csrf
                            @method('put')
                        </form>
                        <button class="yellow-btn  yellow-btn--profile"
                                type="submit" form="banClient">{{$client->banned ? 'Восстановить' : 'Забанить'}}
                        </button>
                        <form id="deleteClient" class="d-none"
                              action="{{route('client.delete', ['id' => $client->id])}}"
                              method="post">
                            @csrf
                            @method('delete')
                        </form>
                        <button class="red-btn  red-btn--profile" type="submit" form="deleteClient">Удалить</button>
                    </div>

                </div>

                <div class="main-content__profile-data  split-container--space-between  split-container">

                    <div class="    main-content__dropdown-container--fixed-width">
                <span class="main-content__dropdown-label">Категория
                </span>

                        <div class="main-content__dropdown-inner-container">
                            <span class="main-content__dropdown-current  popup__dropdown-current">VIP</span>

                            <ul class="main-content__dropdown-list">
                                <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                            </ul>
                        </div>
                    </div>

                    <div class="    main-content__dropdown-container--fixed-width">
                        <span class="main-content__dropdown-label">Статус</span>

                        <div class="main-content__dropdown-inner-container">
                            <span class="main-content__dropdown-current  popup__dropdown-current">Реанимированный</span>

                            <ul class="main-content__dropdown-list">
                                <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                                <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
                            </ul>
                        </div>
                    </div>


                    <ul class="main-content__profile-data-list  main-content__profile-data-list--col-1">
                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">ID:</li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">{{$client->id}}</li>

                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">
                            Зарегистрирован:
                        </li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                            {{$client->created_at->format('d / m / Y')}}
                        </li>

                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">Сделанных
                            заказов:
                        </li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">{{$client->orders}}</li>

                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">Моб.
                            телефон:
                        </li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                            {{$client->phone}}
                        </li>

                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">Почта:</li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                            <a class="main-content__profile-data-link" href="mailto:{{$client->email}}">{{$client->email}}</a>
                        </li>
                    </ul>

                    <ul class="main-content__profile-data-list  main-content__profile-data-list--col-3">
                        <li class="main-content__profile-data-item  main-content__profile-data-item--title">Приглашенные
                            пользователи:
                        </li>
                        <li class="main-content__profile-data-item  main-content__profile-data-item--value">
                            <ul class="main-content__profile-data-sublist">
                                <li class="main-content__profile-data-subitem">
                                    <a class="main-content__table-link" href="client-inner.html">Димон</a>
                                </li>
                                <li class="main-content__profile-data-subitem">
                                    <a class="main-content__table-link" href="client-inner.html">Сергей</a>
                                </li>
                                <li class="main-content__profile-data-subitem">
                                    <a class="main-content__table-link" href="client-inner.html">Артур</a>
                                </li>
                                <li class="main-content__profile-data-subitem">
                                    <a class="main-content__table-link" href="client-inner.html">Никитос</a>
                                </li>
                                <li class="main-content__profile-data-subitem">
                                    <a class="main-content__table-link" href="client-inner.html">Денис</a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                </div>

                <div class="main-content__profile-stats  main-content__order-split--flex-start">
                    <h2 class="main-content__profile-stats-title">Статистика</h2>

                    <div class="main-content__profile-stats-item">
                        <p class="main-content__profile-stats-paragraph">Среднее кол. заказов</p>

                        <h3 class="main-content__profile-stats-bold">2,5</h3>
                    </div>


                    <div class="main-content__profile-stats-item">
                        <p class="main-content__profile-stats-paragraph">Отмененных заказов</p>

                        <h3 class="main-content__profile-stats-bold">14</h3>
                    </div>


                    <div class="main-content__profile-stats-item">
                        <p class="main-content__profile-stats-paragraph">Средний чек</p>

                        <h3 class="main-content__profile-stats-bold">1 584
                            <span CLASS="main-content__profile-stats-small">руб</span>
                        </h3>
                    </div>

                </div>

                <h2 class="main-content__profile-stats-title">Все заказы</h2>
                <div class="main-content__profile-stats  main-content__order-split--flex-start">
                    <div class="white-square__input-container  main-content__data-range-container">

                        <label class="white-square__input-label">
                            Выберите период
                        </label>

                        <input type="text" class="white-square__input  popup__input  datepicker-here"
                               data-position="right bottom" data-timepicker="true" data-range="true"
                               data-multiple-dates-separator=" - ">
                    </div>
                </div>

                <table class="main-content__table  main-content__store-photo--margin-bottom">
                    <tr class="main-content__header-row">
                        <th class="main-content__header-cell  main-content__header-cell--nowrap">
                            <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">№ Заказа</button>
                        </th>

                        <th class="main-content__header-cell  main-content__header-cell--nowrap">№ Пакета</th>

                        <th class="main-content__header-cell">Адрес доставки</th>

                        <th class="main-content__header-cell">
                            <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort-down" type="button">
                                Статус
                            </button>
                        </th>

                        <th class="main-content__header-cell">
                            <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort-up" type="button">Доставка</button>
                        </th>

                        <th class="main-content__header-cell">
                            <button class="main-content__table-sort-btn" type="button">Чек</button>
                        </th>

                        <th class="main-content__header-cell">Курьер</th>

                        <th class="main-content__header-cell">Сборщик</th>
                    </tr>

                    <tr class="main-content__table-row">
                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <a class="main-content__id  main-content__id--link" href="order-inner.html">225584</a>
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">154887</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">610048, Киров,
                            Московская улица, дом 153 кв 98
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">Выполняется</td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <span class="main-content__activated-from"> 07.02.2019 18:00</span>
                            <span>07.02.2019 18:00</span>
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            1 084 руб наличными курьеру
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <a href="" class="main-content__table-link">Белокаменский Алексей</a>
                        </td>

                        <td class="main-content__table-cell  main-content__table-cell--padding">
                            <a href="" class="main-content__table-link">Белокаменский Алексей</a>
                        </td>
                    </tr>
                </table>


                <div class="main-content__profile-reviews">
                    <input type="radio" class="hidden-input" id="profile-review-all" name="profile-review" checked>
                    <input type="radio" class="hidden-input" id="profile-review-negative" name="profile-review">

                    <div class="main-content__profile-reviews-btns">
                        <label class="main-content__profile-reviews-btn" for="profile-review-all">Отзывы о
                            курьерах</label>

                        <label class="main-content__profile-reviews-btn" for="profile-review-negative">Отзывы о
                            товарах</label>
                    </div>

                    <table class="main-content__table  main-content__profile-reviews-table"
                           id="profile-reviews-table-all">
                        <tr class="main-content__header-row">
                            <th class="main-content__header-cell">
                                <button class="main-content__table-sort-btn" type="button">№ Заказа</button>
                            </th>

                            <th class="main-content__header-cell">
                                <button class="main-content__table-sort-btn" type="button">Оценка</button>
                            </th>

                            <th class="main-content__header-cell">Комментарий</th>

                            <th class="main-content__header-cell">
                                <button class="main-content__table-sort-btn" type="button">Кому поставил</button>
                            </th>

                            <th class="main-content__header-cell">
                                <button class="main-content__table-sort-btn" type="button">Дата</button>
                            </th>
                        </tr>

                        <tr class="main-content__table-row">
                            <td class="main-content__table-cell  main-content__table-cell--padding">296</td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">
                                <span class="main-content__profile-star"></span>
                                <span class="main-content__profile-star"></span>
                                <span class="main-content__profile-star"></span>
                                <span class="main-content__profile-star"></span>
                                <span class="main-content__profile-star  main-content__profile-star--empty"></span>
                            </td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">Нет</td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">
                                <a class="main-content__profile-review-author  main-content__table-link" href="#">Артем
                                    Высокий</a>
                            </td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">12 / 03 / 2019</td>
                        </tr>

                        <tr class="main-content__table-row">
                            <td class="main-content__table-cell  main-content__table-cell--padding">2960</td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">
                                <span class="main-content__profile-star"></span>
                                <span class="main-content__profile-star"></span>
                                <span class="main-content__profile-star"></span>
                                <span class="main-content__profile-star"></span>
                                <span class="main-content__profile-star"></span>
                            </td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">Принес все быстро и
                                даже на чай не взял денег. спасибо за доставку))
                            </td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">
                                <a class="main-content__profile-review-author  main-content__table-link" href="">Иванова
                                    Светлана</a>
                            </td>

                            <td class="main-content__table-cell  main-content__table-cell--padding">12 / 03 / 2019</td>
                        </tr>
                    </table>


                    <table class="main-content__table  main-content__profile-reviews-table"
                           id="profile-reviews-table-negative">
                        <tr class="main-content__header-row">
                            <th class="main-content__header-cell">
                                Пусто
                            </th>
                        </tr>
                    </table>

                </div>
            </div>

   
@endsection
