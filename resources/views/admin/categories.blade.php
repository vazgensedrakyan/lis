@extends('admin.layouts.adminLayout')

@section('content')


    <section class="breadcrumbs">
        <div class="breadcrumbs__list">
            <span class="breadcrumbs__item  breadcrumbs__item--current">Категории</span>
        </div>
    </section>

    <div class="main-content__top-container">
        <h1 class="main-content__title">Категории</h1>

        <div class="main-content__search-container  main-content__search-container--margin">
            <input class="main-content__search-input" type="text" placeholder="Искать...">
            <button class="main-content__search-btn" type="button"></button>
        </div>

        {{--<a class="green-btn  green-btn--small" href="category-add.html">Создать категорию</a>--}}
    </div>

    <div class="main-content__dropdown-container  popup__dropdown-container  main-content__dropdown-container--margin">
                <span class="main-content__dropdown-label">Выберите склад
                </span>

        <div class="main-content__dropdown-inner-container">
            <span class="main-content__dropdown-current  popup__dropdown-current">Все</span>

            <ul class="main-content__dropdown-list">
                <li class="main-content__dropdown-item  popup__dropdown-item">Да</li>
                <li class="main-content__dropdown-item  popup__dropdown-item">Нет</li>
            </ul>
        </div>
    </div>

    <section class="pseudo-table">
        <div class="pseudo-table__header-row">
            <div class="pseudo-table__header-cell">ID</div>

            <div class="pseudo-table__header-cell">
                <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">
                    Название
                </button>
            </div>

            <div class="pseudo-table__header-cell">
                <button class="main-content__table-sort-btn  main-content__table-sort-btn--sort" type="button">
                    Количество товаров
                </button>
            </div>

            {{--<div class="pseudo-table__header-cell">Действия</div>--}}
        </div>

        <div class="pseudo-table__rows-container">
            <div class="pseudo-table__table-row  pseudo-table__table-row--category">
                <div class="pseudo-table__table-cell">1699</div>

                <div class="pseudo-table__table-cell">
                    Категория 1 (2 подкатегории)

                    <button class="pseudo-table__table-dropdown-btn  pseudo-table__category-dropdown-btn"
                            type="button"></button>
                </div>

                <div class="pseudo-table__table-cell">
                    <a class="main-content__table-link" href="goods.html">254</a>
                </div>

                {{--<div class="pseudo-table__table-cell">--}}
                {{--<a class="main-content__table-btn  main-content__table-list-btn" href="category-edit.html"></a>--}}
                {{--<button class="main-content__table-btn  main-content__category-eye-btn"--}}
                {{--type="button"></button>--}}
                {{--</div>--}}


                <div class="pseudo-table__table-row  pseudo-table__table-row--subcategory">
                    <div class="pseudo-table__table-cell">1699</div>

                    <div class="pseudo-table__table-cell">
                        Подкатегория 1 (1 подкатегория)

                        <button class="pseudo-table__table-dropdown-btn  pseudo-table__subcategory-dropdown-btn"
                                type="button"></button>
                    </div>

                    <div class="pseudo-table__table-cell">
                        <a class="main-content__table-link" href="goods.html">254</a>
                    </div>

                    {{--<div class="pseudo-table__table-cell">--}}
                    {{--<a class="main-content__table-btn  main-content__table-list-btn" href="category-edit.html"></a>--}}
                    {{--<button class="main-content__table-btn  main-content__category-eye-btn"--}}
                    {{--type="button"></button>--}}
                    {{--</div>--}}
                </div>

                <div class="pseudo-table__table-row  pseudo-table__table-row--subcategory-2">
                    <div class="pseudo-table__table-cell">1699</div>

                    <div class="pseudo-table__table-cell">
                        Под-подкатегория 1
                    </div>

                    <div class="pseudo-table__table-cell">
                        <a class="main-content__table-link" href="goods.html">254</a>
                    </div>

                    {{--<div class="pseudo-table__table-cell">--}}
                    {{--<a class="main-content__table-btn  main-content__table-list-btn" href="category-edit.html"></a>--}}
                    {{--<button class="main-content__table-btn  main-content__category-eye-btn"--}}
                    {{--type="button"></button>--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="pseudo-table__table-row  pseudo-table__table-row--category">
                <div class="pseudo-table__table-cell">1699</div>

                <div class="pseudo-table__table-cell">
                    Категория 1 (2 подкатегории)

                    <button class="pseudo-table__table-dropdown-btn  pseudo-table__category-dropdown-btn"
                            type="button"></button>
                </div>

                <div class="pseudo-table__table-cell">
                    <a class="main-content__table-link" href="goods.html">254</a>
                </div>

                <div class="pseudo-table__table-row  pseudo-table__table-row--subcategory">
                    <div class="pseudo-table__table-cell">1699</div>

                    <div class="pseudo-table__table-cell">
                        Подкатегория 1 (1 подкатегория)

                        <button class="pseudo-table__table-dropdown-btn  pseudo-table__subcategory-dropdown-btn"
                                type="button"></button>
                    </div>

                    <div class="pseudo-table__table-cell">
                        <a class="main-content__table-link" href="goods.html">254</a>
                    </div>
                </div>

                <div class="pseudo-table__table-row  pseudo-table__table-row--subcategory-2">
                    <div class="pseudo-table__table-cell">1699</div>

                    <div class="pseudo-table__table-cell">
                        Под-подкатегория 1
                    </div>

                    <div class="pseudo-table__table-cell">
                        <a class="main-content__table-link" href="goods.html">254</a>
                    </div>
                </div>
                <div class="pseudo-table__table-row  pseudo-table__table-row--subcategory-2">
                    <div class="pseudo-table__table-cell">1699</div>

                    <div class="pseudo-table__table-cell">
                        Под-подкатегория 2
                    </div>

                    <div class="pseudo-table__table-cell">
                        <a class="main-content__table-link" href="goods.html">254</a>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
