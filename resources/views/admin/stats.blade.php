@extends('admin.layouts.adminLayout')

@section('content')


            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <span class="breadcrumbs__item  breadcrumbs__item--current">Статистика</span>
                </div>
            </section>

            <div class="main-content__top-container">
                <h1 class="main-content__title">Статистика</h1>
            </div>

            <div class="white-square__input-container  main-content__data-range-container  main-content__store-photo--margin-bottom">

                <label class="white-square__input-label">
                    Выберите период
                </label>

                <input type="text" class="white-square__input  popup__input  datepicker-here"
                       data-position="right bottom" data-timepicker="true" data-range="true"
                       data-multiple-dates-separator=" - ">
            </div>


            <div class="main-content__profile-stats  main-content__order-split--flex-start  main-content__store-photo--margin-bottom">
                <div class="main-content__profile-stats-item  main-content__profile-stats-item--active">
                    <p class="main-content__profile-stats-paragraph">Новых клиентов</p>

                    <h3 class="main-content__profile-stats-bold">2 544</h3>

                    <ul class="main-content__profile-stats-list">
                        <li class="main-content__profile-stats-list-item">
                            Реанимированно
                            <b>67</b>
                        </li>
                        <li class="main-content__profile-stats-list-item">
                            Потеряно
                            <b>5</b>
                        </li>
                    </ul>
                </div>


                <div class="main-content__profile-stats-item">
                    <p class="main-content__profile-stats-paragraph">Заказы</p>

                    <h3 class="main-content__profile-stats-bold">14 648</h3>

                    <ul class="main-content__profile-stats-list">
                        <li class="main-content__profile-stats-list-item">
                            Средний чек
                            <b>7.99 р.</b>
                        </li>
                        <li class="main-content__profile-stats-list-item">
                            Отменено
                            <b>7</b>
                        </li>
                    </ul>
                </div>


                <div class="main-content__profile-stats-item">
                    <p class="main-content__profile-stats-paragraph">Время выполнения заказа</p>

                    <h3 class="main-content__profile-stats-bold">35
                        <span class="main-content__profile-stats-small">мин</span>
                    </h3>

                    <ul class="main-content__profile-stats-list">
                        <li class="main-content__profile-stats-list-item  main-content__profile-stats-list-item--like">
                            Иванов Борис
                        </li>
                        <li class="main-content__profile-stats-list-item  main-content__profile-stats-list-item--dislike">
                            Васильев Котовасий
                        </li>
                    </ul>
                </div>


                <div class="main-content__profile-stats-item">
                    <p class="main-content__profile-stats-paragraph">Время сбора заказа</p>

                    <h3 class="main-content__profile-stats-bold">35
                        <span class="main-content__profile-stats-small">мин</span>
                    </h3>

                    <ul class="main-content__profile-stats-list">
                        <li class="main-content__profile-stats-list-item  main-content__profile-stats-list-item--like">
                            Иванов Борис
                        </li>
                        <li class="main-content__profile-stats-list-item  main-content__profile-stats-list-item--dislike">
                            Васильев Котовасий
                        </li>
                    </ul>
                </div>

            </div>


            <canvas id="popChart" width="600" height="400"></canvas>


            @push('scripts')
                <script src="/js/Chart.bundle.min.js"></script>
                <script>
                    var popCanvas = $("#popChart");
                    // var popCanvas = document.getElementById("popChart");
                    // popCanvas.parentNode.style.width = '885px';
                    // popCanvas.parentNode.style.height = '395px';

                    var horizontalPoints = ["Понедельник", "Вторник", "Среда"];
                    var verticalPoints = [2000, 3000, 500];

                    var data = {
                        labels: horizontalPoints,
                        datasets: [{
                            data: verticalPoints,
                            borderColor: '#fecd2f',
                            backgroundColor: 'rgba(254, 205, 47, 0.3)',
                            borderWidth: "7",
                            pointBorderColor: '#fecd2f',
                            pointBackgroundColor: '#ffffff',
                            pointHoverBackgroundColor: '#ffffff',
                            pointRadius: 10,
                            pointHoverRadius: 10,
                            pointBorderWidth: 7,
                            pointHoverBorderWidth: 7,
                        }]
                    };

                    var chartOptions = {
                        legend: {
                            display: false,
                        }
                    };

                    var lineChart = new Chart(popCanvas, {
                        type: 'line',
                        data: data,
                        options: chartOptions
                    });

                    $(".main-content__profile-stats-item").on("click", function () {
                        $(this).addClass("main-content__profile-stats-item--active");
                        $(".main-content__profile-stats-item").not($(this)).removeClass("main-content__profile-stats-item--active");
                    });
                </script>
            @endpush


@endsection
