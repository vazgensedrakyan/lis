@extends('admin.layouts.adminLayout')

@section('content')



            <section class="breadcrumbs">
                <div class="breadcrumbs__list">
                    <span class="breadcrumbs__item  breadcrumbs__item--current">Магазины</span>
                </div>
            </section>

            <div class="main-content__top-container">
                <h1 class="main-content__title">Склады</h1>

                <div class="main-content__search-container  main-content__search-container--margin">
                    <input class="main-content__search-input" type="text" placeholder="Искать...">
                    <button class="main-content__search-btn" type="button"></button>
                </div>

            </div>

            <table class="main-content__table">
                <tr class="main-content__header-row">
                    <th class="main-content__header-cell">№</th>

                    <th class="main-content__header-cell">Название</th>

                    <th class="main-content__header-cell">Адрес</th>

                    <th class="main-content__header-cell">Время работы</th>

                    <th class="main-content__header-cell">Контактный телефон</th>

                    <th class="main-content__header-cell">Статус</th>

                    {{--<th class="main-content__header-cell">Действия</th>--}}
                </tr>

                <tr class="main-content__table-row">
                    <td class="main-content__table-cell  main-content__table-cell--padding">154887</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <a href="#" class="main-content__table-link">Магазин1</a>
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">Киров, Московская улица</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        08-00, 18-00 <br>
                        Пн-Сб
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <a class="main-content__table-link" href="#">+ 7 234 345 34 34</a>
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <button class="switcher" type="button"></button>
                    </td>

                    {{--<td class="main-content__table-cell  main-content__table-cell--padding">--}}
                        {{--<a class="main-content__table-list-btn" href="stores-edit.html"></a>--}}
                        {{--<button class="main-content__table-delete-btn  js--delete-row" type="button"></button>--}}
                    {{--</td>--}}
                </tr>

                <tr class="main-content__table-row">
                    <td class="main-content__table-cell  main-content__table-cell--padding">154887</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <a href="#" class="main-content__table-link">Магазин1</a>
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">Киров, Московская улица</td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        08-00, 18-00 <br>
                        Пн-Сб
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <a class="main-content__table-link" href="#">+ 7 234 345 34 34</a>
                    </td>

                    <td class="main-content__table-cell  main-content__table-cell--padding">
                        <button class="switcher  switcher--active" type="button"></button>
                    </td>

                    {{--<td class="main-content__table-cell  main-content__table-cell--padding">--}}
                        {{--<a class="main-content__table-list-btn" href="stores-edit.html"></a>--}}
                        {{--<button class="main-content__table-delete-btn  js--delete-row" type="button"></button>--}}
                    {{--</td>--}}
                </tr>
            </table>


@endsection
