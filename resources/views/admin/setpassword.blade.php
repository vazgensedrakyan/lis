@extends('admin.layouts.authLayout')

@section('content')


    <section class="white-square">
        <div class="white-square__wrapper">
            <h1 class="white-square__title">Восстановление пароля</h1>

            {{ Form::open(array('id' => 'formx', 'class'=> 'white-square__form', 'route' => 'setpassword_form', 'method' => 'POST')) }}


                <div class="white-square__input-container{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="white-square__input-label">E-mail</label>
                    <input type="text" class="white-square__input" name="email"  value="{{ $email }}" required autofocus readonly>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="white-square__input-container{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="white-square__input-label">Password</label>

                    <input type="password" class="white-square__input" name="password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="white-square__input-container{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label class="white-square__input-label">Confirm Password</label>

                    <input type="password" class="white-square__input" name="password_confirmation" required>

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                    @endif
                </div>

                <div class="white-square__links-container">
                    <button type="submit" class="green-btn">Новый пароль</button>
                </div>
            </form>

        </div>
    </section>


@endsection
