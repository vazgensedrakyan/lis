<?php

namespace App\Repositories;


use App\Http\Models\Auth_codes;
use App\Http\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UserRepository
{


    /**
     * Check if user exist;
     * If not exist -> create;
     * Return user_id
     *
     * @param $phone
     * @return int
     */
    public static function UserGetOrCreateByPhone($phone) {

        $user = User::Where("phone",$phone)->first();


        if(empty($user)) {
            $clientRole = config('roles.models.role')::where('name', '=', 'Client')->first();
            $newUser = User::create(
                array(
                    'email' => '',
                    'name' => 'Пользоатель',
                    'password'=> Hash::make(Str::random(15)),
                    'phone' => $phone,
                    'active' => 1,
                    'banned' => 0,
                    'comment' => '',
                    'api_token' => Str::random(60)
                )
            );

            # Attach new role for user
            $user = User::find($newUser->id)->attachRole($clientRole);

            return $newUser->id;
        }

        return $user->id;
    }


    /**
     * FALSE - user banner or not active
     * TRUE - user active and not banned
     *
     * @param $user_id
     * @return bool
     */
    public static function checkUserStatus($user_id) {

        $user = User::Where("id",$user_id)->first();

        if($user->banned == 1) {
            return false;
        }
        return true;
    }


    /**
     * Get special array for user profile
     *
     * @param $user_id
     * @return array
     */
    public static function getUserProfileByID($user_id) {

        $user = User::Where("id",$user_id)->first();
        if(!empty($user)) {
            return [
                        "name" =>  $user->name,
                        "photo" => $user->photo,
                        "phone" => $user->phone,
                        "email" => $user->email,
                        "rating" => intval($user->rating)
                    ];
        }
        return [];

    }



    public static function getUserProfileByToken($request) {

        $token = $request->bearerToken();
        $phone = Auth_codes::select("phone")->where("token",$token)->first()["phone"];

        $user = User::Where("phone",$phone)->first();
        if(!empty($user)) {
            return [
                "name" =>  $user->name,
                "photo" => $user->photo,
                "phone" => $user->phone,
                "email" => $user->email,
                "rating" => intval($user->rating)
            ];
        }
        return [];
    }


    public static function setUserProfileByToken($request, $data) {

        if(count($data) >= 1) {
            $token = $request->bearerToken();
            $phone = Auth_codes::select("phone")->where("token", $token)->first()["phone"];

            User::Where("phone", $phone)->update($data);
        }
    }



    public static function getUserIdByToken($request) {

        $token = $request->bearerToken();
        $phone = Auth_codes::select("phone")->where("token",$token)->first()["phone"];

        $user = User::Where("phone",$phone)->first();
        if(!empty($user)) {
            return $user->id;
        }
        return 0;

    }





    public static function getAllUsersForAdmin() {

        # STEP 1: get id of role
        $clientRole = config('roles.models.role')::where('name', '=', 'Client')->first();
        $clientRoleID = $clientRole->id;

        $results = User::select("users.*","role_user.role_id")->leftJoin("role_user","role_user.user_id","=","users.id")->where("role_id",$clientRoleID)->get();

        $response = [];

        foreach ($results as $key=>$result) {
            $response[$key]["id"] = $result->id;
            if($result->photo != "")
                $response[$key]["photo"] = Storage::url($result->photo);
            else
                $response[$key]["photo"] = "/img/user-anchor.jpg";
            $response[$key]["name"] = $result->name;
            $response[$key]["phone"] = $result->phone;
            $response[$key]["comment"] = $result->comment;
            $response[$key]["email"] = $result->email;
            $response[$key]["rating"] = $result->rating;
            $response[$key]["orders"] = $result->orders;
            $response[$key]["banned"] = $result->banned;
            $response[$key]["created_at"] = $result->created_at;

        }

        return $response;


    }


//            # get user address
//            $response_array["addresses"] = [];
//            $user_address = User_address::where("user_id",$user->id)->get();
//            if(!empty($user_address)) {
//                foreach ($user_address as $key=>$item) {
//                    $response_array["addresses"][$key]["city"] = $item["city"];
//                    $response_array["addresses"][$key]["street"] = $item["street"];
//                    $response_array["addresses"][$key]["house"] = $item["house"];
//                    $response_array["addresses"][$key]["apartment"] = $item["apartment"];
//                    $response_array["addresses"][$key]["latitude"] = $item["latitude"];
//                    $response_array["addresses"][$key]["longitude"] = $item["longitude"];
//                }
//            }


}
