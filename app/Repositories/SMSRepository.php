<?php

namespace App\Repositories;


class SMSRepository
{

    # Documentation about plugin SMS.RU
    # https://github.com/zelenin/sms_ru


    public static function sendSMS($phone,$text) {

        $client = new \Zelenin\SmsRu\Api(new \Zelenin\SmsRu\Auth\ApiIdAuth('D55D12FC-0906-C6B0-72BC-87346F1051CD'));

        $sms1 = new \Zelenin\SmsRu\Entity\Sms($phone,$text);
        $client->smsSend($sms1);
    }


    public static function getBalance() {
        $client = new \Zelenin\SmsRu\Api(new \Zelenin\SmsRu\Auth\ApiIdAuth('D55D12FC-0906-C6B0-72BC-87346F1051CD'));

        $result = $client->myBalance();
        return $result->balance;
    }

}
