<?php

namespace App\Repositories;


use App\Http\Models\Auth_codes;
use Illuminate\Support\Str;

class JWTRepository
{

    # time in minutes
    private $time_to_live = 60;

    public static function renderToken() {
        return Str::random(60);
    }

    public function checkToken($token) {

        $time = $this->time_to_live;

        $authTry = Auth_codes::where([
            ["token","=",$token],
            ["active","=",1],
            ["created_at",">=",date("Y-m-d H:i:s",strtotime('- '. $time .' minutes'))]
        ])->first();

        if(!$authTry) {

            # remove old token
            Auth_codes::where("token",$token)->delete();
            return false;
        }

        return true;
    }




}
