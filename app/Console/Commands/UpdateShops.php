<?php

namespace App\Console\Commands;

use App\Http\Models\Shop;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class UpdateShops extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shops:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update shops from 1C API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $client = new Client(); //GuzzleHttp\Client
            $result = $client->request('GET', config('1c_api.url').'getShops')->getBody();

            foreach (json_decode($result) as $value) {
                Shop::updateOrCreate(['id' => $value->id], ['name' => $value->name]);
            }

            $this->line('<fg=green>Shops are updated successfully.</>');
        }
        catch (\Exception $e) {
            $this->line('<fg=red>Error occurred while trying to update shops.</>');
        }
    }
}
