<?php

namespace App\Console\Commands;

use App\Http\Models\Product;
use App\Http\Models\Shop;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class UpdateProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update products from 1C API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $shops = Shop::all();
            foreach ($shops as $shop) {
                foreach ($shop->categories as $category){
                    $client = new Client(); //GuzzleHttp\Client
                    $result = $client->request('GET', config('1c_api.url').'getProducts/' . $shop->id . '/' . $category->id)->getBody();

                    foreach (json_decode($result) as $value) {
                        if (isset($value->photo)){
//                            $f = finfo_open();
//                            $mime_type = finfo_buffer($f, $value->photo, FILEINFO_MIME_TYPE);
                            $mime_type = 'png';
                            $photo = 'product_images/'.str_random(25) . '.' . $mime_type;
                            Storage::put('public/' . $photo, base64_decode($value->photo));
                        } else {
                            $photo = '';
                        }
                        $this->line('<fg=yellow>Updating product ' . $value->id . '.</>');
                        Product::updateOrCreate(['id' => $value->id], [
                            'name' => $value->name,
                            'category_id' => $value->category_id,
                            'shop_id' => $shop->id,
                            'vendor_code' => $value->vendor_code,
                            'description' => $value->description,
                            'weight'      => $value->weight,
                            'unit' => $value->unit,
                            'price' => $value->price,
                            'total_left' => $value->total_left,
                            'place' => $value->place,
                            'status' => $value->status,
                            'photo' => $photo,
                        ]);
                    }
                }
            }

            $this->line('<fg=green>Products are updated successfully.</>');
        }
        catch (\Exception $e) {
            $this->line('<fg=red>Error occurred while trying to update products.</>');
            $this->line($e->getMessage());
        }
    }
}
