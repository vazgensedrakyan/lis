<?php

namespace App\Console\Commands;

use App\Http\Models\Category;
use App\Http\Models\Shop;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class UpdateCatgories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'categories:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Update categories from 1C API";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $client = new Client(); //GuzzleHttp\Client
            $shops = Shop::all();
            foreach ($shops as $shop){
                $result = $client->request('GET', config('1c_api.url').'getCategories/' . $shop->id)->getBody();

                foreach (json_decode($result) as $value) {
                    Category::updateOrCreate(['id' => $value->id], ['shop_id' => $shop->id, 'parentId' => isset($value->parentId) ? $value->parentId : null, 'name' => $value->name]);
                }
            }


            $this->line('<fg=green>Categories are updated successfully.</>');
        }
        catch (\Exception $e) {
            $this->line('<fg=red>Error occurred while trying to update categories.</>');
        }
    }
}
