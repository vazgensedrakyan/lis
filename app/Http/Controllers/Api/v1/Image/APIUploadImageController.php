<?php

namespace App\Http\Controllers\Api\v1\Image;

use App\Http\Models\Auth_codes;
use App\Http\Models\User;
use App\Http\Models\User_address;
use App\Repositories\JWTRepository;
use App\Repositories\SMSRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

class APIUploadImageController extends Controller
{


    public function uploadimg(Request $request)
    {


        # -----------
        # get Type
        $type = $request->input("type", "");
        if (empty($type)) {
            return response()->json([
                "code" => 2,
                "message" => "attribute type is required",
            ]);
        }

        if(empty($request->file('image'))) {
            return response()->json([
                "code" => 3,
                "message" => "Image file is required",
            ]);
        }

        switch ($type) {


            case "avatar":



                $fileType["extension"] = $request->file('image')->getClientOriginalExtension();
                $fileType["size"] = $request->file('image')->getSize();
                $fileType["mimeType"] = $request->file('image')->getMimeType();;


                # check mime type
                if(!in_array($fileType["mimeType"], ["image/jpeg","image/pjpeg","image/png"])) {
                    return response()->json([
                        "code" => 2,
                        "message" => "Mime type is not correct; Allowed types: image/jpeg, image/pjpeg, image/png"
                    ]);
                }


                # check file extension
                if(!in_array($fileType["extension"], ["jpeg","pjpeg","jpg","png"])) {
                    return response()->json([
                        "code" => 3,
                        "message" => "Extension type is not correct; Allowed types: jpg,jpeg,png"
                    ]);
                }

                $file_name = md5(uniqid(mt_rand(), true)).".".$fileType["extension"];
                $file_name_thumb = md5(uniqid(mt_rand(), true))."_thumb.".$fileType["extension"];

                $file_src = Storage::putFileAs('public/avatar', $request->file('image'), $file_name);



                $path = Storage::path($file_src);
                $link = Storage::url($file_src);



                $imageThumb = Image::make($path)->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(storage_path('/app/public/avatar/'.$file_name_thumb));




                $file_link_thumb = Storage::path('/app/public/avatar/'.$file_name_thumb);
                $link_thumb = Storage::url("avatar/".$file_name_thumb);

                $mainURL = env("APP_URL");

                return response()->json([
                    "code" => 0,
                    "message" => "Success",
                    "data" => [
                        "link_image" => $mainURL.$link,
                        "link_image_thumb" => $mainURL.$link_thumb,
                        "image" => "avatar/$file_name",
                        "image_thumb" => "avatar/$file_name_thumb"
                    ]
                ]);



                break;

        }




    }


}
