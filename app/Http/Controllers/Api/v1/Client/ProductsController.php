<?php

namespace App\Http\Controllers\Api\v1\Client;

use App\Http\Models\Product;
use App\Http\Resources\ProductResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    public function getProducts(Request $request)
    {
        if (!isset($request->shop_id)) {
            $responce = ['code' => 400, 'message' => 'Bad Request', 'errors' => ['shop_id' => 'Parameter `shop_id` is required']];
            return response()->json($responce)->setStatusCode(400);
        } elseif (!isset($request->page)) {
            $responce = ['code' => 400, 'message' => 'Bad Request', 'errors' => ['page' => 'Parameter `page` is required']];
            return response()->json($responce)->setStatusCode(400);
        }

        $products = Product::where('shop_id', $request->shop_id)->when(isset($request->category_id), function ($q) use ($request) {
            return $q->whereHas('category', function ($q) use ($request){
                return $q->where('id', $request->category_id)->orWhere('parentId', $request->category_id);
            });
        })->when(isset($request->sub_category_id), function ($q) use ($request) {
            return $q->whereHas('category', function ($q) use ($request){
                return $q->where('id', $request->sub_category_id);
            });
        })->paginate(10);

        $responce = ['code' => 200, 'message' => 'OK', 'data' => ['total' => $products->total(), 'last_page' => $products->lastPage(), 'page' => $products->currentPage(), 'per_page' => 10, 'products' => ProductResource::collection($products)]];

        return response()->json($responce);
    }
}
