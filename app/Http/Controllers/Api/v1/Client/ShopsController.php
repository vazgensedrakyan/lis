<?php

namespace App\Http\Controllers\Api\v1\Client;

use App\Http\Models\Shop;
use App\Http\Resources\ShopResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopsController extends Controller
{
    public function getShops()
    {
        $shops = Shop::all();

        $responce = ['code' => 200, 'message' => 'OK', 'data' => ['shops' => ShopResource::collection($shops)]];

        return response()->json($responce);
    }
}
