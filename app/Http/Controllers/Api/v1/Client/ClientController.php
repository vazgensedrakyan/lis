<?php

namespace App\Http\Controllers\Api\v1\Client;

use App\Http\Models\Auth_codes;
use App\Http\Models\User;
use App\Http\Models\User_address;
use App\Repositories\JWTRepository;
use App\Repositories\SMSRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ClientController extends Controller
{


    # get phone and response: nothing
    # send in header:
    # Accept: application/json
    # -- Authorization: Bearer pZpWcTffNRaOeNYJ75BUSzCjNPHk9r414rC1xULbaxMpb0IdrFZjzimNfulU--
    # Content-Type: application/json
    # send in body:
    # {"phone":"1234567890"}
    # dd($request->bearerToken());
    public function code(Request $request)
    {

        # get phone
        $phone = $request->input("phone", "");

        # clear + from phone if present
        $phone = str_replace("+", "", $phone);


        if (mb_strlen($phone) < 10 || mb_strlen($phone) > 10) {
            return response()->json([
                "code" => 1,
                "message" => "Phone number is incorrect"
            ]);
        }

        # generate random code
        $code = rand(1111, 9999);

        # store in auth_code table
        Auth_codes::create([
            "phone" => $phone,
            "code" => $code,
            "os" => "noname",
            "active" => 0
        ]);

        # send SMS
        // SMSRepository::sendSMS("+".$phone, "Ваш SMS-код: ".$code);


        return response()->json([
            "code" => 0,
            "message" => "Success"
        ]);

    }



    # SignIn user
    # get phone, sms-code and os; response: user profile + auth token
    # send in header:
    # Accept: application/json
    # -- Authorization: Bearer pZpWcTffNRaOeNYJ75BUSzCjNPHk9r414rC1xULbaxMpb0IdrFZjzimNfulU--
    # Content-Type: application/json
    # send in body:
    # {"phone":"1234567891","code":"2852","os":"iOS"}
    public function login(Request $request)
    {

        # -----------
        # get phone
        $phone = $request->input("phone", "");
        $phone = str_replace("+", "", $phone);
        # phone validation
        if (mb_strlen($phone) < 10 || mb_strlen($phone) > 10) {
            return response()->json([
                "code" => 1,
                "message" => "Phone number is incorrect"
            ]);
        }


        # -----------
        # get code
        $code = $request->input("code", 0);
        $code = intval($code);
        # code validation
        if ($code == 0 || $code < 1111 || $code > 9999) {
            return response()->json([
                "code" => 2,
                "message" => "Code is incorrect"
            ]);
        }


        # -----------
        # get OS
        $os = $request->input("os", "");
        if ($os == "") {
            return response()->json([
                "code" => 3,
                "message" => "OS is required"
            ]);
        }


        # -----------
        # check code and phone, if exist => set: active=1, OS=$os
        $Auth_try = Auth_codes::where([
            ["phone", "=", $phone],
            ["code", "=", $code],
            ["active", "=", 0],
            ["created_at", ">=", date("Y-m-d H:i:s", strtotime('-1 days'))]
        ])->first();


        if (empty($Auth_try)) {
            return response()->json([
                "code" => 4,
                "message" => "Code is incorrect"
            ]);
        } else {


            # render token
            $response_array["token"] = Str::random();
            Auth_codes::where("id", $Auth_try->id)->update([
                "token" => $response_array["token"],
                "os" => $os,
                "active" => 1
            ]);


            # check if user exist!
            # if not exist -> create new
            $user_id = UserRepository::UserGetOrCreateByPhone($phone);


            # check if user is active and not banned
            if (!UserRepository::checkUserStatus($user_id)) {
                return response()->json([
                    "code" => 5,
                    "message" => "User banned or not active",
                    "data" => []
                ]);
            }

            # get user profile
            $response_array["profile"] = UserRepository::getUserProfileByID($user_id);


            # General response
            return response()->json([
                "code" => 0,
                "message" => "Success",
                "data" => $response_array
            ]);

        }


    }


    public function getProfile(Request $request)
    {

        $response_array = UserRepository::getUserProfileByToken($request);

        # General response
        return response()->json([
            "code" => 0,
            "message" => "Success",
            "data" => $response_array
        ]);

    }


    public function setProfile(Request $request)
    {


        $data = [];

        # -----------
        # get Name
        $name = $request->input("name", "");
        if (!empty($name)) $data["name"] = $name;

        # -----------
        # get photo
        $photo = $request->input("photo", "");
        if (!empty($photo)) $data["photo"] = $photo;


        # -----------
        # get email
        $email = $request->input("email", "");
        if (!empty($email)) $data["email"] = $email;


        UserRepository::setUserProfileByToken($request, $data);


        $response_array = UserRepository::getUserProfileByToken($request);
        # General response
        return response()->json([
            "code" => 0,
            "message" => "Success",
            "data" => $response_array
        ]);

    }


    public function logout(Request $request)
    {

        $token = $request->bearerToken();

        Auth_codes::where("token",$token)->delete();

        # General response
        return response()->json([
            "code" => 0,
            "message" => "Success"
        ]);

    }


    }
