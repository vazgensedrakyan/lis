<?php

namespace App\Http\Controllers\Api\v1\Client;

use App\Http\Models\Category;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function getCategories(Request $request)
    {
        if (!isset($request->shop_id)) {
            $responce = ['code' => 400, 'message' => 'Bad Request', 'errors' => ['shop_id' => 'Parameter shop_id is required']];
            return response()->json($responce)->setStatusCode(400);
        }

        $categories = Category::where('shop_id', $request->shop_id)->whereNull('parentId')->select(['id', 'name'])->get();

        $responce = ['code' => 200, 'message' => 'OK', 'data' => ['categories' => CategoryResource::collection($categories)]];

        return response()->json($responce);
    }

    public function getSubcategories(Request $request)
    {
        if (!isset($request->shop_id)) {
            $responce = ['code' => 400, 'message' => 'Bad Request', 'errors' => ['shop_id' => 'Parameter shop_id is required']];
            return response()->json($responce)->setStatusCode(400);
        } elseif (!isset($request->category_id)) {
            $responce = ['code' => 400, 'message' => 'Bad Request', 'errors' => ['category_id' => 'Parameter category_id is required']];
            return response()->json($responce)->setStatusCode(400);
        }

        $subcategories = Category::where('shop_id', $request->shop_id)->where('parentId', $request->category_id)->select(['id', 'name'])->get();

        $responce = ['code' => 200, 'message' => 'OK', 'data' => ['subcategories' => CategoryResource::collection($subcategories)]];

        return response()->json($responce);
    }
}
