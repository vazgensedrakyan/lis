<?php

namespace App\Http\Controllers\Assembly;

use App\Http\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{



    public function index() {

        $args = [];
        $args["menu"] = "profile";
        return view("assembly.profiles",$args);

    }


    public function profile(Request $request, $id = 0) {
        $args = [];
        $args["menu"] = "profile";
        return view("assembly.profile",$args);
    }


}
