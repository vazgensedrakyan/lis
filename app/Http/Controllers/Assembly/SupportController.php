<?php

namespace App\Http\Controllers\Assembly;

use App\Http\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SupportController extends Controller
{



    public function index() {

        $args = [];
        $args["menu"] = "support";
        return view("assembly.supports",$args);

    }


    public function support(Request $request, $id = 0) {
        $args = [];
        $args["menu"] = "support";
        return view("assembly.support",$args);
    }


}
