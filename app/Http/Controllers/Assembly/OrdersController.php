<?php

namespace App\Http\Controllers\Assembly;

use App\Http\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{



    public function index() {

        $args = [];
        $args["menu"] = "orders";
        return view("assembly.orders",$args);

    }


    public function order(Request $request, $id = 0) {
        $args = [];
        $args["menu"] = "orders";
        return view("assembly.order",$args);
    }


}
