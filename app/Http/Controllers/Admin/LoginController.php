<?php

namespace App\Http\Controllers\Admin;

use App\Http\Models\User;
use App\Repositories\SMSRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    public function index() {
        # check if user
        if (Auth::check()) {
            return redirect()->route("orders");
        } else {
            $args = [];
            return view("admin.signin",$args);
        }
    }


    public function signin(Request $request) {

        $request->validate([
            'email' => 'bail|required|email',
            'password' => 'bail|required',
        ], [
            'email.required' => 'Ввод email обязателен',
            'email.email' => 'Некорректный формат email',
            'password.required' => 'Требуется пароль',
        ]);


        $data = $request->only("email","password");



        if(!Auth::attempt($data,true)) {

            return redirect()->route('login')->withErrors(["email"=>"Некорректный логин или пароль"]);
        }



        return redirect()->route("orders");

    }


    public function signout(Request $request) {
        Auth::logout();
        return redirect()->route("login");
    }


    public function resetpassword() {
        $args = [];
        return view("admin.resetpassword",$args);
    }
    public function resetpassword_form(Request $request) {

        $email = $request->only("email");
        $user = User::where("email",$email)->first();

        if(empty($user)) {
            return redirect()->route('resetpassword')->withErrors(["email"=>"Некорректный email"]);
        } else {
            $data["code"] = Str::random(60);
            User::where("email",$email)->update(["remember_token" => $data["code"]]);


            Mail::send('emails.resetpassword', $data, function ($message) {
                $message->Subject('reset password');
                $message->from('seredaes@gmail.com', 'Sitiy lis');
                $message->to('seredaes@gmail.com');
            });

            return redirect()->route('resetpassword')->withErrors(["email"=>"Письмо со ссылкой восстановления отправлено. Проверьте почту"]);
        }


    }
    public function setpassword(Request $request) {
        $code = $request->get("code");

        $checkCode = User::where("remember_token",$code)->first();

        if(empty($checkCode)) {
            return abort('403');
        } else {
            $args["email"] = $checkCode->email;
            return view("admin.setpassword",$args);
        }

    }
    public function setpassword_form(Request $request) {

        $request->validate([
            'password' => 'bail|required',
            'password_confirmation' => 'bail|required_with:password|same:password',
        ], [
            'password.required' => 'Требуется пароль',
            'password_confirmation.same' => 'Пароли не совпадают',
        ]);

        $email = $request->get("email");
        $new_password = $request->get("password");
        $new_password = Hash::make($new_password);

        User::where("email",$email)->update([ "password" =>  $new_password]);

        return redirect()->route("login");

    }


    public function test() {

        //        $client = new \Zelenin\SmsRu\Api(new \Zelenin\SmsRu\Auth\ApiIdAuth('D55D12FC-0906-C6B0-72BC-87346F1051CD'));
        //        $sms1 = new \Zelenin\SmsRu\Entity\Sms("+79146726790","test 123");
        //        $client->smsSend($sms1);


        $res = SMSRepository::getBalance();

        dd($res);

    }


}
