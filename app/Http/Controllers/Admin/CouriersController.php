<?php

namespace App\Http\Controllers\Admin;

use App\Http\Models\Courier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CouriersController extends Controller
{

    public function index() {
        $menu = "couriers";
        $couriers = Courier::all();

        return view("admin.couriers.index", compact('couriers', 'menu'));
    }

    public function show($id)
    {
        $menu = "couriers";
        $courier = Courier::findOrFail($id);

        return view('admin.couriers.show', compact('courier', 'menu'));
    }

    public function add()
    {
        $menu = "couriers";

        return view("admin.couriers.add", compact('menu'));
    }

    public function create(Request $request)
    {
        $courier = new Courier();

        $courier->full_name = $request->full_name;
        $courier->username = $request->username;
        $courier->phone = $request->phone;
        $courier->email = $request->email;
        $courier->banned = false;
        $courier->password = Hash::make(Str::random(10));

        if ($request->hasFile('photo')) {
            $courier->photo = $request->file('photo')->store('courier_avatars', 'public');
        }

        $courier->save();

        return redirect()->route('courier.edit', ['id' => $courier->id]);
    }

    public function edit($id)
    {
        $courier = Courier::findOrFail($id);
        $menu = "couriers";

        return view('admin.couriers.edit', compact('courier', 'menu'));
    }

    public function update($id, Request $request)
    {
        $courier = Courier::findOrFail($id);

        $courier->full_name = $request->full_name;
        $courier->username = $request->username;
        $courier->phone = $request->phone;
        $courier->email = $request->email;
        if (isset($request->password) && $request->password) {
            $courier->password = Hash::make($request->password);
        }

        if ($request->hasFile('photo')) {
            Storage::disk('public')->delete($courier->photo);
            $courier->photo = $request->file('photo')->store('courier_avatars', 'public');
        }

        $courier->save();

        return redirect()->route('courier.edit', ['id' => $courier->id]);
    }

    public function delete($id)
    {
        $courier = Courier::findOrFail($id);

        $courier->delete();

        return redirect()->route('couriers');
    }

    /*
     * Toggle banned property of client
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function toggle_banned($id)
    {
        $courier = Courier::findOrFail($id);

        $courier->banned = !$courier->banned;
        $courier->save();
        return back();
    }

    public function courier(Request $request) {
        $args = [];
        $args["menu"] = "couriers";
        return view("admin.courier",$args);
    }


    public function courier_form(Request $request, $id = 0) {
        $args = [];
        $args["menu"] = "couriers";


        $args["id"] = intval($id);

        return view("admin.courier_form",$args);
    }


}
