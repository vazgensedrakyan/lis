<?php

namespace App\Http\Controllers\Admin;

use App\Http\Models\Picker;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class PickerController extends Controller
{


    public function index() {

        $args = [];
        $args["menu"] = "pickers";

        $pickers = Picker::all();
        $args["pickers"] = $pickers;
        return view("admin.pickers.index",$args);

    }

    public function show($id)
    {
        $menu = "pickers";
        $picker = Picker::findOrFail($id);

        return view('admin.pickers.show', compact('picker', 'menu'));
    }

    public function add()
    {
        $menu = "pickers";

        return view("admin.pickers.add", compact('menu'));
    }

    public function create(Request $request)
    {
        $picker = new Picker();

        $picker->full_name = $request->full_name;
        $picker->username = $request->username;
        $picker->phone = $request->phone;
        $picker->email = $request->email;
        $picker->password = Hash::make($request->password);

        if ($request->hasFile('photo')) {
            $picker->photo = $request->file('photo')->store('picker_avatars', 'public');
        }

        $picker->save();

        return redirect()->route('picker.edit', ['id' => $picker->id]);
    }

    public function edit($id)
    {
        $picker = Picker::findOrFail($id);
        $menu = "pickers";

        return view('admin.pickers.edit', compact('picker', 'menu'));
    }

    public function update($id, Request $request)
    {
        $picker = Picker::findOrFail($id);

        $picker->full_name = $request->full_name;
        $picker->username = $request->username;
        $picker->phone = $request->phone;
        $picker->email = $request->email;
        if (isset($request->password) && $request->password) {
            $picker->password = Hash::make($request->password);
        }

        if ($request->hasFile('photo')) {
            Storage::disk('public')->delete($picker->photo);
            $picker->photo = $request->file('photo')->store('picker_avatars', 'public');
        }

        $picker->save();

        return redirect()->route('picker.edit', ['id' => $picker->id]);
    }

    public function delete($id)
    {
        $picker = Picker::findOrFail($id);

        $picker->delete();

        return redirect()->route('pickers');
    }

    public function picker(Request $request) {
        $args = [];
        $args["menu"] = "pickers";
        return view("admin.picker",$args);
    }

    public function picker_form(Request $request, $id = 0) {
        $args = [];
        $args["menu"] = "pickers";


        $args["id"] = intval($id);

        return view("admin.picker_form",$args);
    }

}
