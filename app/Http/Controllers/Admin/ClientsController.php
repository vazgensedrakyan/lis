<?php

namespace App\Http\Controllers\Admin;

use App\Http\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ClientsController extends Controller
{


    public function index() {

        $args = [];
        $args["menu"] = "clients";


        # get Clients
        $args["usersList"] = UserRepository::getAllUsersForAdmin();

        return view("admin.clients",$args);

    }


    public function client($client_id) {
        $client = User::findOrFail($client_id);

        $args = [];
        $args["menu"] = "clients";
        $args["client"] = $client;

        return view("admin.client",$args);
    }

    /*
     * Delete client
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $client = User::findOrFail($id);

        $client->delete();

        return redirect()->route('clients');
    }

    /*
     * Toggle banned property of client
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function toggle_banned($client_id)
    {
        $client = User::findOrFail($client_id);

        $client->banned = !$client->banned;
        $client->save();
        return back();
    }
}
