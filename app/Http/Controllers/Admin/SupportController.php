<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SupportController extends Controller
{


    public function index() {

        $args = [];
        $args["menu"] = "support";

        return view("admin.supports",$args);

    }


    public function support(Request $request, $id = 0) {

        $args = [];
        $args["menu"] = "support";

        return view("admin.support",$args);

    }

}
