<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoyaltyController extends Controller
{


    public function index() {

        $args = [];
        $args["menu"] = "loyalty";
        $args["submenu"] = "";

        return view("admin.loyalty",$args);

    }


    public function stock() {
        $args = [];
        $args["menu"] = "loyalty";
        $args["submenu"] = "stock";

        return view("admin.stock",$args);
    }


    public function bonuses() {
        $args = [];
        $args["menu"] = "loyalty";
        $args["submenu"] = "bonuses";

        return view("admin.bonuses",$args);
    }

}
