<?php

namespace App\Http\Controllers\Admin;

use App\Http\Models\Manager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ManagersController extends Controller
{

    public function index() {

        $args = [];
        $args["menu"] = "managers";

        $managers = Manager::all();

        $args["managers"] = $managers;
        return view("admin.managers.index",$args);

    }

    public function show($id)
    {
        $menu = "managers";
        $manager = Manager::findOrFail($id);

        return view('admin.managers.show', compact('manager', 'menu'));
    }

    public function add()
    {
        $menu = "managers";

        return view("admin.managers.add", compact('menu'));
    }

    public function create(Request $request)
    {
        $manager = new Manager();

        $manager->full_name = $request->full_name;
        $manager->username = $request->username;
        $manager->phone = $request->phone;
        $manager->email = $request->email;
        $manager->password = Hash::make($request->password);

        if ($request->hasFile('photo')) {
            $manager->photo = $request->file('photo')->store('manager_avatars', 'public');
        }

        $manager->save();

        return redirect()->route('manager.edit', ['id' => $manager->id]);
    }

    public function edit($id)
    {
        $manager = Manager::findOrFail($id);
        $menu = "managers";

        return view('admin.managers.edit', compact('manager', 'menu'));
    }

    public function update($id, Request $request)
    {
        $manager = Manager::findOrFail($id);

        $manager->full_name = $request->full_name;
        $manager->username = $request->username;
        $manager->phone = $request->phone;
        $manager->email = $request->email;
        if (isset($request->password) && $request->password) {
            $manager->password = Hash::make($request->password);
        }

        if ($request->hasFile('photo')) {
            Storage::disk('public')->delete($manager->photo);
            $manager->photo = $request->file('photo')->store('manager_avatars', 'public');
        }

        $manager->save();

        return redirect()->route('manager.edit', ['id' => $manager->id]);
    }

    public function delete($id)
    {
        $manager = Manager::findOrFail($id);

        $manager->delete();

        return redirect()->route('managers');
    }

    public function manager(Request $request) {
        $args = [];
        $args["menu"] = "managers";
        return view("admin.manager",$args);
    }


    public function manager_form(Request $request, $id = 0) {
        $args = [];
        $args["menu"] = "managers";


        $args["id"] = intval($id);

        return view("admin.manager_form",$args);
    }


}
