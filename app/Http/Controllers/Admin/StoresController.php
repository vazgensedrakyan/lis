<?php

namespace App\Http\Controllers\Admin;

use App\Http\Models\Category;
use App\Http\Models\Product;
use App\Http\Models\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoresController extends Controller
{


    public function index() {
        $menu = "stores";
        $submenu = "stores";

        $shops = Shop::all();

        return view("admin.stores.index", compact('shops', 'menu', 'submenu'));

    }


    public function categories() {
        $menu = "stores";
        $submenu = "categories";

        $categories = Category::whereNull('parentId')->paginate(10);
        $shops = Shop::all();


        return view("admin.stores.categories", compact('categories', 'shops', 'menu', 'submenu'));
    }


    public function products() {
        $menu = "stores";
        $submenu = "products";

        $categories = Category::whereNull('parentId')->paginate(10);
        $shops = Shop::all();
        $products = Product::paginate(15);
        return view("admin.stores.products" ,compact('categories', 'shops', 'menu', 'submenu', 'products'));
    }


    public function product(Request $request, $id = 0) {
        $args = [];
        $args["menu"] = "stores";
        $args["submenu"] = "products";
        return view("admin.product",$args);
    }


    public function stats() {
        $args = [];
        $args["menu"] = "stores";
        $args["submenu"] = "stats";
        return view("admin.stats",$args);
    }

}
