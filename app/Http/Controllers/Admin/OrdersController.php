<?php

namespace App\Http\Controllers\Admin;

use App\Http\Models\Order;
use App\Http\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{



    public function index() {

        $menu = "orders";
        $orders = Order::all();

        return view("admin.orders.index", compact('orders', 'menu'));

    }


    public function order(Request $request) {
        $args = [];
        $args["menu"] = "orders";
        return view("admin.order",$args);
    }


}
