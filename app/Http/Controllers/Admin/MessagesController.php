<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessagesController extends Controller
{

    public function index() {

        $args = [];
        $args["menu"] = "messages";

        return view("admin.messages",$args);

    }

}
