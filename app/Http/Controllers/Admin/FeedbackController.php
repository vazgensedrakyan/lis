<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{

    public function index() {

        $args = [];
        $args["menu"] = "feedback";

        return view("admin.feedback",$args);

    }

}
