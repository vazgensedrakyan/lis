<?php

namespace App\Http\Middleware;

use App\Repositories\JWTRepository;
use Closure;

class CheckAWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $token = $request->bearerToken();
        $jwtClass = new JWTRepository();
        if(!$jwtClass->checkToken($token)) {
            return response()->json([
                "code" => 403,
                "message" => "Auth token is incorrect",
            ]);
        }



        return $next($request);
    }
}
