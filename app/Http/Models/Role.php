<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name', 'slug', 'description', 'level'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, config('roles.roleUserTable'));
    }
}
