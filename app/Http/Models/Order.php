<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'client_id', 'courier_id', 'picker_id', 'weight', 'unit', 'address', 'status',
        'delivery_from', 'delivery_to', 'price', 'currency', 'payment_method'
    ];

    public $dates = ['delivery_from', 'delivery_to'];

    public function client()
    {
        return $this->belongsTo(User::class, 'client_id');
    }

    public function courier()
    {
        return $this->belongsTo(Courier::class, 'courier_id');
    }

    public function picker()
    {
        return $this->belongsTo(Picker::class, 'picker_id');
    }
}
