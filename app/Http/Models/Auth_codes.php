<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Auth_codes extends Model
{
    protected $fillable = [
        'phone',
        'code',
        'os',
        'active'
    ];

}
