<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Picker extends Model
{
    protected $fillable =[
        'shop_id', 'username', 'full_name', 'phone', 'photo', 'email', 'email_verified_at', 'password',
        'api_token', 'status', 'active',
    ];

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }
}
