<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use SoftDeletes;

    protected $fillable = ['id', 'name'];

    public function categories()
    {
        return $this->hasMany(Category::class, 'shop_id');
    }
}
