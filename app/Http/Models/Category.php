<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $fillable = ['id', 'shop_id', 'parentId', 'name'];

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    public function subcategories()
    {
        return $this->hasMany(Category::class, 'parentId');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parentId');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    public function productsCount()
    {
        $count = $this->products()->count();

        foreach ($this->subcategories as $subcategory) {
            $count += $subcategory->products()->count();
        }

        return $count;
    }
}
