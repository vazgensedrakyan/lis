<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class User_address extends Model
{

    protected $fillable = [
        'user_id',
        'city',
        'street',
        'house',
        'apartment',
        'latitude',
        'longitude'
    ];


}
