<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'points' => [
                'lat' =>  48.490787,
                'lng' => 135.086620,
            ],
            'color' => ['r' => 241, 'g' => 204, 'b' => 67],
        ];
    }
}
