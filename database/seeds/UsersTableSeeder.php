<?php


use App\Http\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //        $userRole = config('roles.models.role')::where('name', '=', 'User')->first();
        //        $adminRole = config('roles.models.role')::where('name', '=', 'Admin')->first();
        //        $permissions = config('roles.models.permission')::all();
        //
        //        /*
        //         * Add Users
        //         *
        //         */
        //        if (config('roles.models.defaultUser')::where('email', '=', 'admin@lis.com')->first() === null) {
        //            $newUser = config('roles.models.defaultUser')::create([
        //                'name'     => 'Admin',
        //                'email'    => 'admin@admin.com',
        //                'password' => bcrypt('password'),
        //            ]);
        //
        //            $newUser->attachRole($adminRole);
        //            foreach ($permissions as $permission) {
        //                $newUser->attachPermission($permission);
        //            }
        //        }
        //
        //        if (config('roles.models.defaultUser')::where('email', '=', 'user@user.com')->first() === null) {
        //            $newUser = config('roles.models.defaultUser')::create([
        //                'name'     => 'User',
        //                'email'    => 'user@user.com',
        //                'password' => bcrypt('password'),
        //            ]);
        //
        //            $newUser;
        //            $newUser->attachRole($userRole);
        //        }


        # CREATE ADMIN
        # get id for role with name "Admin"
        $adminRole = config('roles.models.role')::where('name', '=', 'Admin')->first();
        # create new user
        $newUser = User::create(
            array(
                'email' => 'admin@lis.com',
                'name' => 'superadmin',
                'password'=> Hash::make("Admin123"),
                'phone' => '380685777230',
                'active' => 1,
                'banned' => 0,
                'comment' => '',
                'api_token' => Str::random(60)
            )
        );
        # Attach new role for user
        User::find($newUser->id)->attachRole($adminRole);



        # CREATE MANAGER
        # get id for role with name "Manager"
        $managerRole = config('roles.models.role')::where('name', '=', 'Manager')->first();
        # create new user
        $newUser = User::create(
            array(
                'email' => 'manager@lis.com',
                'name' => 'Manager',
                'password'=> Hash::make("test"),
                'phone' => '380685777231',
                'active' => 1,
                'banned' => 0,
                'comment' => '',
                'api_token' => Str::random(60)
            )
        );
        # Attach new role for user
        User::find($newUser->id)->attachRole($managerRole);


        # CREATE PICKER
        # get id for role with name "Manager"
        $pickerRole = config('roles.models.role')::where('name', '=', 'Assembly')->first();
        # create new user
        $newUser = User::create(
            array(
                'email' => 'picker@lis.com',
                'name' => 'Сборщик',
                'password'=> Hash::make("test"),
                'phone' => '380685777231',
                'active' => 1,
                'banned' => 0,
                'comment' => '',
                'api_token' => Str::random(60)
            )
        );
        # Attach new role for user
        User::find($newUser->id)->attachRole($pickerRole);


        # CREATE COURIER
        # get id for role with name "Courier"
        $courierRole = config('roles.models.role')::where('name', '=', 'Courier')->first();
        # create new user
        $newUser = User::create(
            array(
                'email' => 'courier@lis.com',
                'name' => 'Courier',
                'password'=> Hash::make("test"),
                'phone' => '380685777232',
                'active' => 1,
                'banned' => 0,
                'comment' => '',
                'api_token' => Str::random(60)
            )
        );
        # Attach new role for user
        User::find($newUser->id)->attachRole($courierRole);



        # CREATE CLIENT
        # get id for role with name "Client"
        $clientRole = config('roles.models.role')::where('name', '=', 'Client')->first();
        # create new user
        $newUser = User::create(
            array(
                'email' => 'client@lis.com',
                'name' => 'Client',
                'password'=> Hash::make("test"),
                'phone' => '380685777233',
                'active' => 1,
                'banned' => 0,
                'comment' => '',
                'api_token' => Str::random(60)
            )
        );
        # Attach new role for user
        User::find($newUser->id)->attachRole($clientRole);


    }
}
