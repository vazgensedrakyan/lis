<?php

use App\Http\Models\Manager;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ManagersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Manager::create([
            'email' => 'manager@lis.com',
            'username' => 'Manager',
            'full_name' => 'Manager',
            'password'=> Hash::make("test"),
            'phone' => '380685777231',
            'email_verified_at' => \Carbon\Carbon::now(),
            'api_token' => Str::random(60)
        ]);
    }
}
