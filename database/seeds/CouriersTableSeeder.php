<?php

use App\Http\Models\Courier;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CouriersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Courier::create([
            'email' => 'courier@lis.com',
            'username' => 'Courier',
            'full_name' => 'Courier',
            'password'=> Hash::make("test"),
            'phone' => '380685777231',
            'email_verified_at' => \Carbon\Carbon::now(),
            'active' => 1,
            'banned' => 0,
            'api_token' => Str::random(60)
        ]);
    }
}
