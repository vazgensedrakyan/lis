<?php

use App\Http\Models\Order;
use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Order::create([
            'client_id' => 5,
            'courier_id' => 1,
            'picker_id' => 1,
            'weight' => 1320,
            'unit' => 'гр',
            'address' => '610048, Киров, Московская улица, дом 153 кв 98',
            'status' => 'Выполняется',
            'delivery_from' => \Carbon\Carbon::create(2019, 2, 7, 18, 0),
            'delivery_to' => \Carbon\Carbon::create(2019, 2, 8, 18, 0),
            'price' => 1450,
            'currency' => 'руб',
            'payment_method' => 'наличными курьеру',
        ]);
    }
}
