<?php

use App\Http\Models\Picker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PickersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Picker::create([
            'email' => 'picker@lis.com',
            'username' => 'Сборщик',
            'full_name' => 'Сборщик',
            'password'=> Hash::make("test"),
            'phone' => '380685777231',
            'email_verified_at' => \Carbon\Carbon::now(),
            'api_token' => Str::random(60)
        ]);
    }
}
