<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('photo')->default("");
            $table->string('name');
            $table->string('phone');
            $table->string('comment')->nullable()->default("");
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable()->default(null);
            $table->string('rating')->default(0);
            $table->string('orders')->default(0);
            $table->string('password');
            $table->string('api_token', 80)->unique()->nullable()->default(null);
            $table->integer('active')->default(1); // 0 - not active; 1 - active;
            $table->integer('banned')->default(0); // 0 - not banned; 1 - banned;
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
