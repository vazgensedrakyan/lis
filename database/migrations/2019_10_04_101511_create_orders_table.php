<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('client_id');
            $table->unsignedInteger('courier_id');
            $table->unsignedInteger('picker_id');
            $table->double('weight')->default(0);
            $table->string('unit')->default('kg');
            $table->string('address')->nullable();
            $table->string('status')->default('awaiting_assembly');
            $table->timestamp('delivery_from')->nullable();
            $table->timestamp('delivery_to')->nullable();
            $table->double('price')->default(0);
            $table->string('currency')->default('RUR');
            $table->string('payment_method')->default('cache');
            $table->timestamps();

            $table->foreign('courier_id')->references('id')->on('couriers');
            $table->foreign('picker_id')->references('id')->on('pickers');
            $table->foreign('client_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
